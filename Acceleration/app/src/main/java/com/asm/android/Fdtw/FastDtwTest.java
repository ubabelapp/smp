/*
 * FastDtwTest.java   Jul 14, 2004
 *
 */

package com.asm.android.Fdtw;


import com.asm.android.Fdtw.dtw.FastDTW;
import com.asm.android.Fdtw.dtw.TimeWarpInfo;
import com.asm.android.Fdtw.dtw.WarpPath;
import com.asm.android.Fdtw.matrix.ColMajorCell;
import com.asm.android.Fdtw.timeseries.TimeSeries;
import com.asm.android.Fdtw.util.DistanceFunction;
import com.asm.android.Fdtw.util.DistanceFunctionFactory;

/**
 * This class contains a main method that executes the FastDTW algorithm on two
 * time series with a specified radius.
 *
 * @author Vikash Sharma
 * 
 */
public class FastDtwTest
{
   /**
    * This main method executes the FastDTW algorithm on two time series with a
    * specified radius. The time series arguments are file names for files that
    * contain one measurement per line (time measurements are an optional value
    * in the first column). After calculating the warp path, the warp
    * path distance will be printed to standard output, followed by the path
    * in the format "(0,0),(1,0),(2,1)..." were each pair of numbers in
    * parenthesis are indexes of the first and second time series that are
    * linked in the warp path
    *
    * @param args  command line arguments (see method comments)
    */
      public static void main(String[] args)
      {
    	  System.out.println("starting time "+System.currentTimeMillis());
    	  args = new String[3];
    	  args[0] = "C:/Users/rainbow/drive/fastDtw/trace2.csv";
    	  args[1] = "C:/Users/rainbow/drive/fastDtw/trace3.csv";
    	  args[2] = "10";
    	//args[3]= "BinaryDistance";
          if (args.length!=3 && args.length!=4)
         {
            System.out.println("USAGE:  java FastDtwTest timeSeries1 timeSeries2 radius [EuclideanDistance|ManhattanDistance|BinaryDistance]");
            System.exit(1);
         }
         else
         {
            final TimeSeries tsI = new TimeSeries(args[0], false, false, ',');
            final TimeSeries tsJ = new TimeSeries(args[1], false, false, ',');
            
            final DistanceFunction distFn;
            if (args.length < 4)
            {
               distFn = DistanceFunctionFactory.getDistFnByName("EuclideanDistance");
            }
            else
            {
               distFn = DistanceFunctionFactory.getDistFnByName(args[3]);
            }   // end if
            
         
            
            
            
            final TimeWarpInfo info = FastDTW.getWarpInfoBetween(tsI, tsJ, Integer.parseInt(args[2]), distFn);

            System.out.println("Warp Distance: " + info.getDistance());
            System.out.println("Warp Path:     " + info.getPath());
            
            
            
			/*
			 * WarpPath paath = info.getPath(); for(int i
			 * =0;i<info.getPath().size();i++){
			 * 
			 * ColMajorCell cell = info.getPath().get(i);
			 * System.out.println("time readings  at index "+i+" x---> "+tsI.
			 * getMeasurementVector(cell.getCol())[0]+"   y -->"+tsJ.
			 * getMeasurementVector(cell.getRow())[0]);
			 * 
			 * }
			 */
            
           //
            WarpPath warpPath = info.getPath();
            
            for(int j=0;j<info.getPath().size();j++){
            	
            	ColMajorCell colMajorCell = info.getPath().get(j);
            	//System.out.println("length of the measurement vector for i ->"+tsI.getMeasurementVector(j).length);
            	//System.out.println("length of the measurement vector for J ->"+tsJ.getMeasurementVector(j).length);
            	//double timeSeriesPoint_I = tsI.getMeasurementVector(colMajorCell.getCol())[0];
            	//double timeSereisPoint_I_ROW  = tsI.getMeasurementVector(colMajorCell.getRow())[0];
            	
            	//double timeSeriesPoint_J = tsJ.getMeasurementVector(colMajorCell.getCol())[0];
            	//double timeSeriesPoint_J_ROW = tsJ.getMeasurementVector(colMajorCell.getRow())[0];
            	//System.out.println();
            //	System.out.print("All point at index -->"+j+"  i col ->"+timeSeriesPoint_I);
            	//System.out.print(" i Row ->"+timeSereisPoint_I_ROW);
            	//System.out.print(" j col ->"+timeSeriesPoint_J);
            	//System.out.print(" j Row ->"+timeSeriesPoint_J_ROW);
            	
            	
            	
            	
            }
            
            
           // ArrayList iIndex = paath.getMatchingIndexesForI(0);
           // ArrayList jIndex = paath.getMatchingIndexesForJ(0);
            
            
            
         }  // end if
          
          
          System.out.println("end  time "+System.currentTimeMillis());  
          

      }  // end main()


}  // end class FastDtwTest
