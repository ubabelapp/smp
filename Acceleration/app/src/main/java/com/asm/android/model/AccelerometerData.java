package com.asm.android.model;

/**
 * @author rainbow
 *
 */
public class AccelerometerData {

	private float x;
	private float y;
	private float z;
	private long timestamp;
	private long timeMillis;
	private double latitude;
	private double longitude;
	private double speed;
	private double bearing;

	/**
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public AccelerometerData(float x,float y, float z, long timestamp) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.timestamp = timestamp;
	}

    public AccelerometerData(float x, float y, float z, long timestamp, long timeMillis) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.timestamp = timestamp;
        this.timeMillis = timeMillis;
    }

    public AccelerometerData(float x, float y, float z, long timestamp, double latitude, double longitude, double speed, double bearing) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.bearing = bearing;
    }

    public AccelerometerData() {
	}

	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}


	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}


	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}


	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}


	/**
	 * @return the z
	 */
	public float getZ() {
		return z;
	}


	/**
	 * @param z the z to set
	 */
	public void setZ(float z) {
		this.z = z;
	}
	/**
	 * setting timestamp values
	 * 
	 * @param timestamp
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * getting timestamp values
	 * 
	 * @return
	 */
	public long getTimestamp() {
		return timestamp;
	}


	public long getTimeMillis() {
		return timeMillis;
	}

	public void setTimeMillis(long timeMillis) {
		this.timeMillis = timeMillis;
	}

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }
}
