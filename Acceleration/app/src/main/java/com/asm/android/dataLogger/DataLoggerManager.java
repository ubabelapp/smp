package com.asm.android.dataLogger;

import android.content.Context;
import android.os.Environment;

import com.asm.android.constant.Constants;
import com.asm.android.utils.Util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/*
 * Created by KircherEngineerH on 4/27/2016.
 */
public class DataLoggerManager implements Runnable {
    private static final String tag = DataLoggerManager.class.getSimpleName();

    public final static String DEFAULT_APPLICATION_DIRECTORY = "ABC_SENSOR_DATA_RAW";

    private final static long THREAD_SLEEP_TIME = 20;
    public final static String FILE_NAME_SEPARATOR = "-";

    // boolean to indicate if the data should be written to a file.
    private volatile boolean logData = false;

    // Log output time stamp
    protected long logTime = 0;

    private ArrayList<String> csvHeaders;
    private ArrayList<String> csvValues;

    private DataLoggerInterface dataLogger;

    private Context context;

    private volatile ArrayList<String> acceleration;

    private Thread thread;

    private final static String TAG = "DataLoggerManager";

    public DataLoggerManager(Context context) {
        this.context = context;
        csvHeaders = getCsvHeaders();
        csvValues = new ArrayList<>();
        acceleration = new ArrayList<>();
    }

    @Override
    public void run() {
        while (logData && !Thread.currentThread().isInterrupted()) {
            // Check if the row is filled and ready to be written to the
            // log.
            logData();

            try {
                Thread.sleep(THREAD_SLEEP_TIME);
            } catch (InterruptedException e) {
                // very important to ensure the thread is killed
                Thread.currentThread().interrupt();
            }
        }
        // very important to ensure the thread is killed
        Thread.currentThread().interrupt();
    }


    public void startDataLog() throws IllegalStateException {
        if (!logData) {
            logData = true;
            logTime = System.currentTimeMillis();
            dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
            dataLogger.setHeaders(csvHeaders);
            thread = new Thread(this);
            thread.start();
        } else {
            throw new IllegalStateException("Logger is already started!");
        }
    }

    public void stopDataLog() throws IllegalStateException {
        if (logData) {
            logData = false;
            thread.interrupt();
            thread = null;
            dataLogger.writeToFile();
        }
    }

    public void setAcceleration(float[] acceleration) {
        synchronized (acceleration) {
            this.acceleration.clear();
            String eventStatus = "-";
            for (int i = 0; i < 40; i++) {
                if(i == 33){
                    if(acceleration[i] == Constants.OVER_SPEEDING){
                        eventStatus = Constants.STRING_OVER_SPEEDING;
                    }else if(acceleration[i] == Constants.HARSH_BRAKE){
                        eventStatus = Constants.STRING_HARSH_BRAKE;
                    }
                    this.acceleration.add(eventStatus);
                }else {
                    this.acceleration.add(String.valueOf(acceleration[i]));
                }
            }
        }
    }

    private void logData() {
        csvValues.clear();
        csvValues.add(String.valueOf((System.currentTimeMillis() - logTime) / 1000.0f));

        synchronized (acceleration) {
            csvValues.addAll(acceleration);
            this.acceleration.clear();
        }

        if(csvValues.size()>= 19) {
            dataLogger.addRow(csvValues);
            csvValues.clear();
        }
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return "SensorData" +".csv";
    }

    private ArrayList<String> getCsvHeaders() {
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("TimestampMillis");
        headers.add("Acc-X-value");
        headers.add("Acc-Y-value");
        headers.add("Acc-Z-value");
        headers.add("Gyro-X-value");
        headers.add("Gyro-Y-value");
        headers.add("Gyro-Z-value");
        headers.add("gyroscopeRotationVelocity");
        headers.add("deltaQuaternionXAxis");
        headers.add("deltaQuaternionYAxis");
        headers.add("deltaQuaternionZAxis");
        headers.add("magnitude");
        headers.add("Azimuth");
        headers.add("Roll");
        headers.add("pitch");
        headers.add("deviation");
        headers.add("latitude");
        headers.add("longitude");
        headers.add("speed");
        headers.add("SensorSpeed");
        headers.add("SensorCurrentAcceleration");
        headers.add("SensorCurrentDistanceTravelled");
        headers.add("SensorCurrentDirection");
        headers.add("Linear-Acc-X-value");
        headers.add("Linear-Acc-Y-value");
        headers.add("Linear-Acc-Z-value");
        headers.add("TotalDistance");
        headers.add("DeltaTime");
        headers.add("YAW radian");
        headers.add("YAW Deviation");
        headers.add("GyroFilteredXValue");
        headers.add("GyroFilteredYValue");
        headers.add("GyroFilteredZValue");
        headers.add("EventStatus");
        headers.add("Bearing");
        headers.add("BearingAccuracy");
        headers.add("Altitude");
        headers.add("SpeedAccuracy");
        headers.add("yawInAngle");
        headers.add("yawDeviationAngle");
        return headers;
    }
}
