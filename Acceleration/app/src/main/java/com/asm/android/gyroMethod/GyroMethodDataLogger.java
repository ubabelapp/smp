package com.asm.android.gyroMethod;

import android.content.Context;

import com.asm.android.constant.Constants;
import com.asm.android.dataLogger.CsvDataLogger;
import com.asm.android.dataLogger.DataLogger;
import com.asm.android.dataLogger.DataLoggerInterface;
import com.asm.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sourabh on 16-03-2018.
 */

public class GyroMethodDataLogger {

    private static final String TAG = DataLogger.class.getSimpleName();

    private ArrayList<String> csvHeaders;
    private DataLoggerInterface dataLogger;

    public GyroMethodDataLogger(Context context) {

        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<String> value){
        dataLogger.addRow(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return Constants.V_SENSE_DATA_FILE_NAME;
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("TimeStampMillis");
        headers.add("type");
        headers.add("Gyro-Z-value (Yaw)");
        headers.add("Speed");
        headers.add("Latitude");
        headers.add("Longitude");
        headers.add("Altitude");
        headers.add("Bearing");
        headers.add("Event Type");
        headers.add("Displacement value");
        return headers;
    }
}
