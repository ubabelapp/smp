package com.asm.android.earthRotation;


import com.asm.android.async.ErrorContext;
import com.asm.android.earthRotation.event.DataCollectionEvent;
import com.asm.android.earthRotation.function.Function;

public class DataCollectionEventConsumerErrorContext implements ErrorContext {
    private Function<DataCollectionEvent, ?> function;
    private DataCollectionEvent dataCollectionEvent;
    private Throwable error;

    public DataCollectionEventConsumerErrorContext(Function<DataCollectionEvent, ?> function, DataCollectionEvent dataCollectionEvent, Throwable error) {
        this.function = function;
        this.dataCollectionEvent = dataCollectionEvent;
        this.error = error;
    }

    public Function<DataCollectionEvent, ?> getFunction() {
        return function;
    }

    public DataCollectionEvent getDataCollectionEvent() {
        return dataCollectionEvent;
    }

    public Throwable getError() {
        return error;
    }
}
