package com.asm.android.model;

/*
 * Created by rainbow on 30-12-2017.
 */

public class AngularAxis {

    private Double angle;
    private Double pitch;
    private Double roll;
    private long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public AngularAxis() {
    }

    public AngularAxis(Double angle, Double pitch, Double roll, long timestamp) {
        this.angle = angle;
        this.pitch = pitch;
        this.roll = roll;
        this.timestamp = timestamp;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }

    public Double getPitch() {
        return pitch;
    }

    public void setPitch(Double pitch) {
        this.pitch = pitch;
    }

    public Double getRoll() {
        return roll;
    }

    public void setRoll(Double roll) {
        this.roll = roll;
    }
}
