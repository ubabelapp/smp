package com.asm.android.filter;

import android.content.Context;

import com.asm.android.vehicleState.VehicleState;
import com.asm.android.filterVO.StationaryFilterVO;
import static com.asm.android.constant.Constants.*;

/**
 * Created by Vikash Sharma on 23-02-2018.
 */

public class StationaryFilter {

    public static boolean isTrackingAllowed(Context mContext,StationaryFilterVO filterVO){
        VehicleState state = new VehicleState();
        String vehicleState = state.getAndUpdateVehicleState(filterVO);
        if(vehicleState.equals(VEHICLE_STATE_1)){
            return false;
        }else
            return true;
    }
}
