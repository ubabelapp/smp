package com.asm.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.asm.android.constant.Constants;
import com.asm.android.constant.SharedPrefConstants;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/*
 * Created by Sourabh on 02-02-2018.
 */

public class Util {

    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static String createFolder(){

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH:mm", Locale.ENGLISH).format(new Date());
        File dir = new File(Environment.getExternalStorageDirectory()+
                "/"+ Constants.APP_FOLDER_NAME+"/"+timeStamp);
        if(!dir.exists()){
            dir.mkdirs();
        }
        SharedPreferenceManager.write(SharedPrefConstants.FOLDER_NAME, dir.getAbsolutePath());
        return dir.getAbsolutePath();
    }

    public static String createAndGetFolderPath(){
        return SharedPreferenceManager.read(SharedPrefConstants.FOLDER_NAME, null);
    }


}
