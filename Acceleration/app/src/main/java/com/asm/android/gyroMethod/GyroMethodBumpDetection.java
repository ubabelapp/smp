package com.asm.android.gyroMethod;

import android.content.Context;

import com.asm.android.model.EventData;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author -
 *
 */
public class GyroMethodBumpDetection {

    private static final String TAG = GyroMethodBumpDetection.class.getSimpleName();

    private static final int NO_BUMP_STATE = 1;
    private static final int ONE_BUMP_STATE = 2;
    private static final int WAITING_FOR_BUMP_STATE = 3;
    private static final int SECOND_BUMP_STATE = 4;
    private static final float DELTA_YAW_SMALL = 0.05f;
    private static final float DELTA_YAW_HIGH = 0.07f;
    private static final float T_BUMP = 1.0f; // 1.5sec in millis
    private static final float T_HARSH_BUMP = 0.7f; // 1.5sec in millis
    private static final float TS_NEXT_DELAY = 3.0f; // 3 seconds in millis;
    private static final float TS_HARSH_NEXT_DELAY = 1.5f; // 3 seconds in millis;

    private int mState = 1;
    private double TS_DWELL = 0.0; // duration_in_bump_state
    private float mStartTime;
    private float mEndTime;
    private boolean mStopLooping = false;
    private float mLastValue = 0;
    private double mStartBearing = 0;
    private float mOneBumpStartTime;
    private boolean mOneBumpFlag = true;


    private int mStateHarsh = 1;
    private double TS_DWELL_HARSH = 0.0; // duration_in_bump_state
    private float mStartTime_harsh;
    private float mEndTime_harsh;
    private float mLastValue_harsh = 0;
    private double mStartBearing_harsh = 0;

    private GyroMethodDataLogger mGyroMethodDataLogger;

    public GyroMethodBumpDetection(Context context){

        mGyroMethodDataLogger = new GyroMethodDataLogger(context);
        mGyroMethodDataLogger.setHeaders();
    }

    public void checkForBumpState(ArrayList<EventData> bumpEventDataList, float sensorFrequency) {

        float yawRate;
        ArrayList<String> rowData = new ArrayList<>();
        double displacement = 0;

        for (int i = 0; i < bumpEventDataList.size(); i++) {

            if(mStopLooping){
                break;
            }

            yawRate = bumpEventDataList.get(i).getGyroscopeData().getZ();
            long timeInNano = bumpEventDataList.get(i).getGyroscopeData().getTimestamp();
            long timeInMillis = (new Date()).getTime()+ (timeInNano - System.nanoTime()) / 1000000L;
            float timeInSec = bumpEventDataList.get(i).getGyroscopeData().getTimeInSec();

            rowData.add(String.valueOf(timeInSec));
            rowData.add(String.valueOf(timeInMillis));
            rowData.add("normalBump");
            rowData.add(String.valueOf(yawRate));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getSpeed()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getLatitude()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getLongitude()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getAltitude()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getBearing()));
            if(timeInSec > mLastValue) {
                if (i == bumpEventDataList.size() - 1) {
                    mLastValue = timeInSec;
                }
            }

            if (mState == NO_BUMP_STATE && yawRate > DELTA_YAW_SMALL) {
                // First Bump Started
                mState = ONE_BUMP_STATE;
                // Record the start time of the bump detection
                mStartTime = timeInSec;
                mStartBearing = bumpEventDataList.get(i).getGyroscopeData().getBearing();


            } else if (mState == ONE_BUMP_STATE && yawRate < DELTA_YAW_SMALL) {
                // Record the end point of a possible bump
                mEndTime = timeInSec;
                mOneBumpFlag = false;

                if ((mEndTime - mStartTime)>= T_BUMP) {

                    // Valid bump detected
                    // find the total duration of the event since it past the
                    // NO_BUMP STATE
                    mState = WAITING_FOR_BUMP_STATE;


                } else {
                    mState = NO_BUMP_STATE;
                }
            }
            else if (mState == WAITING_FOR_BUMP_STATE) {

                // Record the duration of the event TS_DWELL -> time stamp to live in one state
                mEndTime = timeInSec;
                TS_DWELL = mEndTime - mStartTime;

                if (TS_DWELL < TS_NEXT_DELAY && yawRate > DELTA_YAW_SMALL) {

                    if (!mOneBumpFlag) {
                        mOneBumpFlag = true;
                        mOneBumpStartTime = timeInSec;
                    }
                    // Second Valid bump detected
                    // find the total duration of the event since it past the
                    // NO_BUMP STATE
                    mState = SECOND_BUMP_STATE;

                } else if (TS_DWELL > TS_NEXT_DELAY) {

                    // One valid bump --> Turn
                    displacement = GyroMethodDisplacement.calculateDisplacement(bumpEventDataList, (int) sensorFrequency);
                    double diff = getDeviation(bumpEventDataList.get(i).getGyroscopeData().getBearing(),
                            mStartBearing);

                    if (diff > 70 && diff < 120) {
                        rowData.add("TURN - one valid bump");
                    } else if (diff > 160 && diff < 220) {
                        rowData.add("U-TURN - one valid bump");
                    }
                    if (displacement >= 4.0) {
                        // turn
                    }
                    rowData.add(String.valueOf(displacement));
                    mState = NO_BUMP_STATE;

                } else {
                    // continue in waiting for bump state;
                }

            }else if (mState == SECOND_BUMP_STATE && yawRate < DELTA_YAW_SMALL) {

                // Record the end point of a possible bump
                mEndTime = timeInSec;

                if ((mEndTime - mOneBumpStartTime)>= T_BUMP) {

                    //Valid Lane change detected
                    displacement = GyroMethodDisplacement.calculateDisplacement(bumpEventDataList,(int)sensorFrequency);
                    rowData.add("LANE CHANGE - two valid bump");
                    rowData.add(String.valueOf(displacement));

                    if(displacement >= 3.0){
                        // lane chnage
                    }

                } else if((mEndTime - mOneBumpStartTime)>= T_HARSH_BUMP){
                    //Valid Lane change detected
                    displacement = GyroMethodDisplacement.calculateDisplacement(bumpEventDataList,(int)sensorFrequency);
                    rowData.add(" HARSH LANE CHANGE - two valid bump");
                    rowData.add(String.valueOf(displacement));

                    if(displacement >= 3.0){
                        // lane chnage
                    }
                }else{
                    // ONLY one valid bump during whole maneuvering
                    //Turn detected
                    //check for turn conditions whether displacement and heading are valid
                    //else no lane change
                    displacement = GyroMethodDisplacement.calculateDisplacement(bumpEventDataList, (int) sensorFrequency);
                    double diff = getDeviation(bumpEventDataList.get(i).getGyroscopeData().getBearing(),
                            mStartBearing);

                    if (diff > 70 && diff < 120) {
                        rowData.add("TURN - one valid bump");
                    } else if (diff > 160 && diff < 220) {
                        rowData.add("U-TURN - one valid bump");
                    }
                    if (displacement >= 4.0) {
                        // turn
                    }
                    rowData.add(String.valueOf(displacement));

                }
                mState = NO_BUMP_STATE;

            }else {
                // continue in the current state
            }
            // add each line of data to file

            if(!mStopLooping) {

                if(timeInSec > mLastValue) {
                    mGyroMethodDataLogger.addRow(rowData);
                }
            }
            rowData.clear();
        }

        checkForHarshBumpState(bumpEventDataList,sensorFrequency);
    }

    public void stopVSenseLogging(){
        mStopLooping = true;
        mGyroMethodDataLogger.stopLogging();
    }



    public void checkForHarshBumpState(ArrayList<EventData> bumpEventDataList, float sensorFrequency) {

        float yawRate;
        ArrayList<String> rowData = new ArrayList<>();
        double displacement = 0;

        for (int i = 0; i < bumpEventDataList.size(); i++) {

            if(mStopLooping){
                break;
            }

            yawRate = bumpEventDataList.get(i).getGyroscopeData().getZ();
            long timeInNano = bumpEventDataList.get(i).getGyroscopeData().getTimestamp();
            long timeInMillis = (new Date()).getTime()+ (timeInNano - System.nanoTime()) / 1000000L;
            float timeInSec = bumpEventDataList.get(i).getGyroscopeData().getTimeInSec();

            rowData.add(String.valueOf(timeInSec));
            rowData.add(String.valueOf(timeInMillis));
            rowData.add("HarshBump");
            rowData.add(String.valueOf(yawRate));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getSpeed()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getLatitude()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getLongitude()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getAltitude()));
            rowData.add(String.valueOf(bumpEventDataList.get(i).getGyroscopeData().getBearing()));
            if(timeInSec > mLastValue_harsh) {
                if (i == bumpEventDataList.size() - 1) {
                    mLastValue_harsh = timeInSec;
                }
            }

            if (mStateHarsh == NO_BUMP_STATE && yawRate > DELTA_YAW_SMALL) {
                // First Bump Started
                mStateHarsh = ONE_BUMP_STATE;
                // Record the start time of the bump detection
                mStartTime_harsh = timeInSec;

            } else if (mStateHarsh == ONE_BUMP_STATE && yawRate < DELTA_YAW_SMALL) {
                // Record the end point of a possible bump
                mEndTime_harsh = timeInSec;

                if ((mEndTime_harsh - mStartTime_harsh)>= T_HARSH_BUMP) {

                    // Valid bump detected
                    // find the total duration of the event since it past the
                    // NO_BUMP STATE
                    mStateHarsh = WAITING_FOR_BUMP_STATE;
                } else {
                    mStateHarsh = NO_BUMP_STATE;
                }
            }
            else if (mStateHarsh == WAITING_FOR_BUMP_STATE) {

                // Record the duration of the event TS_DWELL -> time stamp to live in one state
                mEndTime_harsh = timeInSec;
                TS_DWELL_HARSH = mEndTime_harsh - mStartTime_harsh;

                if (TS_DWELL_HARSH < TS_HARSH_NEXT_DELAY && yawRate > DELTA_YAW_SMALL) {
                    // Possibility of the next bump
                    if ((mEndTime_harsh - mStartTime_harsh)>= T_HARSH_BUMP) {
                        // Two valid bumps --> Lane chane
                        displacement = GyroMethodDisplacement.calculateDisplacement(bumpEventDataList,(int)sensorFrequency);
                        rowData.add("LANE CHANGE - two valid bump");
                        rowData.add(String.valueOf(displacement));

                        if(displacement >= 3.0){
                            // lane chnage
                        }

                    } else {
                        // One valid bump --> Turn
                        displacement =  GyroMethodDisplacement.calculateDisplacement(bumpEventDataList,(int)sensorFrequency);
                        double diff = getDeviation(bumpEventDataList.get(i).getGyroscopeData().getBearing(),
                                mStartBearing);

                        if(diff > 70 && diff <120){
                            rowData.add("TURN - one valid bump");
                        }else if(diff > 160 && diff <220 ){
                            rowData.add("U-TURN - one valid bump");
                        }

                        if(displacement > 4.0){
                            // turn
                        }
                        rowData.add(String.valueOf(displacement));

                    }
                    mStateHarsh = NO_BUMP_STATE;
                } else if (TS_DWELL_HARSH > TS_HARSH_NEXT_DELAY) {

                    // One valid bump --> Turn
                    displacement =  GyroMethodDisplacement.calculateDisplacement(bumpEventDataList,(int)sensorFrequency);

                    double diff = getDeviation(bumpEventDataList.get(i).getGyroscopeData().getBearing(),
                            mStartBearing);

                    if(diff > 70 && diff <120){
                        rowData.add("TURN - one valid bump");
                    }else if(diff > 160 && diff <220 ){
                        rowData.add("U-TURN - one valid bump");
                    }

                    if(displacement > 4.0){
                        // turn
                    }
                    rowData.add(String.valueOf(displacement));
                    mStateHarsh = NO_BUMP_STATE;

                } else {
                    // continue in waiting for bump state;
                }
            } else {
                // continue in the current state
            }
            // add each line of data to file

            if(!mStopLooping) {

                if(timeInSec > mLastValue_harsh) {
                    mGyroMethodDataLogger.addRow(rowData);
                }
            }
            rowData.clear();
        }
    }

    private double getDeviation(double a, double b){

        double d = Math.abs(a - b) % 360;
        double r = d > 180 ? 360 - d : d;
        int sign = (a - b >= 0 && a - b <= 180) || (a - b <=-180 && a- b>= -360) ? 1 : -1;
        r *= sign;
        return r;
    }
}
