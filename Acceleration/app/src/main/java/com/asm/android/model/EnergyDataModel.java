package com.asm.android.model;

/**
 *
 * Created by Sourabh on 09-01-2018.
 */

public class EnergyDataModel {

    private float deviation;
    private float timestamp;
    private float accelerometerXAxis;
    private float accelerometerYAxis;
    private float accelerometerZAxis;
    private float gyroXAxis;
    private float gyroYAxis;
    private float gyroZAxis;
    private float azimuth;
    private float roll;
    private float pitch;
    private float gpsSpeed;
    private double latitude;
    private double longitude;
    private double bearing;

    public float getDeviation() {
        return deviation;
    }

    public void setDeviation(float deviation) {
        this.deviation = deviation;
    }

    public float getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(float timestamp) {
        this.timestamp = timestamp;
    }

    public float getAccelerometerXAxis() {
        return accelerometerXAxis;
    }

    public void setAccelerometerXAxis(float accelerometerXAxis) {
        this.accelerometerXAxis = accelerometerXAxis;
    }

    public float getAccelerometerYAxis() {
        return accelerometerYAxis;
    }

    public void setAccelerometerYAxis(float accelerometerYAxis) {
        this.accelerometerYAxis = accelerometerYAxis;
    }

    public float getAccelerometerZAxis() {
        return accelerometerZAxis;
    }

    public void setAccelerometerZAxis(float accelerometerZAxis) {
        this.accelerometerZAxis = accelerometerZAxis;
    }

    public float getGyroXAxis() {
        return gyroXAxis;
    }

    public void setGyroXAxis(float gyroXAxis) {
        this.gyroXAxis = gyroXAxis;
    }

    public float getGyroYAxis() {
        return gyroYAxis;
    }

    public void setGyroYAxis(float gyroYAxis) {
        this.gyroYAxis = gyroYAxis;
    }

    public float getGyroZAxis() {
        return gyroZAxis;
    }

    public void setGyroZAxis(float gyroZAxis) {
        this.gyroZAxis = gyroZAxis;
    }

    public float getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public float getRoll() {
        return roll;
    }

    public void setRoll(float roll) {
        this.roll = roll;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public float getGpsSpeed() {
        return gpsSpeed;
    }

    public void setGpsSpeed(float gpsSpeed) {
        this.gpsSpeed = gpsSpeed;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public EnergyDataModel(float deviation, float timestamp, float acceleXAxis, float acceleYAxis,
                           float acceleZAxis, float gyroXAxis, float gyroYAxis, float gyroZAxis,
                           float azimuth, float roll, float pitch, float gpsSpeed, double latitude,
                           double longitude, double bearing) {

        this.deviation = deviation;
        this.timestamp = timestamp;
        this.accelerometerXAxis = acceleXAxis;
        this.accelerometerYAxis = acceleYAxis;
        this.accelerometerZAxis = acceleZAxis;
        this.gyroXAxis = gyroXAxis;
        this.gyroYAxis = gyroYAxis;
        this.gyroZAxis = gyroZAxis;
        this.azimuth = azimuth;
        this.roll = roll;
        this.pitch = pitch;
        this.gpsSpeed = gpsSpeed;
        this.latitude = latitude;
        this.longitude = longitude;
        this.bearing = bearing;
    }
}
