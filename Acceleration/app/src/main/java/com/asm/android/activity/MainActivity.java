package com.asm.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.asm.android.R;
import com.asm.android.constant.Constants;
import com.asm.android.constant.SharedPrefConstants;
import com.asm.android.location.OldLocationTracker;
import com.asm.android.utils.SharedPreferenceManager;
import com.asm.android.utils.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/*
 * Created by Sourabh on 14-02-2018.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CODE = 200;

    private SensorData mSensorData;
    private Context mContext = this;
    private Button button;
    private Camera mCamera;
    private TextureView mPreview;
    private MediaRecorder mMediaRecorder;
    private File mOutputFile;
    private boolean isRecording = false;
    private OldLocationTracker locationTracker;
    private Button btnPotHole, btnUTurn, btnLTurn, btnRTurn,btnZigZag, btnRoundAbout, btnLeftCurve,
            btnRightCurve, btnRashDriving, btnHarshBrake, btnLeftLaneChange, btnRightLaneChange, btnRoadHumps,
            btnUnevenRoad, btnContinousCurve, btnTrafficLight, btnUpSlope, btnDownSlope,
            btnUnderPass, btnFlyover, btnHarshLaneChange,btnSetSpeed;
    private boolean mIsButtonClick = false;
    private boolean mIsVideoStarted = false;

    private boolean potHoleEnabled, UTurnEnabled, LTurnEnabled, RTurnEnabled, zigZagEnabled,
            roundAboutEnabled,LRoadCurveEnabled,RRoadCurveEnabled,rashDrivingEnabled,harshBrakeEnabled,
            LLaneChangeEnabled,RLaneChangeEnabled,roadHumpsEnabled,unevenRoadEnabled, contiuonusCurveEnabled,
            trafficLightEnabled,upwardSlopeEnabled,downSlopeEnabled,HLaneChangeEnabled,underPassEnabled,
            flyOverEnabled = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPreview = findViewById(R.id.surface_view);
        button = findViewById(R.id.btn);
        btnPotHole = findViewById(R.id.btn_pot_hole);
        btnUTurn = findViewById(R.id.btn_u_turn);
        btnLTurn = findViewById(R.id.btn_left_turn);
        btnRTurn = findViewById(R.id.btn_right_turn);
        btnZigZag = findViewById(R.id.btn_zig_zag);
        btnRoundAbout = findViewById(R.id.btn_round_about);
        btnLeftCurve = findViewById(R.id.btn_left_road_curve);
        btnRightCurve = findViewById(R.id.btn_right_road_curve);
        btnRashDriving = findViewById(R.id.btn_rash_drive);
        btnHarshBrake = findViewById(R.id.btn_harsh_brake);
        btnLeftLaneChange = findViewById(R.id.btn_left_lane_change);
        btnRightLaneChange = findViewById(R.id.btn_right_lane_change);
        btnRoadHumps = findViewById(R.id.btn_road_hump);
        btnUnevenRoad = findViewById(R.id.btn_uneven_road);
        btnContinousCurve = findViewById(R.id.btn_continous_curve);
        btnTrafficLight = findViewById(R.id.btn_traffic_light);
        btnUpSlope = findViewById(R.id.btn_upward_slope);
        btnDownSlope = findViewById(R.id.btn_downward_slope);
        btnUnderPass = findViewById(R.id.btn_underPass);
        btnFlyover = findViewById(R.id.btn_flyover);
        btnHarshLaneChange = findViewById(R.id.btn_harsh_lane_change);
        btnSetSpeed = findViewById(R.id.btn_set_speed);

        mSensorData = new SensorData(this);
        initStartButton();
        initButtonClick();
        checkSensorPresent();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        locationTracker = new OldLocationTracker(this);
        disableButtons();
    }

    private void initStartButton() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPermissionGranted()) {

                    if(locationTracker.canGetLocation()) {
                        recordClick();
                    }else{
                        Toast.makeText(mContext, "Please turn on your GPS to continue", Toast.LENGTH_LONG).show();
                    }
                }else{
                    ActivityCompat.requestPermissions((Activity) mContext,
                            new String[]{WRITE_EXTERNAL_STORAGE , READ_EXTERNAL_STORAGE, CAMERA, RECORD_AUDIO, ACCESS_FINE_LOCATION},
                            PERMISSION_REQUEST_CODE);
                }
            }
        });
    }

    private void startDataLog() {
        mSensorData.registerSensor();
        enableButtons();
    }

    private void stopDataLog() {
        mSensorData.unRegisterSensor();
        //mSensorData.finishTrip();
    }

    private boolean isPermissionGranted(){

        return (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(mContext, WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext,CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean storagePermissionAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraPermissionAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean audioPermissionAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean locationPermissionAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;

                    if (storagePermissionAccepted && cameraPermissionAccepted && audioPermissionAccepted &&
                            locationPermissionAccepted)
                        Toast.makeText(this,"Permission granted. Proceed now", Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(this,"Permission denied. Cannot proceed", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    private void recordClick(){

        if(isRecording) {

            if(isBtnEnabled()){
                Toast.makeText(this, "Please disable all the buttons",Toast.LENGTH_LONG).show();
                return;
            }

            mIsVideoStarted = false;
           /* try {
                mMediaRecorder.stop();  // stop the recording
            } catch (RuntimeException e) {
                Log.e(TAG, "RuntimeException: stop() is called immediately after start()");
                mOutputFile.delete();
            }*/
            stopDataLog();
//            releaseMediaRecorder();
//            mCamera.lock();
            button.setText("Start");
            isRecording = false;
//            releaseCamera();
            generateTextFile();
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();

        } else {
            Util.createFolder();
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if(null != vibrator){
                vibrator.vibrate(1000);
            }
            mIsVideoStarted = true;
            button.setText("Stop");
            new MediaPrepareTask().execute();
        }
    }

    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
            mCamera.lock();
        }
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();
            mCamera = null;
        }
    }

    private boolean prepareVideoRecorder(){

        /*mCamera = CameraHelper.getDefaultCameraInstance();
        mCamera.setDisplayOrientation(90);
        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
        List<Camera.Size> mSupportedVideoSizes = parameters.getSupportedVideoSizes();
        Camera.Size optimalSize = CameraHelper.getOptimalVideoSize(mSupportedVideoSizes,
                mSupportedPreviewSizes, mPreview.getWidth(), mPreview.getHeight());
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
        profile.videoFrameWidth = optimalSize.width;
        profile.videoFrameHeight = optimalSize.height;
        parameters.setPreviewSize(profile.videoFrameWidth, profile.videoFrameHeight);
        mCamera.setParameters(parameters);
        try {
            mCamera.setPreviewTexture(mPreview.getSurfaceTexture());
        } catch (IOException e) {
            Log.e(TAG, "Surface texture is unavailable or unsuitable" + e.getMessage());
            return false;
        }

        mMediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setProfile(profile);
        mOutputFile = CameraHelper.getOutputMediaFile(CameraHelper.MEDIA_TYPE_VIDEO);
        if (mOutputFile == null) {
            return false;
        }
        mMediaRecorder.setOutputFile(mOutputFile.getPath());
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d(TAG, "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d(TAG, "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }*/
        return true;
    }



    /**
     * Asynchronous task for preparing the {@link android.media.MediaRecorder} since it's a long blocking
     * operation.
     */
    class MediaPrepareTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (prepareVideoRecorder()) {
                //mMediaRecorder.start();
                isRecording = true;
                return true;
            } else {
                releaseMediaRecorder();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                finish();
            }
            startDataLog();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(mIsVideoStarted) {
            try {
                mMediaRecorder.stop();
            } catch (Exception e) {
                Log.e(TAG, "RuntimeException: stop() is called immediately after start()");
            }
            mMediaRecorder = null;
            stopDataLog();
            releaseMediaRecorder();
            button.setText("Start");
            isRecording = false;
            releaseCamera();
            generateTextFile();
            finish();
        }
    }

    private void checkSensorPresent(){

        SensorManager mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(mSensorManager != null){
            if(mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null){
                Toast.makeText(this,"Accelerometer sensor not present", Toast.LENGTH_LONG).show();
            }
            if(mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) == null){
                Toast.makeText(this,"Gyroscope sensor not present", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(locationTracker != null){
            locationTracker.stopUsingGPS();
        }
    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case R.id.btn_pot_hole:
                if(potHoleEnabled){
                    potHoleEnabled = false;
                    btnPotHole.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("POT_HOLE", Constants.POT_HOLE);
                }else{
                    potHoleEnabled = true;
                    btnPotHole.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("POT_HOLE",Constants.POT_HOLE);
                }
                break;
            case R.id.btn_u_turn:
                if(UTurnEnabled){
                    UTurnEnabled = false;
                    btnUTurn.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("U_TURN", Constants.U_TURN);
                }else{
                    UTurnEnabled = true;
                    btnUTurn.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("U_TURN",Constants.U_TURN);
                }
                break;
            case R.id.btn_left_turn:
                if(LTurnEnabled){
                    LTurnEnabled = false;
                    btnLTurn.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("LEFT_TURN", Constants.L_TURN);
                }else{
                    LTurnEnabled = true;
                    btnLTurn.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("LEFT_TURN",Constants.L_TURN);
                }
                break;
            case R.id.btn_right_turn:
                if(RTurnEnabled){
                    RTurnEnabled = false;
                    btnRTurn.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("RIGHT_TURN", Constants.R_TURN);
                }else{
                    RTurnEnabled = true;
                    btnRTurn.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("RIGHT_TURN",Constants.R_TURN);
                }
                break;
            case R.id.btn_zig_zag:
                if(zigZagEnabled){
                    zigZagEnabled = false;
                    btnZigZag.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("ZIG_ZAG_TURN", Constants.ZIG_ZAG);
                }else{
                    zigZagEnabled = true;
                    btnZigZag.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("ZIG_ZAG_TURN",Constants.ZIG_ZAG);
                }
                break;
            case R.id.btn_round_about:
                if(roundAboutEnabled){
                    roundAboutEnabled = false;
                    btnRoundAbout.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("ROUND_ABOUT", Constants.ROUND_ABOUT);
                }else{
                    roundAboutEnabled = true;
                    btnRoundAbout.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("ROUND_ABOUT",Constants.ROUND_ABOUT);
                }
                break;
            case R.id.btn_left_road_curve:
                if(LRoadCurveEnabled){
                    LRoadCurveEnabled = false;
                    btnLeftCurve.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("LEFT_ROAD_CURVE", Constants.L_ROAD_CURVE);
                }else{
                    LRoadCurveEnabled = true;
                    btnLeftCurve.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("LEFT_ROAD_CURVE",Constants.L_ROAD_CURVE);
                }
                break;
            case R.id.btn_right_road_curve:
                if(RRoadCurveEnabled){
                    RRoadCurveEnabled = false;
                    btnRightCurve.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("RIGHT_ROAD_CURVE", Constants.R_ROAD_CURVE);
                }else{
                    RRoadCurveEnabled = true;
                    btnRightCurve.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("RIGHT_ROAD_CURVE",Constants.R_ROAD_CURVE);
                }
                break;
            case R.id.btn_rash_drive:
                if(rashDrivingEnabled){
                    rashDrivingEnabled = false;
                    btnRashDriving.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("RASH_DRIVING", Constants.RASH_DRIVING);
                }else{
                    rashDrivingEnabled = true;
                    btnRashDriving.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("RASH_DRIVING",Constants.RASH_DRIVING);
                }
                break;
            case R.id.btn_harsh_brake:
                if(harshBrakeEnabled){
                    harshBrakeEnabled = false;
                    btnHarshBrake.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("HARSH_BRAKE", Constants.HARSH_BRAKING);
                }else{
                    harshBrakeEnabled = true;
                    btnHarshBrake.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("HARSH_BRAKE",Constants.HARSH_BRAKING);
                }
                break;
            case R.id.btn_left_lane_change:
                if(LLaneChangeEnabled){
                    LLaneChangeEnabled = false;
                    btnLeftLaneChange.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("LEFT_LANE_CHANGE", Constants.L_LANE_CHANGE);
                }else{
                    LLaneChangeEnabled = true;
                    btnLeftLaneChange.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("LEFT_LANE_CHANGE",Constants.L_LANE_CHANGE);
                }
                break;
            case R.id.btn_right_lane_change:
                if(RLaneChangeEnabled){
                    RLaneChangeEnabled = false;
                    btnRightLaneChange.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("RIGHT_LANE_CHANGE", Constants.R_LANE_CHANGE);
                }else{
                    RLaneChangeEnabled = true;
                    btnRightLaneChange.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("RIGHT_LANE_CHANGE",Constants.R_LANE_CHANGE);
                }
                break;
            case R.id.btn_road_hump:
                if(roadHumpsEnabled){
                    roadHumpsEnabled = false;
                    btnRoadHumps.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("ROAD_HUMP", Constants.ROAD_HUMPS);
                }else{
                    roadHumpsEnabled = true;
                    btnRoadHumps.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("ROAD_HUMP",Constants.ROAD_HUMPS);
                }
                break;
            case R.id.btn_uneven_road:
                if(unevenRoadEnabled){
                    unevenRoadEnabled = false;
                    btnUnevenRoad.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("UNEVEN_ROAD", Constants.UNEVEN_ROAD);
                }else{
                    unevenRoadEnabled = true;
                    btnUnevenRoad.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("UNEVEN_ROAD",Constants.UNEVEN_ROAD);
                }
                break;
            case R.id.btn_continous_curve:
                if(contiuonusCurveEnabled){
                    contiuonusCurveEnabled = false;
                    btnContinousCurve.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("CONTINUOUS_CURVE", Constants.CONTIUNOUS_CURVE);
                }else{
                    contiuonusCurveEnabled = true;
                    btnContinousCurve.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("CONTINUOUS_CURVE",Constants.CONTIUNOUS_CURVE);
                }
                break;
            case R.id.btn_traffic_light:
                if(trafficLightEnabled){
                    trafficLightEnabled = false;
                    btnTrafficLight.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("TRAFFIC_LIGHT", Constants.TRAFFIC_LIGHT);
                }else{
                    trafficLightEnabled = true;
                    btnTrafficLight.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("TRAFFIC_LIGHT",Constants.TRAFFIC_LIGHT);
                }
                break;
            case R.id.btn_upward_slope:
                if(upwardSlopeEnabled){
                    upwardSlopeEnabled = false;
                    btnUpSlope.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("UPWARD_SLOPE", Constants.UPWARD_SLOPE);
                }else{
                    upwardSlopeEnabled = true;
                    btnUpSlope.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("UPWARD_SLOPE",Constants.UPWARD_SLOPE);
                }
                break;
            case R.id.btn_downward_slope:
                if(downSlopeEnabled){
                    downSlopeEnabled = false;
                    btnDownSlope.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("DOWN_SLOPE", Constants.DOWN_SLOPE);
                }else{
                    downSlopeEnabled = true;
                    btnDownSlope.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("DOWN_SLOPE",Constants.DOWN_SLOPE);
                }
                break;
            case R.id.btn_underPass:
                if(underPassEnabled){
                    underPassEnabled = false;
                    btnUnderPass.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("UNDERPASS", Constants.UNDER_PASS);
                }else{
                    underPassEnabled = true;
                    btnUnderPass.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("UNDERPASS",Constants.UNDER_PASS);
                }
                break;
            case R.id.btn_flyover:
                if(flyOverEnabled){
                    flyOverEnabled = false;
                    btnFlyover.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("FLY_OVER", Constants.FLY_OVER);
                }else{
                    flyOverEnabled = true;
                    btnFlyover.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("FLY_OVER",Constants.FLY_OVER);
                }
                break;
            case R.id.btn_harsh_lane_change:
                if(HLaneChangeEnabled){
                    HLaneChangeEnabled = false;
                    btnHarshLaneChange.setBackground(getResources().getDrawable(R.drawable.grey_bng));
                    if(mSensorData != null)mSensorData.buttonEventStopped("HARSH_LANE_CHANGE", Constants.H_LANE_CHANGE);
                }else{
                    HLaneChangeEnabled = true;
                    btnHarshLaneChange.setBackground(getResources().getDrawable(R.drawable.blue_bng));
                    if(mSensorData != null)mSensorData.buttonEventTriggered("HARSH_LANE_CHANGE",Constants.H_LANE_CHANGE);
                }
                break;
            case R.id.btn_set_speed:
                speedSetDialog();
                break;
        }
    }


    private boolean isBtnEnabled(){

        return potHoleEnabled || UTurnEnabled || LTurnEnabled || RTurnEnabled ||zigZagEnabled ||
                roundAboutEnabled || LRoadCurveEnabled || RRoadCurveEnabled || rashDrivingEnabled ||
                harshBrakeEnabled || LLaneChangeEnabled || RLaneChangeEnabled || roadHumpsEnabled || unevenRoadEnabled ||
                contiuonusCurveEnabled || trafficLightEnabled || upwardSlopeEnabled || downSlopeEnabled || HLaneChangeEnabled ||
                underPassEnabled || flyOverEnabled;
    }



    private void btnClick(Button button,String name,int type){

        if(mIsButtonClick){
            mIsButtonClick = false;
            button.setBackground(getResources().getDrawable(R.drawable.grey_bng));
            //enableButtons();
            if(mSensorData != null)mSensorData.buttonEventStopped(name,type);
        }else{
            mIsButtonClick = true;
            button.setBackground(getResources().getDrawable(R.drawable.blue_bng));
            //disableButtons();
            button.setEnabled(true);
            if(mSensorData != null)mSensorData.buttonEventTriggered(name,type);
        }
    }


    private void initButtonClick(){

        btnPotHole.setOnClickListener(this);
        btnUTurn.setOnClickListener(this);
        btnLTurn.setOnClickListener(this);
        btnRTurn.setOnClickListener(this);
        btnZigZag.setOnClickListener(this);
        btnRoundAbout.setOnClickListener(this);
        btnLeftCurve.setOnClickListener(this);
        btnRightCurve.setOnClickListener(this);
        btnRashDriving.setOnClickListener(this);
        btnHarshBrake.setOnClickListener(this);
        btnLeftLaneChange.setOnClickListener(this);
        btnRightLaneChange.setOnClickListener(this);
        btnRoadHumps.setOnClickListener(this);
        btnUnevenRoad.setOnClickListener(this);
        btnContinousCurve.setOnClickListener(this);
        btnTrafficLight.setOnClickListener(this);
        btnUpSlope.setOnClickListener(this);
        btnDownSlope.setOnClickListener(this);
        btnUnderPass.setOnClickListener(this);
        btnFlyover.setOnClickListener(this);
        btnHarshLaneChange.setOnClickListener(this);
        btnSetSpeed.setOnClickListener(this);
    }

    private void disableButtons(){

        btnPotHole.setEnabled(false);
        btnUTurn.setEnabled(false);
        btnLTurn.setEnabled(false);
        btnRTurn.setEnabled(false);
        btnZigZag.setEnabled(false);
        btnRoundAbout.setEnabled(false);
        btnLeftCurve.setEnabled(false);
        btnRightCurve.setEnabled(false);
        btnRashDriving.setEnabled(false);
        btnHarshBrake.setEnabled(false);
        btnLeftLaneChange.setEnabled(false);
        btnRightLaneChange.setEnabled(false);
        btnRoadHumps.setEnabled(false);
        btnUnevenRoad.setEnabled(false);
        btnContinousCurve.setEnabled(false);
        btnTrafficLight.setEnabled(false);
        btnUpSlope.setEnabled(false);
        btnDownSlope.setEnabled(false);
        btnUnderPass.setEnabled(false);
        btnFlyover.setEnabled(false);
        btnHarshLaneChange.setEnabled(false);
    }

    private void enableButtons(){

        btnPotHole.setEnabled(true);
        btnUTurn.setEnabled(true);
        btnLTurn.setEnabled(true);
        btnRTurn.setEnabled(true);
        btnZigZag.setEnabled(true);
        btnRoundAbout.setEnabled(true);
        btnLeftCurve.setEnabled(true);
        btnRightCurve.setEnabled(true);
        btnRashDriving.setEnabled(true);
        btnHarshBrake.setEnabled(true);
        btnLeftLaneChange.setEnabled(true);
        btnRightLaneChange.setEnabled(true);
        btnRoadHumps.setEnabled(true);
        btnUnevenRoad.setEnabled(true);
        btnContinousCurve.setEnabled(true);
        btnTrafficLight.setEnabled(true);
        btnUpSlope.setEnabled(true);
        btnDownSlope.setEnabled(true);
        btnUnderPass.setEnabled(true);
        btnFlyover.setEnabled(true);
        btnHarshLaneChange.setEnabled(true);
    }


    private void speedSetDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.speed_set_dialog);
        dialog.setTitle("Set Speed Threshold");
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        Button ok = dialog.findViewById(R.id.dialog_btn_ok);
        final EditText speedEdt = dialog.findViewById(R.id.dialog_edt_speed);
        dialog.show();

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(speedEdt.length() >0){
                    int value = Integer.parseInt(speedEdt.getText().toString().trim());
                    SharedPreferenceManager.write(SharedPrefConstants.SPEED_THRESHOLD,value);
                }
                dialog.dismiss();
            }
        });
    }


    private void generateTextFile() {

        String harshBrake = "Harsh Brake Count :" + SharedPreferenceManager.read(SharedPrefConstants.HARSH_BRAKE);
        String harshLaneChange = "Harsh Lane Change Count :" + SharedPreferenceManager.read(SharedPrefConstants.HARSH_LANE_CHANGE);
        String LRChange = "L-R Turn Count :" + SharedPreferenceManager.read(SharedPrefConstants.LR_TURN_CHANGE);
        String UTurn = "U Turn Count :" + SharedPreferenceManager.read(SharedPrefConstants.U_TURN_CHANGE);
        String normalLaneChange = "Normal Lane Change Count :" + SharedPreferenceManager.read(SharedPrefConstants.NORMAL_LANE_CHANGE);
        String overSpeeding = "Over Speeding Count :" + SharedPreferenceManager.read(SharedPrefConstants.OVER_SPEEDING);
        String speedLimitSet = "Speed limit set for the trip :" + SharedPreferenceManager.read(SharedPrefConstants.SPEED_THRESHOLD);

        File file = new File(Util.createAndGetFolderPath() + "/EventsCountFile.txt");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception e) {
                Log.e(TAG, "^^^^^^^^ exception 1 boss:"+e.toString());
            }
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
            writer.append(harshBrake + "\n");
            writer.append(harshLaneChange + "\n");
            writer.append(LRChange + "\n");
            writer.append(UTurn + "\n");
            writer.append(normalLaneChange + "\n");
            writer.append(overSpeeding + "\n");
            writer.append(speedLimitSet);
            writer.close();
            fileOutputStream.close();
        } catch (Exception e) {
            Log.e(TAG, "^^^^^^^^ exception 2 boss :"+e.toString());
        }
    }
}
