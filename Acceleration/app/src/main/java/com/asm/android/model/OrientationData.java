package com.asm.android.model;

/**
 * @author Vikash Sharma
 *
 */
public class OrientationData {

	private double azimuth;
	private double yaw;
	private double pitch;
	private double roll;
	private double azimuthDeviation;
	private double pitchDeviation;
	private double rollDeviation;
	private double timestamp;

	/**
	 * 
	 * @param azimuth
	 * @param yaw
	 * @param pitch
	 * @param roll
	 * @param azimuthDeviation
	 * @param pitchDeviation
	 * @param rollDeviation
	 * @param timestamp
	 */
	public OrientationData(double azimuth, double yaw, double pitch, double roll, double azimuthDeviation,
			double pitchDeviation, double rollDeviation, double timestamp) {
		this.azimuth = azimuth;
		this.yaw = yaw;
		this.pitch = pitch;
		this.roll = roll;
		this.azimuthDeviation = azimuthDeviation;
		this.pitchDeviation = pitchDeviation;
		this.rollDeviation = rollDeviation;
		this.timestamp = timestamp;

	}

	public OrientationData() {
	}
	/**
	 * setting azimuth angle from 0 to 360
	 * 
	 * @param azimuth
	 */
	public void setAzimuth(double azimuth) {
		this.azimuth = azimuth;
	}

	/**
	 * getting azimuth value
	 * 
	 * @return double azimuth value
	 */
	public double getAzimuth() {
		return azimuth;
	}

	/**
	 * Setting yaw value from -180 to 180
	 * 
	 * @param yaw
	 */
	public void setYaw(double yaw) {
		this.yaw = yaw;
	}

	/**
	 * getting yaw value of the orientation sensor
	 * 
	 * @return
	 */
	public double getYaw() {
		return yaw;
	}

	/**
	 * Setting pitch value for the device position inside the vehicle it can be
	 * from 0 to 90
	 * 
	 * @param pitch
	 */
	public void setPitch(double pitch) {
		this.pitch = pitch;
	}

	/**
	 * 
	 * @return double pitch value
	 */
	public double getPitch() {
		return pitch;
	}

	/**
	 * Setting the Roll of the data it can also be from 0 to 90
	 * 
	 * @param roll
	 */
	public void setRoll(double roll) {
		this.roll = roll;
	}

	/**
	 * Getting the roll the data
	 * 
	 * @return
	 */
	public double getRoll() {
		return roll;
	}

	/**
	 * Setting Deviation from the previous angle
	 * 
	 * @param azimuthDeviation
	 */
	public void setAzimuthDeviation(double azimuthDeviation) {
		this.azimuthDeviation = azimuthDeviation;
	}

	/**
	 * Getting azimuth deviation at any point of time for the delta theta
	 * displacements
	 * 
	 * @return
	 */
	public double getAzimuthDeviation() {
		return azimuthDeviation;
	}

	/**
	 * Setting the pitch deviation to check the displacement of the phone
	 * 
	 * @param pitchDeviation
	 */
	public void setPitchDeviation(double pitchDeviation) {
		this.pitchDeviation = pitchDeviation;
	}

	/**
	 * 
	 * @return double pitchDeviation
	 */
	public double getPitchDeviation() {
		return pitchDeviation;
	}

	/**
	 * setting roll deviation
	 * 
	 * @param rollDeviation
	 */
	public void setRollDeviation(double rollDeviation) {
		this.rollDeviation = rollDeviation;
	}

	/**
	 * Roll Deviation
	 * 
	 * @return double
	 */
	public double getRollDeviation() {
		return rollDeviation;
	}

	/**
	 * setting timestamp values
	 * 
	 * @param timestamp
	 */
	public void setTimestamp(double timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * getting timestamp values
	 * 
	 * @return
	 */
	public double getTimestamp() {
		return timestamp;
	}

}
