package com.asm.android.dataLogger;

import android.content.Context;

import com.asm.android.constant.Constants;
import com.asm.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * Created by Sourabh on 27-03-2018.
 */

public class DataLoggerAudioEvent {

    private static final String TAG = DataLoggerAudioEvent.class.getSimpleName();

    private ArrayList<String> csvHeaders;

    private DataLoggerInterface dataLogger;

    public DataLoggerAudioEvent(Context context) {

        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<String> value){
        dataLogger.addRow(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return Constants.INDICATOR_EVENT_FILE_NAME;
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("Start Time");
        headers.add("End Time");
        headers.add("Latitude");
        headers.add("Longitude");
        headers.add("Speed");
        headers.add("Bearing");
        headers.add("Event");
        headers.add("TimeStampInMills");
        return headers;
    }
}
