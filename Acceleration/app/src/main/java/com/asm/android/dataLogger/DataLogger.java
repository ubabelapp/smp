package com.asm.android.dataLogger;

import android.content.Context;

import com.asm.android.constant.Constants;
import com.asm.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * Created by Sourabh on 14-02-2018.
 */

public class DataLogger {

    private static final String TAG = DataLogger.class.getSimpleName();

    private ArrayList<String> csvHeaders;

    private DataLoggerInterface dataLogger;

    public DataLogger(Context context) {
        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<Float> value){
        dataLogger.addRowData(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return Constants.SENSOR_DATA_FILE_NAME;
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("AccTimeInMillis");
        headers.add("Acc-X-value");
        headers.add("Acc-Y-value");
        headers.add("Acc-Z-value");
        headers.add("Gyro-X-value(ROLL)");
        headers.add("Gyro-Y-value(PITCH)");
        headers.add("Gyro-Z-value(YAW)");
        headers.add("Linear-Acc-X-value");
        headers.add("Linear-Acc-Y-value");
        headers.add("Linear-Acc-Z-value");
        headers.add("Gravity X-value");
        headers.add("Gravity Y-value");
        headers.add("Gravity Z-value");
        headers.add("Azimuth");
        headers.add("Roll");
        headers.add("Pitch");
        headers.add("Yaw");
        headers.add("YawInRadian");
        headers.add("Deviation");
        headers.add("Latitude");
        headers.add("Longitude");
        headers.add("Altitude");
        headers.add("Speed");
        headers.add("Magnitude");
        headers.add("Bearing");
        return headers;
    }
}

