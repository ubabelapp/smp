package com.asm.android.location;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/*
 * Created by Sourabh on 20-08-2016.
 */
public class OldLocationTracker implements LocationListener {

    private final Context mContext;
    private boolean canGetLocation = false;
    private boolean isPermissionGranted = false;

    private Location location = null;
    private double latitude;
    private double longitude;
    private double speed;

    private LocationManager locationManager;

    public OldLocationTracker(Context context) {
        this.mContext = context;
        getLocation();
    }

    private void getLocation() {

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(mContext, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                isPermissionGranted = false;
        }else{

            isPermissionGranted =true;
            try {
                locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

                boolean isGPSEnabled = false;
                boolean isNetworkEnabled = false;
                if (locationManager != null) {
                    isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                    isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                }

                if (!isGPSEnabled && !isNetworkEnabled) {
                    this.canGetLocation = false;
                } else {
                    this.canGetLocation = true;
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                speed = location.getSpeed();
                            }
                        }
                    }
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                    speed = location.getSpeed();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Function to get latitude
     * */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public double getSpeed() {
        if (location != null) {
            speed = location.getSpeed();
        }
        return speed;
    }


    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public boolean isPermissionGranted() {
        return this.isPermissionGranted;
    }

    @Override
    public void onLocationChanged(Location location) {

        //Log.e("TAG", "*************** onLocationChanged *******");
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            speed = location.getSpeed();
        }
    }

    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

}




