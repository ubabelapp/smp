/*
 * DtwTest.java   Jul 14, 2004
 *
 * Copyright (c) 2004 Stan Salvador
 * stansalvador@hotmail.com
 */

package com.asm.android.Fdtw;


import com.asm.android.Fdtw.dtw.DTW;
import com.asm.android.Fdtw.dtw.TimeWarpInfo;
import com.asm.android.Fdtw.timeseries.TimeSeries;
import com.asm.android.Fdtw.util.DistanceFunction;
import com.asm.android.Fdtw.util.DistanceFunctionFactory;

public class DtwTest
{

   // PUBLIC FUNCTIONS
   public static void main(String[] args)
   {
	   
	   args = new String[3];
 	  args[0] = "C:/Users/rainbow/drive/fastDtw/trace0.csv";
 	  args[1] = "C:/Users/rainbow/drive/fastDtw/trace1.csv";
 	  args[2] = "ManhattanDistance";
	   
      if (args.length!=2 && args.length!=3)
      {
         System.out.println("USAGE:  java DtwTest timeSeries1 timeSeries2 [EuclideanDistance|ManhattanDistance|BinaryDistance]");
         System.exit(1);
      }
      else
      {
         final TimeSeries tsI = new TimeSeries(args[0], false, false, ',');
         final TimeSeries tsJ = new TimeSeries(args[1], false, false, ',');
         
         final DistanceFunction distFn;
         if (args.length < 3)
         {
            distFn = DistanceFunctionFactory.getDistFnByName("EuclideanDistance");
         }
         else
         {
            distFn = DistanceFunctionFactory.getDistFnByName(args[2]);
         }   // end if
         
         final TimeWarpInfo info = DTW.getWarpInfoBetween(tsI, tsJ, distFn);

         System.out.println("Warp Distance: " + info.getDistance());
         System.out.println("Warp Path:     " + info.getPath());
      }  // end if

   }  // end main()

}  // end class DtwTest
