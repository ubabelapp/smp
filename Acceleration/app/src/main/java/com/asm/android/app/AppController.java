package com.asm.android.app;

import android.app.Application;

import com.asm.android.utils.SharedPreferenceManager;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/*
 * Created by sourabh.n on 13-06-2017.
 */

public class AppController extends Application {

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        SharedPreferenceManager.init(getApplicationContext());
        Fabric.with(this, new Crashlytics());
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }
}
