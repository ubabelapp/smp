package com.asm.android.async;


public interface AsyncCallback<S,E extends ErrorContext> {
    void onSuccess(S successContext);
    void onError(E errorContext);
}
