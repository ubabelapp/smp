package com.asm.android.async;


public class AsyncCallbackUtil {
    private AsyncCallbackUtil(){

    }

    public static <S,E extends ErrorContext> AsyncCallback<S, E> makeQuiet(AsyncCallback<S, E> asyncCallback){
        if(asyncCallback instanceof QuietAsyncCallback){
            return asyncCallback;
        }

        return new QuietAsyncCallback<>(asyncCallback);
    }

}
