package com.asm.android.utils;

import com.asm.android.constant.Constants;
import com.asm.android.model.AccelerometerData;
import com.asm.android.model.EventData;
import com.asm.android.model.GyroscopeData;

import java.util.ArrayList;
import java.util.List;



/**
 *
 * Created by Vikash Sharma on 20-02-2018.
 */

public class ResamplingData {

    private static final String TAG = ResamplingData.class.getSimpleName();


    public static List<GyroscopeData> reSampleGyroDataModel(List<GyroscopeData> dataToResample, float sensorFrequency, int desiredFrequency){

        if(sensorFrequency == desiredFrequency || sensorFrequency<desiredFrequency){
            return dataToResample;
        }
        List<GyroscopeData> gyroscopeList = new ArrayList<>();
        int dataPoints = (int)Math.ceil(sensorFrequency)/desiredFrequency;

        for(int i=0;i<dataToResample.size();i++){
            if(i%dataPoints==0){
                gyroscopeList.add(dataToResample.get(i));
            }
        }
        return gyroscopeList;
    }


    public static List<AccelerometerData> reSampleAccelerometerDataModel(List<AccelerometerData> dataToResample, float sensorFrequency, int desiredFrequency){

        if(sensorFrequency == desiredFrequency || sensorFrequency<desiredFrequency){
            return dataToResample;
        }
        List<AccelerometerData> accelerometerList = new ArrayList<>();
        int dataPoints = (int)Math.ceil(sensorFrequency)/desiredFrequency;

        for(int i=0;i<dataToResample.size();i++){
            if(i%dataPoints==0){
                accelerometerList.add(dataToResample.get(i));
            }
        }
        return accelerometerList;
    }

    public static List<EventData> reSampleEventDataModel(List<EventData> dataToResample, float sensorFrequency, int desiredFrequency){

        if(sensorFrequency == desiredFrequency || sensorFrequency<desiredFrequency){
            return dataToResample;
        }
        List<EventData> eventList = new ArrayList<>();
        int dataPoints = (int)Math.ceil(sensorFrequency)/desiredFrequency;

        for(int i=0;i<dataToResample.size();i++){
            if(i%dataPoints==0){
                eventList.add(dataToResample.get(i));
            }
        }
        return eventList;
    }


}
