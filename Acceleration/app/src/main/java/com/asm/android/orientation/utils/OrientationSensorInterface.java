package com.asm.android.orientation.utils;

/*
 * Created by majidGolshadi on 9/22/2014.
 */
public interface OrientationSensorInterface {

    void orientation(Double AZIMUTH, Double PITCH, Double ROLL, Double YAW);
    void orientationYawData(String Yaw, String Roll, String Pitch);
}
