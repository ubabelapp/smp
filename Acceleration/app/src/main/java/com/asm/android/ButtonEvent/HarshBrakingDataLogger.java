package com.asm.android.ButtonEvent;

import android.content.Context;

import com.asm.android.dataLogger.CsvDataLogger;
import com.asm.android.dataLogger.DataLoggerButtonEvent;
import com.asm.android.dataLogger.DataLoggerInterface;
import com.asm.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sourabh on 30-03-2018.
 */

public class HarshBrakingDataLogger {

    private static final String TAG = DataLoggerButtonEvent.class.getSimpleName();

    private ArrayList<String> csvHeaders;

    private DataLoggerInterface dataLogger;

    private String mFileName;

    public HarshBrakingDataLogger(Context context, String fileName) {

        this.mFileName = fileName;
        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<Float> value){
        dataLogger.addRowData(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return mFileName+"_"+System.currentTimeMillis()+".csv";
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("Acc-X-value");
        headers.add("Acc-Y-value");
        headers.add("Acc-Z-value");
        headers.add("Gyro-X-value");
        headers.add("Gyro-Y-value");
        headers.add("Gyro-Z-value");
        headers.add("Azimuth");
        headers.add("Roll");
        headers.add("Pitch");
        headers.add("Speed");
        headers.add("latitude");
        headers.add("longitude");
        headers.add("bearing");
        headers.add("deviation");
        headers.add("energy");
        return headers;
    }
}
