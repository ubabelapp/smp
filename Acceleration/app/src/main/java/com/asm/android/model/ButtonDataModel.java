package com.asm.android.model;

/**
 * Created by Sourabh on 31-01-2018.
 */

public class ButtonDataModel {

    private Float timeStamp;
    private Float accXValue;
    private Float accYValue;
    private Float accZValue;
    private Float gyroXValue;
    private Float gyroYValue;
    private Float gyroZValue;
    private Float gyroRotationVelocity;
    private Float deltaQuaternionXAxis;
    private Float deltaQuaternionYAxis;
    private Float deltaQuaternionZAxis;
    private Float magnitude;
    private Float azimuth;
    private Float roll;
    private Float pitch;
    private Float deviation;
    private Float latitude;
    private Float longitude;
    private Float speed;
    private Float currentSpeed;
    private Float currentAcceleration;
    private Float CurrentDistanceTravelled;
    private Float currentDirection;
    private Float linearAccXValue;
    private Float linearAccYValue;
    private Float linearAccZValue;
    private Float totalDistance;
    private Float deltaTime;
    private Float yawRadians;
    private Float yawDeviation;
    private Float gyroFilteredXValue;
    private Float gyroFilteredYValue;
    private Float gyroFilteredZValue;

    public Float getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Float timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Float getAccXValue() {
        return accXValue;
    }

    public void setAccXValue(Float accXValue) {
        this.accXValue = accXValue;
    }

    public Float getAccYValue() {
        return accYValue;
    }

    public void setAccYValue(Float accYValue) {
        this.accYValue = accYValue;
    }

    public Float getAccZValue() {
        return accZValue;
    }

    public void setAccZValue(Float accZValue) {
        this.accZValue = accZValue;
    }

    public Float getGyroXValue() {
        return gyroXValue;
    }

    public void setGyroXValue(Float gyroXValue) {
        this.gyroXValue = gyroXValue;
    }

    public Float getGyroYValue() {
        return gyroYValue;
    }

    public void setGyroYValue(Float gyroYValue) {
        this.gyroYValue = gyroYValue;
    }

    public Float getGyroZValue() {
        return gyroZValue;
    }

    public void setGyroZValue(Float gyroZValue) {
        this.gyroZValue = gyroZValue;
    }

    public Float getGyroRotationVelocity() {
        return gyroRotationVelocity;
    }

    public void setGyroRotationVelocity(Float gyroRotationVelocity) {
        this.gyroRotationVelocity = gyroRotationVelocity;
    }

    public Float getDeltaQuaternionXAxis() {
        return deltaQuaternionXAxis;
    }

    public void setDeltaQuaternionXAxis(Float deltaQuaternionXAxis) {
        this.deltaQuaternionXAxis = deltaQuaternionXAxis;
    }

    public Float getDeltaQuaternionYAxis() {
        return deltaQuaternionYAxis;
    }

    public void setDeltaQuaternionYAxis(Float deltaQuaternionYAxis) {
        this.deltaQuaternionYAxis = deltaQuaternionYAxis;
    }

    public Float getDeltaQuaternionZAxis() {
        return deltaQuaternionZAxis;
    }

    public void setDeltaQuaternionZAxis(Float deltaQuaternionZAxis) {
        this.deltaQuaternionZAxis = deltaQuaternionZAxis;
    }

    public Float getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(Float magnitude) {
        this.magnitude = magnitude;
    }

    public Float getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(Float azimuth) {
        this.azimuth = azimuth;
    }

    public Float getRoll() {
        return roll;
    }

    public void setRoll(Float roll) {
        this.roll = roll;
    }

    public Float getPitch() {
        return pitch;
    }

    public void setPitch(Float pitch) {
        this.pitch = pitch;
    }

    public Float getDeviation() {
        return deviation;
    }

    public void setDeviation(Float deviation) {
        this.deviation = deviation;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Float getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(Float currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public Float getCurrentAcceleration() {
        return currentAcceleration;
    }

    public void setCurrentAcceleration(Float currentAcceleration) {
        this.currentAcceleration = currentAcceleration;
    }

    public Float getCurrentDistanceTravelled() {
        return CurrentDistanceTravelled;
    }

    public void setCurrentDistanceTravelled(Float currentDistanceTravelled) {
        CurrentDistanceTravelled = currentDistanceTravelled;
    }

    public Float getCurrentDirection() {
        return currentDirection;
    }

    public void setCurrentDirection(Float currentDirection) {
        this.currentDirection = currentDirection;
    }

    public Float getLinearAccXValue() {
        return linearAccXValue;
    }

    public void setLinearAccXValue(Float linearAccXValue) {
        this.linearAccXValue = linearAccXValue;
    }

    public Float getLinearAccYValue() {
        return linearAccYValue;
    }

    public void setLinearAccYValue(Float linearAccYValue) {
        this.linearAccYValue = linearAccYValue;
    }

    public Float getLinearAccZValue() {
        return linearAccZValue;
    }

    public void setLinearAccZValue(Float linearAccZValue) {
        this.linearAccZValue = linearAccZValue;
    }

    public Float getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Float totalDistance) {
        this.totalDistance = totalDistance;
    }

    public Float getDeltaTime() {
        return deltaTime;
    }

    public void setDeltaTime(Float deltaTime) {
        this.deltaTime = deltaTime;
    }

    public Float getYawRadians() {
        return yawRadians;
    }

    public void setYawRadians(Float yawRadians) {
        this.yawRadians = yawRadians;
    }

    public Float getYawDeviation() {
        return yawDeviation;
    }

    public void setYawDeviation(Float yawDeviation) {
        this.yawDeviation = yawDeviation;
    }

    public Float getGyroFilteredXValue() {
        return gyroFilteredXValue;
    }

    public void setGyroFilteredXValue(Float gyroFilteredXValue) {
        this.gyroFilteredXValue = gyroFilteredXValue;
    }

    public Float getGyroFilteredYValue() {
        return gyroFilteredYValue;
    }

    public void setGyroFilteredYValue(Float gyroFilteredYValue) {
        this.gyroFilteredYValue = gyroFilteredYValue;
    }

    public Float getGyroFilteredZValue() {
        return gyroFilteredZValue;
    }

    public void setGyroFilteredZValue(Float gyroFilteredZValue) {
        this.gyroFilteredZValue = gyroFilteredZValue;
    }

    public ButtonDataModel(Float timeStamp, Float accXValue, Float accYValue, Float accZValue,
                           Float gyroXValue, Float gyroYValue, Float gyroZValue, Float gyroRotationVelocity,
                           Float deltaQuaternionXAxis, Float deltaQuaternionYAxis, Float deltaQuaternionZAxis,
                           Float magnitude, Float azimuth, Float roll, Float pitch, Float deviation,
                           Float latitude, Float longitude, Float speed, Float currentSpeed,
                           Float currentAcceleration, Float currentDistanceTravelled, Float currentDirection,
                           Float linearAccXValue, Float linearAccYValue, Float linearAccZValue,
                           Float totalDistance, Float deltaTime, Float yawRadians, Float yawDeviation,
                           Float gyroFilteredXValue, Float gyroFilteredYValue, Float gyroFilteredZValue) {
        this.timeStamp = timeStamp;
        this.accXValue = accXValue;
        this.accYValue = accYValue;
        this.accZValue = accZValue;
        this.gyroXValue = gyroXValue;
        this.gyroYValue = gyroYValue;
        this.gyroZValue = gyroZValue;
        this.gyroRotationVelocity = gyroRotationVelocity;
        this.deltaQuaternionXAxis = deltaQuaternionXAxis;
        this.deltaQuaternionYAxis = deltaQuaternionYAxis;
        this.deltaQuaternionZAxis = deltaQuaternionZAxis;
        this.magnitude = magnitude;
        this.azimuth = azimuth;
        this.roll = roll;
        this.pitch = pitch;
        this.deviation = deviation;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.currentSpeed = currentSpeed;
        this.currentAcceleration = currentAcceleration;
        CurrentDistanceTravelled = currentDistanceTravelled;
        this.currentDirection = currentDirection;
        this.linearAccXValue = linearAccXValue;
        this.linearAccYValue = linearAccYValue;
        this.linearAccZValue = linearAccZValue;
        this.totalDistance = totalDistance;
        this.deltaTime = deltaTime;
        this.yawRadians = yawRadians;
        this.yawDeviation = yawDeviation;
        this.gyroFilteredXValue = gyroFilteredXValue;
        this.gyroFilteredYValue = gyroFilteredYValue;
        this.gyroFilteredZValue = gyroFilteredZValue;
    }
}
