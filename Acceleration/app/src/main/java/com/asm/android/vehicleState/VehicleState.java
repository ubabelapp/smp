package com.asm.android.vehicleState;

import android.util.Log;

import com.asm.android.constant.StateType;
import com.asm.android.filterVO.StationaryFilterVO;
import com.asm.android.utils.SharedPreferenceManager;
import com.asm.android.utils.Util;

import static com.asm.android.constant.Constants.FIRST_STATIONARY_OBJECT;
import static com.asm.android.constant.Constants.STATIONARY_EXPCT_THRESHOLD;
import static com.asm.android.constant.Constants.STATIONARY_MIN_THRESHOLD;
import static com.asm.android.constant.SharedPrefConstants.STATIONARY_LATITUDE;
import static com.asm.android.constant.SharedPrefConstants.STATIONARY_LONGITUDE;
import static com.asm.android.constant.SharedPrefConstants.STATIONARY_TIME_START;
import static com.asm.android.constant.SharedPrefConstants.VEHICLE_STATE;

/**
 * Created by VIKASH SHARMA on 23-02-2018.
 */

public class VehicleState {

    public static final String TAG = VehicleState.class.getSimpleName();
    private static final int STATIONARY = 1;
    private static final int MOVING = 3;
    private static final int EXPECTED_MOVEMENT = 4;
    private static final int EXPECTED_STATIONARY = 2;

    public int getState(){

        switch (SharedPreferenceManager.read(VEHICLE_STATE)){
            case STATIONARY:
                break;
            case MOVING:
                break;
            case EXPECTED_MOVEMENT:
                break;
            case EXPECTED_STATIONARY:

                break;
            default:
                break;
        }
        return 0;
    }

    public void updateVehicleState(boolean isMovingNow){
        if(isMovingNow){
            if(getTimestampOfVehicleState()>0){
                SharedPreferenceManager.writeLong(STATIONARY_TIME_START,0L);
                SharedPreferenceManager.write(VEHICLE_STATE,StateType.getByStateID(MOVING).name());
            }else{
                SharedPreferenceManager.write(VEHICLE_STATE,StateType.getByStateID(MOVING).name());
            }
        }else{
             if((System.currentTimeMillis()-getTimestampOfVehicleState())>STATIONARY_MIN_THRESHOLD){
                 SharedPreferenceManager.write(VEHICLE_STATE,StateType.getByStateID(STATIONARY).name());
            }else if((System.currentTimeMillis()-getTimestampOfVehicleState())>STATIONARY_EXPCT_THRESHOLD){
                 SharedPreferenceManager.write(VEHICLE_STATE,StateType.getByStateID(EXPECTED_MOVEMENT).name());
             }else if(getTimestampOfVehicleState()==0){
                 SharedPreferenceManager.writeLong(STATIONARY_TIME_START,System.currentTimeMillis());
                 SharedPreferenceManager.write(VEHICLE_STATE,StateType.getByStateID(STATIONARY).name());
             }
        }
    }


    public String getAndUpdateVehicleState(StationaryFilterVO filterVO){

        long stationaryTime = getTimestampOfVehicleState();
        boolean isMovingNow = filterVO.getSpeed() > 3;
        long timeDiff;
        if(stationaryTime>0){
            if(isMovingNow && isVehicleMovedSignificantly(filterVO)){
                resetAllStationaryPrefs();
                return StateType.getByStateID(MOVING).name();
            }else if(isMovingNow && !isVehicleMovedSignificantly(filterVO)){
                resetAllStationaryPrefs();
                saveVehicleState(StateType.getByStateID(EXPECTED_MOVEMENT).name());
                return  StateType.getByStateID(EXPECTED_MOVEMENT).name();
            }else{
                timeDiff = System.currentTimeMillis()-stationaryTime;
                if(timeDiff>STATIONARY_MIN_THRESHOLD){
                    if(!SharedPreferenceManager.read(VEHICLE_STATE,null).equals(StateType.getByStateID(STATIONARY).name())) {
                        saveVehicleState(StateType.getByStateID(STATIONARY).name());
                        try {
                            FIRST_STATIONARY_OBJECT = (StationaryFilterVO) filterVO.clone();
                        }catch (CloneNotSupportedException e){
                            Log.e(TAG,"Exception while cloning stationary objcect "+e.getLocalizedMessage());
                        }
                    }
                    return StateType.getByStateID(STATIONARY).name();
                }else if(timeDiff>STATIONARY_EXPCT_THRESHOLD){
                    saveVehicleState(StateType.getByStateID(EXPECTED_STATIONARY).name());
                    return StateType.getByStateID(EXPECTED_STATIONARY).name();
                }else{
                    saveVehicleState(StateType.getByStateID(EXPECTED_STATIONARY).name());
                    return StateType.getByStateID(EXPECTED_STATIONARY).name();
                }
            }
        }else{
            if(isMovingNow){
                if(!SharedPreferenceManager.read(VEHICLE_STATE,null).equals(StateType.getByStateID(MOVING).name()))
                    saveVehicleState(StateType.getByStateID(MOVING).name());
                return StateType.getByStateID(MOVING).name();
            }else {
                savePrefsForCurrentLocation(StateType.getByStateID(EXPECTED_STATIONARY).name(),filterVO.getLatitude(),filterVO.getLongitude());
                return StateType.getByStateID(EXPECTED_STATIONARY).name();
            }
        }

    }


    private boolean isVehicleMovedSignificantly(StationaryFilterVO stationaryFilterVO){

        double latitude = Double.parseDouble(SharedPreferenceManager.read(STATIONARY_LATITUDE,null));
        double longitude = Double.parseDouble(SharedPreferenceManager.read(STATIONARY_LONGITUDE,null));
        double displacement = Util.getDistance(latitude,longitude,stationaryFilterVO.getLatitude(),stationaryFilterVO.getLongitude());
        return displacement > 0.05;
    }


    private void savePrefsForCurrentLocation(String state, double latitude, double longitude){
        saveVehicleState(state);
        saveVehicleLocation(latitude,longitude);
        SharedPreferenceManager.writeLong(STATIONARY_TIME_START,System.currentTimeMillis());
    }


    private long getTimestampOfVehicleState(){
        return SharedPreferenceManager.readLong(STATIONARY_TIME_START);
    }

    private void saveVehicleState(String state){
        SharedPreferenceManager.write(VEHICLE_STATE,state);
    }

    private void saveVehicleLocation(double latitude, double longitude){
        SharedPreferenceManager.write(STATIONARY_LATITUDE,String.valueOf(latitude));
        SharedPreferenceManager.write(STATIONARY_LONGITUDE,String.valueOf(longitude));
    }

    public static void resetAllStationaryPrefs(){
        SharedPreferenceManager.write(VEHICLE_STATE,StateType.getByStateID(MOVING).name());
        SharedPreferenceManager.write(STATIONARY_LATITUDE,"");
        SharedPreferenceManager.write(STATIONARY_LONGITUDE,"");
        SharedPreferenceManager.writeLong(STATIONARY_TIME_START,0L);
    }
}
