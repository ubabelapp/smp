package com.asm.android.model;

/**
 * @author Vikash Sharma
 *
 */
public class GyroscopeData {

	private float x;
	private float y;
	private float z;
	private long timestamp;
	private double speed;
	private double latitude;
	private double longitude;
	private double altitude;
	private double bearing;
	private float timeInSec;

	public GyroscopeData(float x, float y, float z, long timestamp) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.timestamp = timestamp;
	}

    public GyroscopeData(float x, float y, float z, long timestamp, double speed,
						 double latitude, double longitude, double altitude, double bearing,
                         float timeInSec) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.timestamp = timestamp;
        this.speed = speed;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.bearing = bearing;
        this.timeInSec = timeInSec;
    }

    public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public float getTimeInSec() {
        return timeInSec;
    }

    public void setTimeInSec(float timeInSec) {
        this.timeInSec = timeInSec;
    }
}
