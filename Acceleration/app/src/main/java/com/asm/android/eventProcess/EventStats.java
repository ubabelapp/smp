/**
 * 
 */
package com.asm.android.eventProcess;

/**
 * @author Vikash Sharma
 *
 */
public class EventStats {

	
	private double confidence;
	private String decision;
	private int eventType;
	private boolean inProcess;
	
	
	public EventStats() {
	}


	/**
	 * @return the confidence
	 */
	public double getConfidence() {
		return confidence;
	}


	/**
	 * @param confidence the confidence to set
	 */
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}


	/**
	 * @return the decision
	 */
	public String getDecision() {
		return decision;
	}


	/**
	 * @param decision the decision to set
	 */
	public void setDecision(String decision) {
		this.decision = decision;
	}


	/**
	 * @return the eventType
	 */
	public int getEventType() {
		return eventType;
	}


	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}


	/**
	 * @return the inProcess
	 */
	public boolean isInProcess() {
		return inProcess;
	}


	/**
	 * @param inProcess the inProcess to set
	 */
	public void setInProcess(boolean inProcess) {
		this.inProcess = inProcess;
	}

	
	
}
