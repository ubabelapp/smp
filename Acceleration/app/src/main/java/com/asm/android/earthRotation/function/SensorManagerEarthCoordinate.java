package com.asm.android.earthRotation.function;

import android.hardware.SensorManager;


public class SensorManagerEarthCoordinate implements EarthCoordinate {

    @Override
    public boolean getRotationMatrix(float[] R, float[] I, float[] gravity, float[] geomagnetic) {
        return SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);
    }
}
