package com.asm.android.dataLogger;

import android.content.Context;

import com.asm.android.constant.Constants;
import com.asm.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * Created by Sourabh on 22-03-2018.
 */

public class DataLoggerSensorEnergy {

    private static final String TAG = DataLoggerButtonEvent.class.getSimpleName();

    private ArrayList<String> csvHeaders;

    private DataLoggerInterface dataLogger;

    public DataLoggerSensorEnergy(Context context) {

        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<Float> value){
        dataLogger.addRowData(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return Constants.ENERGY_DATA_FILE_NAME;
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("Acc-X-value");
        headers.add("Acc-Y-value");
        headers.add("Acc-Z-value");
        headers.add("Gyro-X-value");
        headers.add("Gyro-Y-value");
        headers.add("Gyro-Z-value");
        headers.add("Azimuth");
        headers.add("Roll");
        headers.add("Pitch");
        headers.add("Speed");
        headers.add("Latitude");
        headers.add("Longitude");
        headers.add("Bearing");
        headers.add("deviation");
        headers.add("energy");
        return headers;
    }
}
