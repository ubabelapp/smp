package com.asm.android.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.util.Log;

import com.asm.android.AudioEvent.AudioDataModel;
import com.asm.android.AudioEvent.PeakDet;
import com.asm.android.ButtonEvent.ButtonEventCapture;
import com.asm.android.Fdtw.vsense.distance.MovingAverage;
import com.asm.android.PotHoleDetection.PotholeDetection;
import com.asm.android.constant.Constants;
import com.asm.android.dataLogger.DataLogger;
import com.asm.android.dataLogger.DataLoggerButtonEvent;
import com.asm.android.dataLogger.DataLoggerSensorEnergy;
import com.asm.android.filter.LPFAndroidDeveloper;
import com.asm.android.filter.LowPassFilter;
import com.asm.android.filter.MeanFilterByArray;
import com.asm.android.filter.MeanFilterByValue;
import com.asm.android.filter.StationaryFilter;
import com.asm.android.filterVO.StationaryFilterVO;
import com.asm.android.location.LocationTracker;
import com.asm.android.model.AccelerometerData;
import com.asm.android.model.EnergyDataModel;
import com.asm.android.model.EventData;
import com.asm.android.model.GravityData;
import com.asm.android.model.GyroscopeData;
import com.asm.android.model.LinearAccelerationData;
import com.asm.android.model.MagneticFieldData;
import com.asm.android.model.RotationVectorData;
import com.asm.android.orientation.sensors.Orientation;
import com.asm.android.orientation.utils.OrientationSensorInterface;
import com.asm.android.utils.DialogUtil;
import com.asm.android.utils.ResamplingData;
import com.asm.android.gyroMethod.GyroMethodBumpDetection;
import com.asm.android.vehicleState.VehicleState;
import com.kircherelectronics.fsensor.filter.fusion.OrientationComplimentaryFusion;
import com.kircherelectronics.fsensor.filter.fusion.OrientationFusion;
import com.kircherelectronics.fsensor.linearacceleration.LinearAcceleration;
import com.kircherelectronics.fsensor.linearacceleration.LinearAccelerationFusion;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.filters.BandPass;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;

/*
 * Created by Sourabh on 14-02-2018.
 */

public class SensorData implements SensorEventListener, OrientationSensorInterface {

    private static final String TAG = SensorData.class.getSimpleName();

    private static final int TYPE_SENSOR_ENERGY = 1;
    private static final int TYPE_BUTTON_EVENT_ENERGY = 2;

    private Context mContext;
    private SensorManager mSensorManager;
    private DataLogger mDataLogger;
    private Orientation mOrientationSensor;
    private LocationTracker mLocationTracker;
    private Thread mSensorDataProcessingThread;
    private ProgressDialog mProgressDialog;

    private volatile LinkedBlockingDeque<AccelerometerData> mAccelerometerDataList;
    private volatile LinkedBlockingDeque<GyroscopeData> mGyroscopeDataList;
    private volatile LinkedBlockingDeque<LinearAccelerationData> mLinearAccelerationDataList;
    private volatile LinkedBlockingDeque<MagneticFieldData> mMagneticDataList;
    private volatile LinkedBlockingDeque<RotationVectorData> mRotationVectorDataList;
    private volatile LinkedBlockingDeque<GravityData> mGravityDataList;
    private ArrayList<Float> mRowData;

    //Filters
    private MeanFilterByArray meanFilterAcceleration;
    private MeanFilterByValue meanFilterMagnitude;
    private LowPassFilter mLowPassFilter;
    private LinearAcceleration linearAccelerationFilterComplimentary;
    private OrientationFusion orientationFusionComplimentary;

    //Flags
    private boolean mOneTimeFlag = false;
    private boolean mLinearAccOneTimeFlag = true;
    private boolean mLinearAccOneTimeSensorFlag = true;
    private boolean mIsComplimentaryFilterEnabled = false;
    private boolean mIsSensorThreadStopped = false;
    private boolean mIsButtonEventTriggered = false;

    private float[] speed = new float[3];

    private long mLogTime;
    private double mAzimuth;
    private double mRoll;
    private double mPitch;
    private double mYaw;
    private float mMagnitude;
    private double mPreviousAzimuthValue = 0.0;
    private float mLinearAccPreviousTime;
    private float mCurrentAcceleration;
    private float mCurrentSpeed;
    private float mCurrentDistanceTravelled;
    private float distanceX, distanceY, distanceZ;
    private long mSensorFreqStartTime = 0;
    private int mSensorFreqCount = 0;
    private long mSensorFrequencyInitTime = 0;
    private float mSensorFreq = 50;
    private int mBtnSelectedType = 0;

    private String mOrientationYawValue;
    private String mOrientationRollValue;
    private String mOrientationPitchValue;

    // VSense Algorithm variables
    private Thread mVSenseThread;
    private volatile LinkedBlockingDeque<GyroscopeData> mVSenseGyroDataList;
    private volatile LinkedBlockingDeque<AccelerometerData> mVSenseAccelerometerDataList;
    private volatile LinkedBlockingDeque<MagneticFieldData> mVSenseMagneticDataList;
    private GyroMethodBumpDetection gyroMethodBumpDetection;
    private boolean mIsVSenseThreadStopped = false;

    // Audio Event
    private LinkedBlockingDeque<AudioDataModel> rmsList;
    private long mAudioEventStartTime = 0;
    private Thread mAudioProcessingThread;
    private boolean mIsAudioProcessingThreadStopped = false;
    private ArrayList<AudioDataModel> mAudioListTempBufferList;
    private PeakDet mPeakDet;

    // Button event energy data
    private ArrayList<EnergyDataModel> mButtonEnergyDataList;
    private DataLoggerButtonEvent mDataLoggerButtonEvent;

    // Sensor data energy data
    private LinkedBlockingDeque<EnergyDataModel> mSensorEnergyDataList;
    private Thread mSensorEnergyThread;
    private boolean mIsSensorEnergyThreadStopped = false;
    private DataLoggerSensorEnergy mDataLoggerSensorEnergy;

    // pot hole detection data
    private PotholeDetection mPotholeDetection;
    private volatile LinkedBlockingDeque<AccelerometerData> mPotHoleDataList;
    private Thread mPotHoleThread;
    private boolean mIsPotHoleThreadStopped = false;

    private ButtonEventCapture mButtonEventCapture;

    public SensorData(Context context){

        this.mContext = context;
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometerDataList = new LinkedBlockingDeque<>();
        mGyroscopeDataList = new LinkedBlockingDeque<>();
        mLinearAccelerationDataList = new LinkedBlockingDeque<>();
        mMagneticDataList = new LinkedBlockingDeque<>();
        mRotationVectorDataList = new LinkedBlockingDeque<>();
        mGravityDataList = new LinkedBlockingDeque<>();
        mRowData = new ArrayList<>();
        mOrientationSensor = new Orientation(context, this);
        mLocationTracker = new LocationTracker(context);
        initFilters();
        initComplementaryFilter();

        mVSenseGyroDataList = new LinkedBlockingDeque<>();
        mVSenseAccelerometerDataList = new LinkedBlockingDeque<>();
        mVSenseMagneticDataList = new LinkedBlockingDeque<>();
        mAudioListTempBufferList = new ArrayList<>();


        rmsList = new LinkedBlockingDeque<>();

        mButtonEnergyDataList = new ArrayList<>();

        mSensorEnergyDataList = new LinkedBlockingDeque<>();

        mPotHoleDataList = new LinkedBlockingDeque<>();
    }

    private void init(){

        mDataLogger = new DataLogger(mContext);
        mDataLogger.setHeaders();
        rawDataProcessingThreadStart();

        gyroMethodBumpDetection = new GyroMethodBumpDetection(mContext);
        vSenseThreadStart();

        mPeakDet = new PeakDet(mContext);
        startAudioEventThread();
        audioDataProcessingThread();

        mDataLoggerSensorEnergy = new DataLoggerSensorEnergy(mContext);
        mDataLoggerSensorEnergy.setHeaders();
        sensorEnergyThreadStart();

        mPotholeDetection = new PotholeDetection(mContext);
        vPotHoleThreadStart();

        mButtonEventCapture = new ButtonEventCapture(mContext);
    }

    private void initFilters(){

        meanFilterAcceleration = new MeanFilterByArray();
        meanFilterAcceleration.setWindowSize(10);

        meanFilterMagnitude = new MeanFilterByValue();
        meanFilterMagnitude.setWindowSize(10);

        mLowPassFilter = new LPFAndroidDeveloper();
        mLowPassFilter.setAlphaStatic(true);
        mLowPassFilter.setAlpha(0.3f);
    }

    private void resetFilter(){

        if(mIsComplimentaryFilterEnabled) {
            linearAccelerationFilterComplimentary.reset();
            orientationFusionComplimentary.reset();
        }
    }

    private void initComplementaryFilter(){

        if(mIsComplimentaryFilterEnabled) {
            orientationFusionComplimentary = new OrientationComplimentaryFusion();
            linearAccelerationFilterComplimentary = new LinearAccelerationFusion(orientationFusionComplimentary);
        }
    }

    public void registerSensor(){

        init();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                SensorManager.SENSOR_DELAY_FASTEST);
        startOrientationSensor();
        mSensorFrequencyInitTime = System.currentTimeMillis();

    }

    public void unRegisterSensor(){

        Log.e(TAG, "Unregister called");
        mSensorManager.unregisterListener(this);
        mIsSensorThreadStopped = true;
        mIsVSenseThreadStopped = true;
        mIsSensorEnergyThreadStopped = true;
        mIsAudioProcessingThreadStopped = true;
        mIsPotHoleThreadStopped = true;

        if(mSensorDataProcessingThread != null){
            mSensorDataProcessingThread.interrupt();
        }
        if(mVSenseThread != null){
            mVSenseThread.interrupt();
        }
        if(mSensorEnergyThread != null){
            mSensorEnergyThread.interrupt();
        }
        if(mAudioProcessingThread != null){
            mAudioProcessingThread.interrupt();
        }
        if(mPotHoleThread != null){
            mPotHoleThread.interrupt();
        }
        stopOrientationSensor();
        mDataLogger.stopLogging();
        gyroMethodBumpDetection.stopVSenseLogging();
        mDataLoggerSensorEnergy.stopLogging();
        mPeakDet.stopLogging();
        resetFilter();

    }

    public void buttonEventTriggered(String buttonEventName, int btnType){

        Log.e(TAG, "Button event triggered :"+buttonEventName);
        mBtnSelectedType = btnType;
//        this.mIsButtonEventTriggered = true;

//        mButtonEnergyDataList.clear();
//        mDataLoggerButtonEvent = new DataLoggerButtonEvent(mContext, buttonEventName);
//        mDataLoggerButtonEvent.setHeaders();
    }

    public void buttonEventStopped(String btnName, int type){

        Log.e(TAG, "Button event stooped ");
        mButtonEventCapture.writeToFile(btnName, type);
//        this.mIsButtonEventTriggered = false;
//        new ButtonEventEnergyAsync().execute();
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        if(!mOneTimeFlag){
            mOneTimeFlag = true;
            mLogTime = System.currentTimeMillis();
            VehicleState.resetAllStationaryPrefs();
        }

        float timestamp = (System.currentTimeMillis() - mLogTime) / 1000.0f;

        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            mRotationVectorDataList.add(new RotationVectorData(event.values[0],event.values[1],event.values[2],
                    event.timestamp));
        }

        else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {

            float[] value = getFilteredValue(event.values, Constants.GYROSCOPE_SENSOR);
            if(value[0] != 0.0) {
                mGyroscopeDataList.add(new GyroscopeData(value[0], value[1], value[2],
                        event.timestamp));
                if(mOrientationYawValue != null) {
                    float roll = Float.parseFloat(mOrientationRollValue);
                    float pitch = Float.parseFloat(mOrientationPitchValue);
                    float yaw = Float.parseFloat(mOrientationYawValue);
                    mVSenseGyroDataList.add(new GyroscopeData(roll, pitch, yaw,
                            event.timestamp, mLocationTracker.getSpeed(), mLocationTracker.getLatitude(),
                            mLocationTracker.getLongitude(), mLocationTracker.getAltitude(),
                            mLocationTracker.getBearing(), timestamp));
                }
            }
        }

        else if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            if((System.currentTimeMillis() - mSensorFrequencyInitTime) < 3000) {
                mSensorFreq = calculateSensorFrequency();
            }

            float[] value = getFilteredValue(event.values, Constants.ACCELEROMETER_SENSOR);
            mAccelerometerDataList.add(new AccelerometerData(value[0], value[1], value[2],
                    event.timestamp, System.currentTimeMillis()));
            mVSenseAccelerometerDataList.add(new AccelerometerData(value[0], value[1], value[2],
                    event.timestamp));
            mPotHoleDataList.add(new AccelerometerData(value[0], value[1], value[2],
                    event.timestamp,mLocationTracker.getLatitude(),mLocationTracker.getLongitude(),
                    mLocationTracker.getSpeed(),mLocationTracker.getBearing()));
        }

        else if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){

            mLinearAccelerationDataList.add(new LinearAccelerationData(event.values[0],event.values[1],
                    event.values[2], event.timestamp));
        }

        else if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){

            float[] value = getFilteredValue(event.values, Constants.MAGNETIC_FIELD_SENSOR);
            mMagneticDataList.add(new MagneticFieldData(value[0],value[1],value[2],
                    event.timestamp));
            mVSenseMagneticDataList.add(new MagneticFieldData(value[0],value[1],value[2],
                    event.timestamp));
        }

        else if(event.sensor.getType() == Sensor.TYPE_GRAVITY){

            mGravityDataList.add(new GravityData(event.values[0],event.values[1],event.values[2],
                    event.timestamp));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private float[] getFilteredValue(float[] value, int sensorType){

        float[] eventValue;

        switch (sensorType){

            case Constants.ACCELEROMETER_SENSOR:
                if(mIsComplimentaryFilterEnabled) {
                    orientationFusionComplimentary.setAcceleration(value);
                    eventValue = linearAccelerationFilterComplimentary.filter(value);
                }else{
                    eventValue = value;
                }
                break;

            case Constants.GYROSCOPE_SENSOR:
                if(mIsComplimentaryFilterEnabled) {
                    eventValue = orientationFusionComplimentary.filter(value);
                }else{
                    eventValue = value;
                }
                break;

            case Constants.MAGNETIC_FIELD_SENSOR:
                if(mIsComplimentaryFilterEnabled) {
                    orientationFusionComplimentary.setMagneticField(value);
                    eventValue = linearAccelerationFilterComplimentary.filter(value);
                }else{
                    eventValue = value;
                }
                break;

            default:
                eventValue = value;
                break;
        }

        return eventValue;
    }


    private void rawDataProcessingThreadStart(){

        mSensorDataProcessingThread = new Thread(new Runnable(){
            @Override
            public void run(){
                processSensorCalculation();
            }
        });
        mSensorDataProcessingThread.start();
    }

    private void processSensorCalculation(){

        while (!mIsSensorThreadStopped){

            float[] rotationVectorValues = new float[3];
            float[] gyroscopeValues = new float[3];
            float[] accelerationValues = new float[3];
            float[] linearAccelerationValues = new float[3];
            float[] magneticFieldValues = new float[3];
            float[] gravityValues = new float[3];
            long rotationVectorTimeStamp;
            long gyroscopeTimeStamp;
            long accelerationTimeStamp;
            long accelerationTimeMillis;
            long linearAccelerationTimeStamp;
            long magneticFieldStamp;

            if(mRotationVectorDataList != null && mRotationVectorDataList.size()> 0 &&
                    mGyroscopeDataList != null && mGyroscopeDataList.size() >0 &&
                    mAccelerometerDataList != null && mAccelerometerDataList.size()>0 &&
                    mLinearAccelerationDataList != null && mLinearAccelerationDataList.size() >0 &&
                    mMagneticDataList != null && mMagneticDataList.size() >0 &&
                    mGravityDataList != null && mGravityDataList.size() > 0 ) {

                float timestamp = (System.currentTimeMillis() - mLogTime) / 1000.0f;

                rotationVectorValues[0] = mRotationVectorDataList.getFirst().getX();
                rotationVectorValues[1] = mRotationVectorDataList.getFirst().getY();
                rotationVectorValues[2] = mRotationVectorDataList.getFirst().getZ();
                rotationVectorTimeStamp = mRotationVectorDataList.getFirst().getTimestamp();
                mRotationVectorDataList.removeFirst();

                gyroscopeValues[0] = mGyroscopeDataList.getFirst().getX();
                gyroscopeValues[1] = mGyroscopeDataList.getFirst().getY();
                gyroscopeValues[2] = mGyroscopeDataList.getFirst().getZ();
                gyroscopeTimeStamp = mGyroscopeDataList.getFirst().getTimestamp();
                mGyroscopeDataList.removeFirst();

                accelerationValues[0] = mAccelerometerDataList.getFirst().getX();
                accelerationValues[1] = mAccelerometerDataList.getFirst().getY();
                accelerationValues[2] = mAccelerometerDataList.getFirst().getZ();
                accelerationTimeStamp = mAccelerometerDataList.getFirst().getTimestamp();
                accelerationTimeMillis = mAccelerometerDataList.getFirst().getTimeMillis();
                mAccelerometerDataList.removeFirst();

                linearAccelerationValues[0] = mLinearAccelerationDataList.getFirst().getX();
                linearAccelerationValues[1] = mLinearAccelerationDataList.getFirst().getY();
                linearAccelerationValues[2] = mLinearAccelerationDataList.getFirst().getZ();
                linearAccelerationTimeStamp = mLinearAccelerationDataList.getFirst().getTimestamp();
                mLinearAccelerationDataList.removeFirst();

                magneticFieldValues[0] = mMagneticDataList.getFirst().getX();
                magneticFieldValues[1] = mMagneticDataList.getFirst().getY();
                magneticFieldValues[2] = mMagneticDataList.getFirst().getZ();
                magneticFieldStamp = mMagneticDataList.getFirst().getTimestamp();

                if(mMagneticDataList != null && mMagneticDataList.size() > 1) {
                    mMagneticDataList.removeFirst();
                }

                gravityValues[0] = mGravityDataList.getFirst().getX();
                gravityValues[1] = mGravityDataList.getFirst().getY();
                gravityValues[2] = mGravityDataList.getFirst().getZ();
                mGravityDataList.removeFirst();

                accelerationSensorCalculation(accelerationValues, accelerationTimeStamp);
                linearAccelerationSensorCalculation(linearAccelerationValues, linearAccelerationTimeStamp);

                double azimuth = getAzimuthInAngle();

                StationaryFilterVO stationaryFilterVO = new StationaryFilterVO(mLocationTracker.getSpeed(),mLocationTracker.getLatitude(), mLocationTracker.getLongitude(),mLocationTracker.getAltitude(),System.currentTimeMillis());
                if(StationaryFilter.isTrackingAllowed(mContext,stationaryFilterVO)) {

                    if (azimuth != 0 && mOrientationYawValue != null) {

                        gyroscopeValues[0] = Float.parseFloat(mOrientationRollValue);
                        gyroscopeValues[1] = Float.parseFloat(mOrientationPitchValue);
                        gyroscopeValues[2] = Float.parseFloat(mOrientationYawValue);

                        double deviation = 0;
                        if(mPreviousAzimuthValue != 0.0){
                            deviation = getDeviation(mPreviousAzimuthValue, azimuth);
                        }

                        addRowToFile(timestamp, accelerationTimeMillis, accelerationValues, gyroscopeValues, linearAccelerationValues,
                                gravityValues, azimuth, mRoll, mPitch, getYawInAngle(), getYawInRadian(),
                                deviation, mLocationTracker.getLatitude(), mLocationTracker.getLongitude(),
                                mLocationTracker.getAltitude(), mLocationTracker.getSpeed(), mMagnitude,
                                mLocationTracker.getBearing());

                        mPreviousAzimuthValue = azimuth;

                        // Sensor energy data main data list
                        mSensorEnergyDataList.add(new EnergyDataModel((float)deviation, timestamp,accelerationValues[0],
                                accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                                gyroscopeValues[2],(float) azimuth, (float) mRoll, (float) mPitch,
                                (float) mLocationTracker.getSpeed(),mLocationTracker.getLatitude(),mLocationTracker.getLongitude(),
                                mLocationTracker.getBearing()));

                        // button event energy data list
                        //if(mIsButtonEventTriggered){

                            mButtonEventCapture.addDataToList(mBtnSelectedType,(float)deviation,timestamp,
                                    accelerationValues,gyroscopeValues,(float) azimuth, (float) mRoll, (float) mPitch,
                                    (float) mLocationTracker.getSpeed(),mLocationTracker.getLatitude(),mLocationTracker.getLongitude(),
                                    mLocationTracker.getBearing());

                           /* mButtonEnergyDataList.add(new EnergyDataModel((float)deviation,timestamp,accelerationValues[0],
                                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                                    gyroscopeValues[2],(float) azimuth, (float) mRoll, (float) mPitch,
                                    (float) mLocationTracker.getSpeed(),mLocationTracker.getLatitude(),mLocationTracker.getLongitude(),
                                    mLocationTracker.getBearing()));*/
                        //}
                    }
                }
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void addRowToFile(float timeStamp, long accTimeMillis, float[] accelerationValue, float[] gyroscopeValue,
                              float[] linearAccValue, float[] gravityValue, double azimuth, double roll, double pitch, double yaw,
                              double yawRadian, double deviation ,double latitude, double longitude, double altitude,
                              double speed, float magnitude, double bearing){

        mRowData.clear();
        mRowData.add(timeStamp);
        mRowData.add((float)accTimeMillis);
        mRowData.add(accelerationValue[0]);
        mRowData.add(accelerationValue[1]);
        mRowData.add(accelerationValue[2]);
        mRowData.add(gyroscopeValue[0]);
        mRowData.add(gyroscopeValue[1]);
        mRowData.add(gyroscopeValue[2]);
        mRowData.add(linearAccValue[0]);
        mRowData.add(linearAccValue[1]);
        mRowData.add(linearAccValue[2]);
        mRowData.add(gravityValue[0]);
        mRowData.add(gravityValue[1]);
        mRowData.add(gravityValue[2]);
        mRowData.add((float)azimuth);
        mRowData.add((float)roll);
        mRowData.add((float)pitch);
        mRowData.add((float)yaw);
        mRowData.add((float)yawRadian);
        mRowData.add((float)deviation);
        mRowData.add((float)latitude);
        mRowData.add((float)longitude);
        mRowData.add((float)altitude);
        mRowData.add((float)speed);
        mRowData.add(magnitude);
        mRowData.add((float)bearing);

        mDataLogger.addRow(mRowData);
    }


    /**
     * @param a - previous azimuth value
     * @param b - new azimuth value
     * @return deviation between two values
     */
    private double getDeviation(double a, double b){

        double d = Math.abs(a - b) % 360;
        double r = d > 180 ? 360 - d : d;
        int sign = (a - b >= 0 && a - b <= 180) || (a - b <=-180 && a- b>= -360) ? 1 : -1;
        r *= sign;
        return r;
    }


    @Override
    public void orientation(Double AZIMUTH, Double PITCH, Double ROLL, Double YAW) {

        this.mAzimuth = AZIMUTH;
        this.mRoll = ROLL;
        this.mPitch = PITCH;
        this.mYaw = YAW;
    }

    @Override
    public void orientationYawData(String Yaw, String Roll, String Pitch) {

        this.mOrientationYawValue = Yaw;
        this.mOrientationRollValue = Roll;
        this.mOrientationPitchValue = Pitch;
    }

    /**
     * method to start the orientation sensor
     */
    private void startOrientationSensor(){

        mOrientationSensor.init(1.0, 1.0, 1.0);
        mOrientationSensor.on(0);
        mOrientationSensor.isSupport();
    }


    /**
     *  method to stop orientation sensor
     */
    private void stopOrientationSensor(){
        mOrientationSensor.off();
    }

    /**
     * @return azimuth in angle
     */
    private double getAzimuthInAngle(){

        if(mAzimuth == 0.0){
            return 0;
        }
        mAzimuth = mAzimuth+0.0001;
        return mAzimuth;
    }

    /**
     * @return yaw in angle
     */
    private double getYawInAngle(){

        if(mYaw == 0.0){
            return 0;
        }
        mYaw = mYaw+0.0001;
        return mYaw;
    }

    /**
     * @return yaw in radian
     */
    private double getYawInRadian(){

        if(mYaw == 0.0){
            return 0;
        }
        return Math.toRadians(mYaw);
    }


    /**
     * @param values acceleration X,Y,Z value
     * @param eventTimeStamp acceleration event timestamp
     */
    private void accelerationSensorCalculation(float[] values, long eventTimeStamp){

        float[] acceleration = new float[3];

        acceleration[0] = values[0]/SensorManager.GRAVITY_EARTH;
        acceleration[1] = values[1]/SensorManager.GRAVITY_EARTH;
        acceleration[2] = values[2]/SensorManager.GRAVITY_EARTH;

        acceleration = meanFilterAcceleration.filterFloat(acceleration);
        mMagnitude = getMagnitude(acceleration);
    }


    /**
     * @param values mean Filtered acceleration X,Y,Z values
     * @return magnitude
     */
    private float getMagnitude(float[] values){

        float[] lowPassFilterAcceleration;
        lowPassFilterAcceleration = mLowPassFilter.addSamples(values);
        return meanFilterMagnitude.filterFloat((float) Math
                .sqrt(Math.pow(lowPassFilterAcceleration[0], 2)
                        + Math.pow(lowPassFilterAcceleration[1], 2)
                        + Math.pow(lowPassFilterAcceleration[2], 2)));
    }


    /**
     * @param values linear acceleration senosor X,Y,Z value
     * @param eventTimeStamp linear acceleration event timestamp
     */
    private void linearAccelerationSensorCalculation(float[] values, long eventTimeStamp){

        if(mLinearAccOneTimeFlag){

            mLinearAccOneTimeFlag = false;
            mLinearAccPreviousTime = eventTimeStamp / 1000000000;

        }else{

            float currentTime = eventTimeStamp / 1000000000.0f;
            float deltaTime = currentTime - mLinearAccPreviousTime;
            mLinearAccPreviousTime = currentTime;
            calculateLinearAccSensorDistance(values, deltaTime);
            mCurrentAcceleration =  (float) Math.sqrt(values[0] * values[0] + values[1] * values[1] + values[2] * values[2]);
            mCurrentSpeed = (float) Math.sqrt(speed[0] * speed[0] + speed[1] * speed[1] + speed[2] * speed[2]);
            mCurrentDistanceTravelled = (float) Math.sqrt(distanceX *  distanceX + distanceY * distanceY +  distanceZ * distanceZ);
            mCurrentDistanceTravelled = mCurrentDistanceTravelled / 1000;

            if(mLinearAccOneTimeSensorFlag){
                mLinearAccOneTimeSensorFlag = false;
//                prevAcceleration = currentAcceleration;
//                prevDistance = currentDistanceTravelled;
//                prevSpeed = currentSpeed;
            }
        }
    }

    /**
     * @param values linear acceleration senosor X,Y,Z value
     * @param deltaTime time diff between current and previous event timestamp
     */
    private void calculateLinearAccSensorDistance (float[] values, float deltaTime) {

        float[] distance = new float[values.length];

        for (int i = 0; i < values.length; i++) {
            speed[i] = values[i] * deltaTime;
            distance[i] = speed[i] * deltaTime + values[i] * deltaTime * deltaTime / 2;
        }
        distanceX = distance[0];
        distanceY = distance[1];
        distanceZ = distance[2];
    }

    /**
     * @return sensor frequency
     */
    private float calculateSensorFrequency() {

        if (mSensorFreqStartTime == 0) {
            mSensorFreqStartTime = System.nanoTime();
        }
        if(mSensorFreqCount > Integer.MAX_VALUE - 1000){
            mSensorFreqCount = 0;
            mSensorFreqStartTime = System.nanoTime();
        }
        long timestamp = System.nanoTime();
        return (mSensorFreqCount++ / ((timestamp - mSensorFreqStartTime) / 1000000000.0f));
    }


    /**
     * @param data - energy data list
     * @param dataSize - stored data list size
     * @param type - sensor data energy or button event energy
     */
    private void calculateEnergy(final ArrayList<EnergyDataModel> data, final int dataSize, final int type) {

        double energy = 0;
        double deviation;

        for (int n = 0; n < dataSize-1; n++) {

            for (int k = 0; k < 50; k++) {

                if (n - k >= 0) {
                    deviation = data.get(n - k).getDeviation();
                    energy = energy + (deviation * deviation);
                } else {
                    energy = energy + 0;
                }
            }

            ArrayList<Float> energyData = new ArrayList<>();
            energyData.add(data.get(n).getTimestamp());
            energyData.add(data.get(n).getAccelerometerXAxis());
            energyData.add(data.get(n).getAccelerometerYAxis());
            energyData.add(data.get(n).getAccelerometerZAxis());
            energyData.add(data.get(n).getGyroXAxis());
            energyData.add(data.get(n).getGyroYAxis());
            energyData.add(data.get(n).getGyroZAxis());
            energyData.add(data.get(n).getAzimuth());
            energyData.add(data.get(n).getRoll());
            energyData.add(data.get(n).getPitch());
            energyData.add(data.get(n).getGpsSpeed());
            energyData.add((float)data.get(n).getLatitude());
            energyData.add((float)data.get(n).getLongitude());
            energyData.add((float)data.get(n).getBearing());
            energyData.add(data.get(n).getDeviation());
            energyData.add((float)energy);

            switch (type){
                case TYPE_SENSOR_ENERGY:
                    mDataLoggerSensorEnergy.addRow(energyData);
                    break;

                case TYPE_BUTTON_EVENT_ENERGY:
                    mDataLoggerButtonEvent.addRow(energyData);
                    break;
            }
            energyData.clear();
            energy = 0;
        }
    }


    /**
     * Async Task to calculate energy for button click events
     */
    @SuppressLint("StaticFieldLeak")
    private class ButtonEventEnergyAsync extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = DialogUtil.showLoadingDialog(mContext,"Writing energy data");
        }

        @Override
        protected String doInBackground(String... strings) {
            calculateEnergy(mButtonEnergyDataList,mButtonEnergyDataList.size(),TYPE_BUTTON_EVENT_ENERGY);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(mProgressDialog != null && mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
            mButtonEnergyDataList.clear();
            if(mDataLoggerButtonEvent != null)
                mDataLoggerButtonEvent.stopLogging();
        }
    }

    private void sensorEnergyThreadStart(){

        mSensorEnergyThread = new Thread(new Runnable(){
            @Override
            public void run(){
                sensorEnergyDataCalculation();
            }
        });
        mSensorEnergyThread.start();
    }

    private void sensorEnergyDataCalculation() {

        while (!mIsSensorEnergyThreadStopped){

            if(mSensorEnergyDataList.size() >= 300){

                ArrayList<EnergyDataModel> tempDataList = new ArrayList<>();
                tempDataList.addAll(mSensorEnergyDataList);

                for (int i=0; i<tempDataList.size(); i++){

                    if(mIsSensorEnergyThreadStopped){
                        break;
                    }
                    if(i < 100){
                        mSensorEnergyDataList.removeFirst();
                    }
                }
                calculateEnergy(tempDataList, tempDataList.size(), TYPE_SENSOR_ENERGY);
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }


    //############################# V SENSE ###########################################

    private void vSenseThreadStart(){

        mVSenseThread = new Thread(new Runnable(){
            @Override
            public void run(){
                vSenseDataCalculation();
            }
        });
        mVSenseThread.start();
    }

    private void vSenseDataCalculation() {

        while (!mIsVSenseThreadStopped){

            if(mVSenseGyroDataList.size() >= Constants.BUFFER_WINDOW_SIZE &&
                    mVSenseAccelerometerDataList.size() >= Constants.BUFFER_WINDOW_SIZE &&
                    mVSenseMagneticDataList.size() >= Constants.BUFFER_WINDOW_SIZE){

                LinkedList<GyroscopeData> tempGyroList = new LinkedList<>();
                LinkedList<AccelerometerData> tempAccelerometerList = new LinkedList<>();
                LinkedList<MagneticFieldData> tempMagneticList = new LinkedList<>();
                tempGyroList.addAll(mVSenseGyroDataList);
                tempAccelerometerList.addAll(mVSenseAccelerometerDataList);
                tempMagneticList.addAll(mVSenseMagneticDataList);

                ArrayList<EventData> bufferList = new ArrayList<>();

                for (int i=0; i<Constants.BUFFER_WINDOW_SIZE; i++){

                    if(mIsVSenseThreadStopped){
                        break;
                    }

                    bufferList.add(new EventData(tempAccelerometerList.get(i),
                            tempGyroList.get(i),tempMagneticList.get(i)));

                    //if(i < Constants.BUFFER_WINDOW_SIZE){
                        mVSenseGyroDataList.removeFirst();
                        mVSenseAccelerometerDataList.removeFirst();
                        mVSenseMagneticDataList.removeFirst();
                    //}
                }
                vSenseCalculation(bufferList);
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }


    /**
     * @param bufferedList - Euler's corrected yaw values
     */
    private void vSenseCalculation(ArrayList<EventData> bufferedList) {

        if(mIsVSenseThreadStopped){
            return;
        }

        ArrayList<GyroscopeData> tempMovingAvgGyroFilter = new ArrayList<>();
        ArrayList<AccelerometerData> tempMovingAvgAccFilter = new ArrayList<>();
        ArrayList<MagneticFieldData> tempMovingAvgMagneticFilter = new ArrayList<>();
        ArrayList<EventData> finalFilteredList = new ArrayList<>();
        //if(StationaryFilter.isTrackingAllowed(mContext, new StationaryFilterVO(speed,longitude,latitude,altitude,eventTimeStamp))){

        List<EventData> reSampledData = ResamplingData.reSampleEventDataModel(bufferedList, mSensorFreq, 50);

        // Moving Average Filter
        MovingAverage movingAverage = new MovingAverage(Constants.BUFFER_WINDOW_SIZE);

        for (int i=0; i<reSampledData.size(); i++){

            //Log.e(TAG, "** before filter :"+reSampledData.get(i).getGyroscopeData().getZ());
            movingAverage.pushValue(reSampledData.get(i).getGyroscopeData().getZ());
            float x = reSampledData.get(i).getGyroscopeData().getX();
            float y = reSampledData.get(i).getGyroscopeData().getY();
            float z = (float) movingAverage.getValue();
            long eventTimeStamp = reSampledData.get(i).getGyroscopeData().getTimestamp();
            double speed = reSampledData.get(i).getGyroscopeData().getSpeed();
            double lat = reSampledData.get(i).getGyroscopeData().getLatitude();
            double lon = reSampledData.get(i).getGyroscopeData().getLongitude();
            double altitude = reSampledData.get(i).getGyroscopeData().getAltitude();
            double bearing = reSampledData.get(i).getGyroscopeData().getBearing();
            float timeInSec = reSampledData.get(i).getGyroscopeData().getTimeInSec();
            //Log.e(TAG, "** after filter :"+z);

            tempMovingAvgGyroFilter.add(new GyroscopeData(x,y,z,eventTimeStamp,speed,lat,lon,altitude,
                    bearing,timeInSec));
            tempMovingAvgAccFilter.add(reSampledData.get(i).getAccelerometerData());
            tempMovingAvgMagneticFilter.add(reSampledData.get(i).getMagneticFieldDataData());
        }

        for (int j=0; j<tempMovingAvgGyroFilter.size(); j++){
            finalFilteredList.add(new EventData(tempMovingAvgAccFilter.get(j),
                    tempMovingAvgGyroFilter.get(j),tempMovingAvgMagneticFilter.get(j)));
        }
        //Bump Detection algo, should pass filtered yaw data, currently raw data is passed
        gyroMethodBumpDetection.checkForBumpState(finalFilteredList, mSensorFreq);
        // }
    }

    //####################### Audio Event ########################################

    private void startAudioEventThread(){

        mAudioEventStartTime = System.currentTimeMillis();
        AudioDispatcher dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(48000,4096,0);
        AudioProcessor p = new BandPass(12000, 6000, 48000);

        dispatcher.addAudioProcessor(new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN, 48000, 4096, new PitchDetectionHandler() {

            @Override
            public void handlePitch(PitchDetectionResult pitchDetectionResult,
                                    final AudioEvent audioEvent) {
                rmsList.add(new AudioDataModel(audioEvent.getTimeStamp(),audioEvent.getRMS()*100,
                        System.currentTimeMillis(),mLocationTracker.getLatitude(),mLocationTracker.getLongitude(),
                        mLocationTracker.getSpeed(),mLocationTracker.getBearing()));
            }
        }));
        dispatcher.addAudioProcessor(p);
        new Thread(dispatcher,"Audio Dispatcher").start();
    }


    private void audioDataProcessingThread(){

        mAudioProcessingThread = new Thread(new Runnable(){
            @Override
            public void run(){
                audioProcessing();
            }
        });
        mAudioProcessingThread.start();
    }

    private void audioProcessing(){

        while (!mIsAudioProcessingThreadStopped){

            if((System.currentTimeMillis() - mAudioEventStartTime) >= 15000) {

                mAudioEventStartTime = System.currentTimeMillis();
                ArrayList<AudioDataModel> tempEventList = new ArrayList<>();
                tempEventList.addAll(rmsList);

                mAudioListTempBufferList.addAll(tempEventList);

                int removeOldLength = tempEventList.size() - 1;
                for(int i=0; i<removeOldLength; i++){
                    rmsList.removeFirst();
                }
                mPeakDet.peakDetCalculation(mAudioListTempBufferList);
                if(!PeakDet.IS_BELOW_THRESHOLD){
                    mAudioListTempBufferList.clear();
                }
            }
        }
        try {
            Thread.sleep(1);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }


    //############################# Pot Hole Detection ###########################################

    private void vPotHoleThreadStart(){

        mPotHoleThread = new Thread(new Runnable(){
            @Override
            public void run(){
                potHoleCalculation();
            }
        });
        mPotHoleThread.start();
    }

    private void potHoleCalculation() {

        while (!mIsPotHoleThreadStopped){

            if(mPotHoleDataList.size() >= Constants.BUFFER_WINDOW_SIZE ){

                LinkedList<AccelerometerData> tempAccelerometerList = new LinkedList<>();
                tempAccelerometerList.addAll(mPotHoleDataList);

                for (int i=0; i<tempAccelerometerList.size()-1; i++){

                    if(mIsPotHoleThreadStopped){
                        break;
                    }
                    mPotHoleDataList.removeFirst();
                }
                mPotholeDetection.process(tempAccelerometerList, mSensorFreq,50);
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

}
