package com.asm.android.dataLogger;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Created by KircherEngineerH on 4/27/2016.
 */
public class CsvDataLogger implements DataLoggerInterface{

    private CSVPrinter csv;
    private FileWriter fileWriter;
    private boolean headersSet;
    private File file;
    private Context context;

    public CsvDataLogger(Context context, File file)
    {
        this.context = context;
        this.file = file;

        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(System.getProperty("line.separator"));

        try{
            fileWriter = new FileWriter(file);
            csv = new CSVPrinter(fileWriter, csvFileFormat);
        } catch (IOException e){
            e.printStackTrace();
        }
        headersSet = false;
    }

    public void setHeaders(Iterable<String> headers) throws IllegalStateException {
        if(!headersSet)
        {
            try
            {
                csv.printRecord(headers);
                headersSet = true;
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            throw new IllegalStateException("Headers already exist!");
        }
    }

    @Override
    public void addRow(Iterable<String> values) throws IllegalStateException
    {
        if(headersSet)
        {
            try
            {
                csv.printRecord(values);
            } catch (IOException e)
            {
                Log.e("CsvDataLOgger", "addRow exception :"+e.toString());
                e.printStackTrace();
            }
        }
        else
        {
            throw new IllegalStateException("Headers do not exist!");
        }
    }

    @Override
    public void addRowData(Iterable<Float> values) throws IllegalStateException {

        if(headersSet)
        {
            try
            {
                csv.printRecord(values);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            throw new IllegalStateException("Headers do not exist!");
        }
    }

    public void writeToFile()
    {
        try {
            fileWriter.flush();
            fileWriter.close();
            csv.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
    }
}
