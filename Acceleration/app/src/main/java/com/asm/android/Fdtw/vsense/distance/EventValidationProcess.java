/**
 * 
 */
package com.asm.android.Fdtw.vsense.distance;

/**
 * @author Vikash Sharma
 *
 */
public interface EventValidationProcess {
	
	public String getValidationProcess();
	
}
