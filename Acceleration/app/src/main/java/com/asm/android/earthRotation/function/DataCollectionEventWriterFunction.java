package com.asm.android.earthRotation.function;

import android.util.Log;

import com.asm.android.earthRotation.APMException;
import com.asm.android.earthRotation.IOUtil;
import com.asm.android.earthRotation.event.DataCollectionEvent;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;



public class DataCollectionEventWriterFunction<T extends DataCollectionEvent> implements Function<T, Void>{
    private static final int STRING_BUILDER_CAPACITY = 120;

    private File file;
    private DateFormat csvDateFormat;

    private Writer writer;

    private StringBuilder stringBuilder;

    public DataCollectionEventWriterFunction(File file, String csvDateFormatPattern){
      /*  if(file == null){
            throw new APMException("file is null");
        }
        if(csvDateFormatPattern == null){
            throw new APMException("csvDateFormatPattern is null");
        }*/

       /* this.file = file;
        this.csvDateFormat = new SimpleDateFormat(csvDateFormatPattern);
        this.stringBuilder = new StringBuilder(STRING_BUILDER_CAPACITY);*/
    }

    public File getFile() {
        return file;
    }

    @Override
    public Void apply(T dataCollectionEvent) throws APMException {
        try {
          //  Log.e("TAG","Aligned DATA FOR -->"+dataCollectionEvent.getSensorType()+" is -->"+dataCollectionEvent.getCSVHeader());

            return null;
        }
        catch(Exception e){
            throw new APMException("Error writing file '" + file.getAbsolutePath() + "'; event: " + dataCollectionEvent, e);
        }
    }

    @Override
    public void clean() {
        IOUtil.closeQuietly(writer);
    }
}
