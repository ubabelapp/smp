package com.asm.android.earthRotation.event;


import android.hardware.SensorEvent;

import com.asm.android.earthRotation.SensorType;

import java.text.DateFormat;
import java.util.Date;


public class    MotionDataCollectionEvent extends DataCollectionEvent {
    private double x;
    private double y;
    private double z;
    private long timeStampMills;

    public MotionDataCollectionEvent(Date timestamp, long uptimeNanos, SensorType sensorType, double x,
                                     double y, double z, long timeStampMills) {
        super(timestamp, uptimeNanos, sensorType);
        this.x = x;
        this.y = y;
        this.z = z;
        this.timeStampMills = timeStampMills;
    }

    public MotionDataCollectionEvent(Date timestamp, SensorType sensorType, SensorEvent sensorEvent, long timeStampMills) {
        super(timestamp, sensorType, sensorEvent);
        this.x = sensorEvent.values[0];
        this.y = sensorEvent.values[1];
        this.z = sensorEvent.values[2];
        this.timeStampMills = timeStampMills;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public long getTimeStampMills() {
        return timeStampMills;
    }

    @Override
    public String getCSVHeader() {
        return "timestamp,uptimeNanos,x,y,z";
    }

    @Override
    public void toCSV(DateFormat dateFormat, StringBuilder stringBuilder) {
        stringBuilder.append(dateFormat.format(getTimestamp())).append(",")
                .append(getUptimeNanos()).append(",")
                .append(x).append(",")
                .append(y).append(",")
                .append(z);
    }
}
