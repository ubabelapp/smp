package com.asm.android.earthRotation;

import android.os.Process;
import android.util.Log;

import com.asm.android.async.AsyncCallback;
import com.asm.android.async.AsyncCallbackUtil;
import com.asm.android.earthRotation.event.DataCollectionEvent;
import com.asm.android.earthRotation.function.Function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;




public class DataCollectionEventConsumer {
    private static final String LOG_TAG = DataCollectionEventConsumer.class.getSimpleName();

    private final BlockingQueue<DataCollectionEvent> blockingQueue;

    private final Map<SensorType, List<Function<DataCollectionEvent, ?>>> functionsBySensorType;

    private volatile Thread executionThread;

    private ThreadPriority threadPriority;

    /**
     * Constructor used only in tests
     */
    DataCollectionEventConsumer(final BlockingQueue<DataCollectionEvent> blockingQueue, ThreadPriority threadPriority){
        this.blockingQueue = blockingQueue;
        this.threadPriority = threadPriority;
        this.functionsBySensorType = new HashMap<>();
    }

    public DataCollectionEventConsumer(final BlockingQueue<DataCollectionEvent> blockingQueue){
        this(blockingQueue, new ProcessThreadPriority());
    }

    public void addFunction(SensorType sensorType, Function<DataCollectionEvent, ?> function){
        List<Function<DataCollectionEvent, ?>> fs = functionsBySensorType.get(sensorType);
        if(fs == null){
            fs = new ArrayList<>();
            functionsBySensorType.put(sensorType, fs);
        }

        fs.add(function);
    }

    public void start(final AsyncCallback<Void, DataCollectionEventConsumerErrorContext> asyncCallback) {
        Thread thread = executionThread;
        if(thread != null){
            throw new APMException("\n" +
                    "DataCollectionEventConsumer is already running");
        }

        final AsyncCallback<Void, DataCollectionEventConsumerErrorContext> quietAsyncCallback = AsyncCallbackUtil.makeQuiet(asyncCallback);

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                threadPriority.set(Process.THREAD_PRIORITY_BACKGROUND);
                try{
                    do {
                        applyFunctions(blockingQueue.take(), quietAsyncCallback);
                    }
                    while(true);
                }
                catch(RuntimeException e){
                    /*
                        applyFunctions has already made exception handling.
                        We just need to leave
                        of the loop and does not propagate the exception to the Thread.
                     */
                }
                catch(InterruptedException e){
                    Log.d(LOG_TAG, "Event consumer interrupted (queue = " + blockingQueue.size() + ")");
                    try {
                        processRemainingEvents(quietAsyncCallback);
                        quietAsyncCallback.onSuccess(null);
                    }
                    catch(RuntimeException ex){
                        /*

                            applyFunctions has already made exception handling.
                            Nothing to do but prevent
                            that the exception is propagated to Thread.
                         */
                    }
                }
                finally{
                    cleanFunctions();
                    executionThread = null;
                }
            }
        });

        thread.start();
        executionThread = thread;
    }

    private void applyFunctions(final DataCollectionEvent event, AsyncCallback<Void, DataCollectionEventConsumerErrorContext> asyncCallback){
        Function<DataCollectionEvent, ?> function = null;
        try {
            final List<Function<DataCollectionEvent, ?>> functions = functionsBySensorType.get(event.getSensorType());
            if(functions == null){
                throw new APMException("No function for sensor type: " + event.getSensorType());
            }

            for(final Function<DataCollectionEvent, ?> f : functions){
                function = f;
                f.apply(event);
            }
        }
        catch(RuntimeException e){
            Log.e(LOG_TAG, "Error while executing function: " + function + "; DataCollectionEvent: " + event, e);
            asyncCallback.onError(new DataCollectionEventConsumerErrorContext(function, event, e));
            throw e;
        }
    }

    private void processRemainingEvents(AsyncCallback<Void, DataCollectionEventConsumerErrorContext> asyncCallback){
        final List<DataCollectionEvent> remainingEvents = new ArrayList<>(blockingQueue.size());
        blockingQueue.drainTo(remainingEvents);
        for (final DataCollectionEvent event : remainingEvents) {
            applyFunctions(event, asyncCallback);
        }
        Log.d(LOG_TAG, "Remaining events processed (queue = " + blockingQueue.size() + ")");
    }

    private void cleanFunctions(){
        for(final List<Function<DataCollectionEvent, ?>> functions : functionsBySensorType.values()){
            for(final Function<DataCollectionEvent, ?> f : functions){
                try{
                    f.clean();
                }
                catch(RuntimeException e){
                    Log.e(LOG_TAG, "Error while cleaning function: " + f, e);
                }
            }
        }
        Log.d(LOG_TAG, "Functions cleaned (queue = " + blockingQueue.size() + ")");
    }

    public void stop() throws APMException {
        final Thread thread = executionThread;
        if(thread == null){
            throw new APMException("DataCollectionEventConsumer não está executando");
        }

        thread.interrupt();
    }

}