/**
 * 
 */
package com.asm.android.model;

/**
 * @author vikash sharma
 *
 */
public class EventData {

	private AccelerometerData accelerometerData;
	private GyroscopeData gyroscopeData;
	private MagneticFieldData magneticFieldDataData;
	private GPSData gpsData;
	private OrientationData orientationData;
	

	/**
	 * @param accelerometerData
	 * @param gyroscopeData
	 * @param gpsData
	 * @param orientationData
	 */
	public EventData(AccelerometerData accelerometerData, GyroscopeData gyroscopeData, GPSData gpsData,
			OrientationData orientationData) {

		this.accelerometerData = accelerometerData;
		this.gyroscopeData = gyroscopeData;
		this.gpsData  = gpsData;
		this.orientationData = orientationData;
		
	}

    public EventData(AccelerometerData accelerometerData, GyroscopeData gyroscopeData, MagneticFieldData magneticFieldDataData) {
        this.accelerometerData = accelerometerData;
        this.gyroscopeData = gyroscopeData;
        this.magneticFieldDataData = magneticFieldDataData;
    }

    /**
	 * @return the accelerometerData
	 */
	public AccelerometerData getAccelerometerData() {
		return accelerometerData;
	}


	/**
	 * @param accelerometerData the accelerometerData to set
	 */
	public void setAccelerometerData(AccelerometerData accelerometerData) {
		this.accelerometerData = accelerometerData;
	}


	/**
	 * @return the gyroscopeData
	 */
	public GyroscopeData getGyroscopeData() {
		return gyroscopeData;
	}


	/**
	 * @param gyroscopeData the gyroscopeData to set
	 */
	public void setGyroscopeData(GyroscopeData gyroscopeData) {
		this.gyroscopeData = gyroscopeData;
	}


	/**
	 * @return the gpsData
	 */
	public GPSData getGpsData() {
		return gpsData;
	}


	/**
	 * @param gpsData the gpsData to set
	 */
	public void setGpsData(GPSData gpsData) {
		this.gpsData = gpsData;
	}


	/**
	 * @return the orientationData
	 */
	public OrientationData getOrientationData() {
		return orientationData;
	}


	/**
	 * @param orientationData the orientationData to set
	 */
	public void setOrientationData(OrientationData orientationData) {
		this.orientationData = orientationData;
	}


	public MagneticFieldData getMagneticFieldDataData() {
		return magneticFieldDataData;
	}

	public void setMagneticFieldDataData(MagneticFieldData magneticFieldDataData) {
		this.magneticFieldDataData = magneticFieldDataData;
	}
}
