package com.asm.android.dataLogger;

/*
 * Created by KircherEngineerH on 4/27/2016.
 */
public interface DataLoggerInterface
{
     void setHeaders(Iterable<String> headers) throws IllegalStateException;
     void addRow(Iterable<String> values) throws IllegalStateException;
     void addRowData(Iterable<Float> values) throws IllegalStateException;
     void writeToFile();
}
