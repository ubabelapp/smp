package com.asm.android.AudioEvent;

import android.content.Context;
import android.util.Log;

import com.asm.android.dataLogger.DataLoggerAudioEvent;
import com.asm.android.location.LocationTracker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Vikash Sharma
 */
public class PeakDet {

    private static final String TAG = PeakDet.class.getSimpleName();

    public static boolean IS_BELOW_THRESHOLD = false;

    private DataLoggerAudioEvent mDataLoggerAudioEvent;

    private LocationTracker mLocationTracker;

    public PeakDet(Context context){

        mDataLoggerAudioEvent = new DataLoggerAudioEvent(context);
        mDataLoggerAudioEvent.setHeaders();
        mLocationTracker = new LocationTracker(context);
	}


	public void peakDetCalculation(ArrayList<AudioDataModel> data) {

	    ArrayList<Double> vectorData = new ArrayList<>();
	    ArrayList<Double> timeSeriesData = new ArrayList<>();

	    for(int i=0; i<data.size(); i++){
	        vectorData.add(data.get(i).getAudioRMS());
            timeSeriesData.add(data.get(i).getAudioTimeStamp());
        }

        Double[] vector = new Double[vectorData.size()];
        vectorData.toArray(vector);

        Double[] timeSeries = new Double[timeSeriesData.size()];
        timeSeriesData.toArray(timeSeries);

       /* Double[] vector = new Double[] { 2.521137731, 1.154420796, 0.713032684, 0.613411136, 0.919271011, 1.680005181,
				0.832340089, 0.651679299, 2.717538265, 0.81491233, 0.688518499, 0.710291874, 1.457004331, 0.787841718,
				0.66786932, 2.67706732, 0.808592954, 0.731736151, 0.703224045, 0.620756436, 1.55416607, 0.664993323,
				2.614315209, 1.042117376, 0.696112929, 0.675502591, 0.569449779, 1.462919192, 0.637609558, 0.747987386,
				2.958455973, 0.828597538, 0.691861314, 0.624152936, 0.552882115, 1.432025783, 0.615517888, 2.32824121,
				1.272848239, 0.660679618, 0.526590594, 0.524776248, 1.107208934, 1.131675863, 0.683890247, 2.637589014,
				1.183464158, 0.846389789, 0.7392611, 0.541725851, 1.383150602, 0.832301472, 0.617993946, 2.761339908,
				0.821310779, 0.616712279, 0.595268875, 0.615830979, 1.412840385, 0.668833553, 0.6102743, 2.565101938,
				0.680753219, 0.587449901, 0.595675095, 0.574848438, 1.483206927, 0.614731347, 1.922971222, 1.622938323,
				0.61211791, 0.503805784, 0.474037981, 0.821287429, 1.147724437, 0.512896048, 2.449012802, 0.853489234,
				0.504068445, 0.417944369, 0.473167545, 1.052788249, 1.189629302, 0.543561801, 2.744743207, 0.921115533,
				0.572974334, 0.602853615, 0.546585291, 0.524130008, 1.692946523, 0.676308718, 2.838840368, 1.488089684,
				1.293452357, 2.308736914, 2.269428971, 1.282493246, 0.55485151, 0.490387383, 0.510614768, 0.515819319,
				1.805580689, 2.915134989, 0.91177059, 0.492779813, 0.441364979, 1.742874423, 0.532691347, 2.444110754,
				0.882981981, 0.583346333, 0.492621154, 0.450582733, 1.234983479, 0.510098232, 1.801846296, 0.562113934,
				0.501801305, 0.464261437, 0.512131756, 1.308878635, 1.791665635, 0.706808181, 0.481295061, 0.505062146,
				0.465008999
		};

		Double[] timeSeries = new Double[] { 0.0, 0.0853333324193954, 0.1706666648387909, 0.2560000121593475,
				0.3413333296775818, 0.4266666769981384, 0.5120000243186951, 0.5973333120346069, 0.6826666593551636,
				0.7680000066757202, 0.8533333539962769, 0.9386666417121887, 1.0240000486373901, 1.1093332767486572,
				1.1946666240692139, 1.2799999713897705, 1.3653333187103271, 1.4506666660308838, 1.5360000133514404,
				1.621333360671997, 1.7066667079925537, 1.7920000553131104, 1.8773332834243774, 1.962666630744934,
				2.0480000972747803, 2.133333444595337, 2.2186665534973145, 2.303999900817871, 2.3893332481384277,
				2.4746665954589844, 2.559999942779541, 2.6453332901000977, 2.7306666374206543, 2.815999984741211,
				2.9013333320617676, 2.986666679382324, 3.072000026702881, 3.1573333740234375, 3.242666721343994,
				3.328000068664551, 3.4133334159851074, 3.498666763305664, 3.5840001106262207, 3.6693332195281982,
				3.754666566848755, 3.8399999141693115, 3.925333261489868, 4.010666847229004, 4.0960001945495605,
				4.181333541870117, 4.266666889190674, 4.3520002365112305, 4.437333106994629, 4.5226664543151855,
				4.607999801635742, 4.693333148956299, 4.7786664962768555, 4.863999843597412, 4.949333190917969,
				5.034666538238525, 5.119999885559082, 5.205333232879639, 5.290666580200195, 5.375999927520752,
				5.461333274841309, 5.546666622161865, 5.631999969482422, 5.7173333168029785, 5.802666664123535,
				5.888000011444092, 5.973333358764648, 6.058666706085205, 6.144000053405762, 6.229333400726318,
				6.314666748046875, 6.400000095367432, 6.485333442687988, 6.570666790008545, 6.656000137329102,
				6.741333484649658, 6.826666831970215, 6.9120001792907715, 6.997333526611328, 7.082666873931885,
				7.168000221252441, 7.253333568572998, 7.3386664390563965, 7.423999786376953, 7.50933313369751,
				7.594666481018066, 7.679999828338623, 7.76533317565918, 7.850666522979736, 7.935999870300293,
				8.021333694458008, 8.106666564941406, 8.192000389099121, 8.27733325958252, 8.362667083740234,
				8.447999954223633, 8.533333778381348, 8.618666648864746, 8.704000473022461, 8.78933334350586,
				8.874666213989258, 8.960000038146973, 9.045332908630371, 9.130666732788086, 9.215999603271484,
				9.3013334274292, 9.386666297912598, 9.472000122070312, 9.557332992553711, 9.642666816711426,
				9.727999687194824, 9.813333511352539, 9.898666381835938, 9.984000205993652, 10.06933307647705,
				10.154666900634766, 10.239999771118164, 10.325333595275879, 10.410666465759277, 10.496000289916992,
				10.58133316040039, 10.666666984558105, 10.751999855041504 };*/

		List<Peak> peaks = peakdet(vector, 1);
		List<Integer> timeDiffList = new ArrayList<>();
		HashMap<Integer,PeakDataModel> mapDataList = new HashMap<>();

		Log.e(TAG,"length of the vector is " + vector.length);
		Log.e(TAG,"total peaks found are " + peaks.size());
		Peak lastPeak = new Peak(0, 0);
		int count = 0;

		if(peaks.size() <= 3){
		    return;
        }

		for (Peak peak : peaks) {
			int timeDiff;
			if (count++ == 0) {
				timeDiff = 0;
			} else {
				timeDiff = (int) (timeSeries[peak.index] * 1000 - timeSeries[lastPeak.index] * 1000);
			}

			if(mapDataList.containsKey(timeDiff)){
			    PeakDataModel peakDataModel = mapDataList.get(timeDiff);
			    peakDataModel.setEndTime(timeSeries[peak.index]);
                mapDataList.put(timeDiff, peakDataModel);

            }else{
                PeakDataModel peakDataModel = new PeakDataModel(timeSeries[peak.index],0.0);
			  /*  PeakDataModel peakDataModel = new PeakDataModel(timeSeries[peak.index],
                        0.0, 0.0,data.get(peak.index).getLatitude(),
                        data.get(peak.index).getLongitude(),data.get(peak.index).getSpeed(),
                        data.get(peak.index).getBearing(),data.get(peak.index).getTimeStamp());*/
                mapDataList.put(timeDiff, peakDataModel);
			}

			timeDiffList.add(timeDiff);
			Log.e(TAG,"timestamp : " + timeDiff + "  ->" + peak);
			lastPeak = peak;
		}

        IS_BELOW_THRESHOLD = false;
		Set<Integer> unique = new HashSet<>(timeDiffList);
		int firstTimeOccurance= 0;
		int secondTimeOccurance= 0;
		int max = 0;
		int curr;
		int currKey = 0;
		int firstTimeKey = 0;
		int secondTimeKey = 0;
		for (Integer key : unique) {
			curr = Collections.frequency(timeDiffList, key);

			if (max < curr) {
				max = curr;
				currKey = key;
			}
		}
        firstTimeKey = currKey;

		Log.e(TAG,"First  time gap  " + firstTimeKey + " happens " + max + " times");
		firstTimeOccurance = max;

		max = 0;
		curr = 0;
		currKey = 0;

		for (Integer key : unique) {
			curr = Collections.frequency(timeDiffList, key);
			if (key == firstTimeKey)
				continue;
			if (max < curr) {
				max = curr;
				currKey = key;
			}
		}
        secondTimeKey = currKey;


		Log.e(TAG, "Second Time Gap  " + secondTimeKey + " happens " + max + " times");
		secondTimeOccurance = max;

		int totalOccurance = firstTimeOccurance + secondTimeOccurance;
		Log.e("PeakDet", "total occurance :"+totalOccurance);

		int keyToCheck;
		if(firstTimeKey > secondTimeKey){
		    keyToCheck = firstTimeKey;
        }else{
		    keyToCheck = secondTimeKey;
        }

        PeakDataModel data1 = mapDataList.get(keyToCheck);

		double startTime = data1.getStartTime();
		double endTime = data1.getEndTime();
		double diff = endTime - startTime;

		if(totalOccurance >=4 && totalOccurance <=10){
            IS_BELOW_THRESHOLD = true;
        }
        if(diff >= 7){
		    Log.e(TAG, "Signal event detected");
		    // TODO: add this event to some list for comparing with vSense
            ArrayList<String> rowData = new ArrayList<>();
            rowData.add(String.valueOf(startTime));
            rowData.add(String.valueOf(endTime));
            rowData.add(String.valueOf(mLocationTracker.getLatitude()));
            rowData.add(String.valueOf(mLocationTracker.getLongitude()));
            rowData.add(String.valueOf(mLocationTracker.getSpeed()));
            rowData.add(String.valueOf(mLocationTracker.getBearing()));
            rowData.add("Event detected : time difference was :"+diff);
            rowData.add(String.valueOf(System.currentTimeMillis()));
            mDataLoggerAudioEvent.addRow(rowData);
            rowData.clear();
        }
	}

	private List<Peak> peakdet(Double[] vector, double triggerDelta) {
		return peakdet(vector, 0, vector.length, triggerDelta);
	}

	/*
	 * !PEAKDET Detect peaks in a vector ! ! call PEAKDET(MAXTAB, MINTAB, N, V,
	 * DELTA) finds the local ! maxima and minima ("peaks") in the vector V of size
	 * N. ! MAXTAB and MINTAB consists of two columns. Column 1 ! contains indices
	 * in V, and column 2 the found values. ! ! call PEAKDET(MAXTAB, MINTAB, N, V,
	 * DELTA, X) replaces the ! indices in MAXTAB and MINTAB with the corresponding
	 * X-values. ! ! A point is considered a maximum peak if it has the maximal !
	 * value, and was preceded (to the left) by a value lower by ! DELTA. ! ! Eli
	 * Billauer, 3.4.05 (http://billauer.co.il) ! Translated into Fortran by Brian
	 * McNoldy (http://andrew.rsmas.miami.edu/bmcnoldy) ! This function is released
	 * to the public domain; Any use is allowed.
	 */
	private List<Peak> peakdet(Double[] vector, int offset, int length, double triggerDelta) {
		double mn = Double.POSITIVE_INFINITY;
		double mx = Double.NEGATIVE_INFINITY;
		double mnpos = Double.NaN;
		double mxpos = Double.NaN;
		int lookformax = 1;

		List<Peak> maxtab_tmp = new ArrayList<>();
		// List<Valley> mintab_tmp = new ArrayList<>();

		for (int i = offset; i < length; i++) {
			double a = vector[i];
			if (a > mx) {
				mx = a;
				mxpos = vector[i];
			}
			if (a < mn) {
				mn = a;
				mnpos = vector[i];
			}
			if (lookformax == 1) {
				if (a < mx - triggerDelta) {
					maxtab_tmp.add(new Peak(mxpos, i));
					mn = a;
					mnpos = vector[i];
					lookformax = 0;
				}
			} else {
				if (a > mn + triggerDelta) {
					// mintab_tmp.add(new Valley(mnpos, i));
					mx = a;
					mxpos = vector[i];
					lookformax = 1;
				}
			}
		}

		return maxtab_tmp;
	}

	static class Peak {

		public final double height;
		public final int index;

		private Peak(double height, int index) {
			this.height = height;
			this.index = index;
		}

		@Override
		public String toString() {
			return "Peak{" + "height=" + height + ", index=" + index + '}';
		}

	}

	static class Valley {

		public final double height;
		public final int index;

		private Valley(double height, int index) {
			this.height = height;
			this.index = index;
		}

		@Override
		public String toString() {
			return "Valley{" + "height=" + height + ", index=" + index + '}';
		}

	}


	public void stopLogging(){
	    if(mDataLoggerAudioEvent != null){
	        mDataLoggerAudioEvent.stopLogging();
        }

        if(mLocationTracker != null){
	        mLocationTracker.stopLocationUpdates();
        }
    }
}
