package com.asm.android.async;


public interface ErrorContext {
    Throwable getError();
}
