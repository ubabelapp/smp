package com.asm.android.eventProcess;

import com.asm.android.model.EventData;
import com.asm.android.processFactory.ProcessFactory;

/**
 * @author Vikash Sharma
 *
 */
public interface EventValidationProcess {
	
	public ProcessFactory getProcessType(ProcessFactory processFactory);
	public EventStats processData(EventData eventData);
	
}
