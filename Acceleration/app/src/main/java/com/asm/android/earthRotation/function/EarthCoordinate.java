package com.asm.android.earthRotation.function;


public interface EarthCoordinate {
    boolean getRotationMatrix(float[] R, float[] I, float[] gravity, float[] geomagnetic);
}
