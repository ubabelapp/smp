package com.accelerationraw.android.async;


public interface AsyncCallback<S,E extends ErrorContext> {
    void onSuccess(S successContext);
    void onError(E errorContext);
}
