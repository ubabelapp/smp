package com.accelerationraw.android.dataLogger;

import android.content.Context;
import android.os.Environment;

import com.accelerationraw.android.utils.Util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Sourabh on 21-02-2018.
 */

public class DataLoggerEarthRotation {

    private static final String TAG = DataLoggerEarthRotation.class.getSimpleName();
    private final static String DEFAULT_APPLICATION_DIRECTORY = "ABC_SENSOR_DATA_RAW";

    private ArrayList<String> csvHeaders;

    private DataLoggerInterface dataLogger;
    private Context context;

    public DataLoggerEarthRotation(Context context) {
        this.context = context;
        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<Float> value){
        dataLogger.addRowData(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return "SensorDataEarthRotation" +".csv";
    }

    private ArrayList<String> getCsvHeaders() {
        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("AccEventTimeInMillis");
        headers.add("Acc-X-value");
        headers.add("Acc-Y-value");
        headers.add("Acc-Z-value");
        headers.add("Gyro-X-value");
        headers.add("Gyro-Y-value");
        headers.add("Gyro-Z-value");
        headers.add("Linear-Acc-X-value");
        headers.add("Linear-Acc-Y-value");
        headers.add("Linear-Acc-Z-value");
        headers.add("Gravity X-value");
        headers.add("Gravity Y-value");
        headers.add("Gravity Z-value");
        return headers;
    }
}

