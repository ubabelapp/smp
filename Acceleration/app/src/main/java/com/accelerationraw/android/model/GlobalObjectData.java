package com.accelerationraw.android.model;

/**
 * Created by Sourabh on 15-03-2018.
 */

public class GlobalObjectData {

    private AccelerometerData accelerometerData;
    private GyroscopeData gyroscopeData;


    public AccelerometerData getAccelerometerData() {
        return accelerometerData;
    }

    public void setAccelerometerData(AccelerometerData accelerometerData) {
        this.accelerometerData = accelerometerData;
    }

    public GyroscopeData getGyroscopeData() {
        return gyroscopeData;
    }

    public void setGyroscopeData(GyroscopeData gyroscopeData) {
        this.gyroscopeData = gyroscopeData;
    }

    public GlobalObjectData(AccelerometerData accelerometerData, GyroscopeData gyroscopeData) {

        this.accelerometerData = accelerometerData;
        this.gyroscopeData = gyroscopeData;
    }
}
