package com.accelerationraw.android.AudioEvent;

/**
 * Created by Sourabh on 17-03-2018.
 */

public class AudioDataModel {

    private double audioTimeStamp;
    private double audioRMS;
    private long timeStamp;
    private double latitude;
    private double longitude;
    private double speed;
    private double bearing;

    public AudioDataModel(double audioTimeStamp, double audioRMS, long timeStamp, double latitude, double longitude, double speed, double bearing) {
        this.audioTimeStamp = audioTimeStamp;
        this.audioRMS = audioRMS;
        this.timeStamp = timeStamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.bearing = bearing;
    }

    public double getAudioTimeStamp() {
        return audioTimeStamp;
    }

    public void setAudioTimeStamp(double audioTimeStamp) {
        this.audioTimeStamp = audioTimeStamp;
    }

    public double getAudioRMS() {
        return audioRMS;
    }

    public void setAudioRMS(double audioRMS) {
        this.audioRMS = audioRMS;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }
}
