package com.accelerationraw.android.earthRotation;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.SensorEvent;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;

import com.accelerationraw.android.activity.SensorData;
import com.accelerationraw.android.async.AsyncCallback;
import com.accelerationraw.android.async.AsyncCallbackUtil;
import com.accelerationraw.android.earthRotation.event.DataCollectionEvent;
import com.accelerationraw.android.earthRotation.function.DataCollectionEventWriterFunction;
import com.accelerationraw.android.earthRotation.function.EarthAxesConverterFunction;
import com.accelerationraw.android.earthRotation.function.EarthCoordinate;
import com.accelerationraw.android.earthRotation.function.Function;
import com.accelerationraw.android.earthRotation.function.FunctionUtil;
import com.accelerationraw.android.earthRotation.function.SensorManagerEarthCoordinate;

import java.io.File;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Sourabh on 08-03-2018.
 */

public class EulerCorrection {

    private static final String NEXT_TRIP_ID_LABEL = "nextTripId";
    private static final long NEXT_TRIP_ID_INITIAL_VALUE = 1;
    private static final String APP_HOME_DIR = "ABC_EULER_DATA";

    private long nextTripId;
    private Trip currentTrip;
    private LocationManager locationManager;
    private File home;
    private BlockingQueue<DataCollectionEvent> blockingQueue;
    private DataCollectionEventConsumer dataCollectionEventConsumer;
    private Context mContext;

    public EulerCorrection(Context context){
        this.mContext = context;
        init();
    }

    private void init(){

        blockingQueue = new LinkedBlockingDeque<>();
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        home = new File(Environment.getExternalStorageDirectory(), APP_HOME_DIR);
        IOUtil.ensureMkDirs(home);
        nextTripId = getSharedPreferences().getLong(NEXT_TRIP_ID_LABEL, NEXT_TRIP_ID_INITIAL_VALUE);
    }

    public void start(){

        final AsyncCallback<Trip, TripErrorContext> asyncCallback = new AsyncCallback<Trip, TripErrorContext>() {
            @Override
            public void onSuccess(final Trip trip) {
            }
            @Override
            public void onError(final TripErrorContext errorContext) {
            }
        };
        startTrip(asyncCallback);
    }

    public void addData(SensorEvent event){

        SensorType st = SensorType.getByAndroidId(event.sensor.getType());
        DataCollectionEvent dataCollectionEvent = st.createDataCollectionEvent(event,
                new Date(System.currentTimeMillis()),System.currentTimeMillis());
        blockingQueue.add(dataCollectionEvent);
    }


    private DataCollectionEventConsumer buildDataCollectionEventConsumer(){

        DataCollectionEventConsumer consumer = new DataCollectionEventConsumer(blockingQueue);
        EarthCoordinate earthCoordinate = new SensorManagerEarthCoordinate();

        for(SensorType st : SensorType.values()){
            consumer.addFunction(st,
                    new DataCollectionEventWriterFunction<>(new File(currentTrip.getHome(), st.getFileName() + ".csv"),
                            "dd/MM/yyyy HH:mm:ss"));

            if(st == SensorType.ROTATION_VECTOR || st == SensorType.GPS){
                continue;
            }

            EarthAxesConverterFunction earthAxesConverterFunction = new EarthAxesConverterFunction(earthCoordinate, st);
            DataCollectionEventWriterFunction dataCollectionEventWriterFunction =
                    new DataCollectionEventWriterFunction<>(new File(currentTrip.getHome(), st.getFileName() + "_terra.csv"), "dd/MM/yyyy HH:mm:ss");
            Function<DataCollectionEvent, Void> earthAxesConverterAndWriter = FunctionUtil.chainWhenNonNull(earthAxesConverterFunction, dataCollectionEventWriterFunction);
            consumer.addFunction(st, earthAxesConverterAndWriter);

            for(SensorType sourceType : EarthAxesConverterFunction.getSourceSensorTypes()){
                consumer.addFunction(sourceType, earthAxesConverterAndWriter);
            }
        }
        return consumer;
    }

    private void startTrip(final AsyncCallback<Trip, TripErrorContext> asyncCallback) throws APMException{

        currentTrip = createTrip();
        currentTrip.save();
        final AsyncCallback<Trip, TripErrorContext> quietAsyncCallback = AsyncCallbackUtil.makeQuiet(asyncCallback);

        final Trip cTrip = currentTrip;
        dataCollectionEventConsumer = buildDataCollectionEventConsumer();
        dataCollectionEventConsumer.start(new AsyncCallback<Void, DataCollectionEventConsumerErrorContext>() {

            @Override
            public void onSuccess(Void successContext) {
                quietAsyncCallback.onSuccess(cTrip);
            }

            @Override
            public void onError(DataCollectionEventConsumerErrorContext errorContext){
                try{
                    finishTrip(errorContext.getError());
                }
                finally{
                    quietAsyncCallback.onError(new TripErrorContext(cTrip, errorContext));
                }
            }
        });
    }

    public void finishTrip() throws APMException {
        finishTrip(null);
    }

    private void finishTrip(Throwable error) throws APMException {
        if(!isTripActive()){
            throw new APMException("There is no active trip");
        }
        if(error == null) {
            dataCollectionEventConsumer.stop();
        }
        dataCollectionEventConsumer = null;
        currentTrip.setFinish(new Date(System.currentTimeMillis()));
        currentTrip = null;
    }

    public Trip getCurrentTrip() {
        return currentTrip;
    }

    private boolean isTripActive(){
        return currentTrip != null;
    }

    private long nextTripId(){

        long nextId = nextTripId++;
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putLong(NEXT_TRIP_ID_LABEL, nextTripId);
        editor.apply();
        return nextId;
    }

    private SharedPreferences getSharedPreferences(){
        return mContext.getSharedPreferences(SensorData.class.getName(), Context.MODE_PRIVATE);
    }

    private Trip createTrip(){

        Trip v = new Trip(nextTripId(), home);
        v.setSmartphoneModel(Build.MODEL);
        v.setAndroidVersion(Build.VERSION.RELEASE);
        v.setStart(new Date(System.currentTimeMillis()));
        v.setLocationProvider(getLocationProvider());
        return v;
    }

    private LocationProvider getLocationProvider(){
        boolean available = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        return available ? locationManager.getProvider(LocationManager.GPS_PROVIDER) : null;
    }

}
