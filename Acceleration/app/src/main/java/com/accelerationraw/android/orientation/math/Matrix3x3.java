package com.accelerationraw.android.orientation.math;


public class Matrix3x3 {

    public static float[] multiplication(float[] A, float[] B) {
        float[] result = new float[9];

        /*

          gyroMatrix[0] = 1.0f; gyroMatrix[1] = 0.0f; gyroMatrix[2] = 0.0f;
        gyroMatrix[3] = 0.0f; gyroMatrix[4] = 1.0f; gyroMatrix[5] = 0.0f;
        gyroMatrix[6] = 0.0f; gyroMatrix[7] = 0.0f; gyroMatrix[8] = 1.0f;

         */


        result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
        result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
        result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];

        result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
        result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
        result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];

        result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
        result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
        result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];

        return result;
    }


    public static float[] multiplyTranspose(float[] A, float[] B) {
        // position_new = multiply ( transformation (3x3) * transpose(3x1)  )


        float result[] = new float[3];
        result[0] = A[0] * B[0] + A[1] * B[1] + A[2] * B[2];
        result[1] = A[3] * B[0] + A[4] * B[1] + A[5] * B[2];
        result[2] = A[6] * B[0] + A[7] * B[1] + A[8] * B[2];

        return result;
    }


}
