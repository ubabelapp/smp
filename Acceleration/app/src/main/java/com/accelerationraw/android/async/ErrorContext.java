package com.accelerationraw.android.async;


public interface ErrorContext {
    Throwable getError();
}
