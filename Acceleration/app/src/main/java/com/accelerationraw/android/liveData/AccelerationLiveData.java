package com.accelerationraw.android.liveData;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

import com.accelerationraw.android.R;
import com.accelerationraw.android.constant.Constants;
import com.accelerationraw.android.constant.SharedPrefConstants;
import com.accelerationraw.android.filter.LPFAndroidDeveloper;
import com.accelerationraw.android.filter.LowPassFilter;
import com.accelerationraw.android.filter.MeanFilterByArray;
import com.accelerationraw.android.filter.MeanFilterByValue;
import com.accelerationraw.android.location.LocationTracker;
import com.accelerationraw.android.model.AccelerometerData;
import com.accelerationraw.android.model.AngularAxis;
import com.accelerationraw.android.model.ButtonDataModel;
import com.accelerationraw.android.model.EnergyDataModel;
import com.accelerationraw.android.model.GyroscopeData;
import com.accelerationraw.android.model.LinearAccelerationData;
import com.accelerationraw.android.model.MagneticFieldData;
import com.accelerationraw.android.model.RotationVectorData;
import com.accelerationraw.android.model.SpeedDataModel;
import com.accelerationraw.android.orientation.sensors.Orientation;
import com.accelerationraw.android.orientation.utils.OrientationSensorInterface;
import com.accelerationraw.android.representation.MatrixF4x4;
import com.accelerationraw.android.representation.Quaternion;
import com.accelerationraw.android.utils.SharedPreferenceManager;
import com.accelerationraw.android.utils.Util;
import com.kircherelectronics.fsensor.filter.fusion.OrientationComplimentaryFusion;
import com.kircherelectronics.fsensor.filter.fusion.OrientationFusion;
import com.kircherelectronics.fsensor.linearacceleration.LinearAcceleration;
import com.kircherelectronics.fsensor.linearacceleration.LinearAccelerationFusion;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class AccelerationLiveData<T> extends LiveData<float[]> implements OrientationSensorInterface {

    private static final String TAG = AccelerationLiveData.class.getSimpleName();

    private SensorManager sensorManager;
    private SimpleSensorListener listener;

    private float[] accelerationListValues = new float[40];
    private float[] acceleration = new float[3];

    private MeanFilterByValue meanFilterMagnitude;
    private MeanFilterByArray meanFilterAcceleration;
    private LowPassFilter lowPassFilter;

    private boolean linearAccelerationActive = true;
    private float[] lpfAcceleration = new float[3];
    private boolean staticAlpha = true;
    private float lpfStaticAlpha = 0.3f;

    private float magnitude = 0;

    private boolean mIsAccDataPresent = false;
    private boolean mIsGyroDataPresent = false;
    private float gyroXAxis = 0.0f;
    private float gyroYAxis = 0.0f;
    private float gyroZAxis = 0.0f;

    private double gyroscopeRotationVelocity = 0;
    private long timestamp;
    private int panicCounter;

    private static final float NS2S = 1.0f / 1000000000.0f;
    private static final double EPSILON = 0.1f;
    private static final float OUTLIER_THRESHOLD = 0.85f;
    private static final float OUTLIER_PANIC_THRESHOLD = 0.65f;
    private static final int PANIC_THRESHOLD = 60;
    private static final float DIRECT_INTERPOLATION_WEIGHT = 0.005f;

    private Quaternion deltaQuaternion = new Quaternion();
    private Quaternion quaternionGyroscope = new Quaternion();
    private Quaternion quaternionRotationVector = new Quaternion();
    private Quaternion interpolatedQuaternion = new Quaternion();
    private Quaternion correctedQuaternion = new Quaternion();
    private Object synchronizationToken = new Object();
    private MatrixF4x4 currentOrientationRotationMatrix = new MatrixF4x4();
    private Quaternion currentOrientationQuaternion = new Quaternion();


    private float[] temporaryQuaternion = new float[4];
    private boolean positionInitialised = false;

    private float deltaQuaternionXAxis = 0;
    private float deltaQuaternionYAxis = 0;
    private float deltaQuaternionZAxis = 0;

    private boolean filterReady = false;
    private int filterCount = 0;
    private float thresholdMax = 0.5f;
    private float thresholdMin = 0.15f;
    private int thresholdCountMax = 0;
    private int thresholdCountMin = 0;
    private int thresholdCountMaxLimit = 3;
    private int thresholdCountMinLimit = 5;
    private boolean accelerationEventActive = false;
    private Context mContext;
    private Orientation orientationSensor;
    private double azimuth = 0;
    private double roll = 0;
    private double pitch = 0;
    private double yaw = 0;
    private double deviation = 0;
    private boolean mIsPitchMovementInRange = false;
    private LocationTracker locationTracker;
    long activityStartTime;
    long lastActivityLogTime = 0;
    private boolean mIsOneTime = false;
    private String ts;
    private boolean mTimeSet = false;
    private long mTimeStamp = 0;

    private LinearAcceleration linearAccelerationFilterComplimentary;
    private OrientationFusion orientationFusionComplimentary;
    private ExecutorService executor;
    private ExecutorService sensorExecutor;
    private ArrayList<EnergyDataModel> mDeviationList= new ArrayList<>();
    private ArrayList<EnergyDataModel> mDeviationTempList= new ArrayList<>();
    private boolean mIsOneTimeEnergyData = false;
    private long logTime = 0;
    private boolean mSingleLogTime = false;

    private static final int MIN_THRESHOLD = 50;
    private static final int MAX_THRESHOLD = 90;

    private static final int LANE_CHANGE = 0;
    private static final int HARSH_LANE_CHANGE = 1;
    private static final int LR_TURN = 2;
    private static final int U_TURN = 3;
    private static final int HARSH_BREAK = 4;
    private static final int FREQUENT_LANE_CHANGE = 5;

    private boolean mManeuverStarted = false;
    private boolean mIsThresholdAchieved = false;
    private boolean mIsManeuverEnded = false;
    private double mMaxValue = 0;
    private double mMinValue = 0;
    private long mManeuverStartTime;
    private long mManeuverEndTime;

    private boolean mLastManevurStarted = false;
    private double mOldAzimuthValue = 0.0;

    private boolean mWentDown = false;
    private int mCounter = 0;
    private ArrayList<SpeedDataModel> speedList = new ArrayList<>();
    private double totalDistance = 0;

    private boolean First = true;
    private boolean FirstSensor = true;
    private float[] prevValues = new float[3];
    private float[] speed = new float[3];
    float[] currentValues = new float[3];
    float[] currentVelocity = new float[3];
    private float prevTime, currentTime, changeTime,distanceY,distanceX,distanceZ;
    private Float currentAcceleration = 0.0F;
    private  Float currentDirection = 0.0F;
    private Float CurrentSpeed = 0.0F;
    private Float CurrentDistanceTravelled = 0.0F;
    private Float prevAcceleration = 0.0F;
    private Float prevSpeed = 0.0F;
    private Float prevDistance = 0.0F;
    private float[] AccelerometerValue;
    private float[] MagnetometerValue;
    private float[] linearAcceleroValue;

    private double deltaTime = 0;
    private double totalDis = 0;

    private ArrayList<ButtonDataModel> mButtonDataList = new ArrayList<>();
    private ArrayList<ButtonDataModel> mButtonDataTempList = new ArrayList<>();
    private long mFileWriteTimeStamp;
    private double yawInRadian = 0;
    private double yawOldInRadian = 0;
    private double yawDeviation = 0;
    private boolean mIsOneButtonData = false;
    private boolean mIsLogTimeCalled = false;
    private long mLogTime =0;
    private float[] mGyroFilterData = new float[3];
    private com.kircherelectronics.fsensor.filter.averaging.LowPassFilter lpfAccelerationSmoothing;
    private double mEventStatus = 0.0;

    private int mHarshBrakeCounter = 0;
    private int mHarshLaneChangeCounter = 0;
    private int mLaneChangeCounter = 0;
    private int mUTurnCounter = 0;
    private int mLRTurnCounter = 0;
    private double mGpsSpeed = 0;
    private List<AngularAxis> rotationSamples = new ArrayList<>();
    private List<AngularAxis> tmpSampleList = new ArrayList<>();
    private double yawOldInAngle = 0.0;
    private int mProcessType = 0;

    private LinkedList<AccelerometerData> mAccelerometerDataList = new LinkedList<>();
    private LinkedList<GyroscopeData> mGyroscopeDataList = new LinkedList<>();
    private LinkedList<LinearAccelerationData> mLinearAccelerationDataList = new LinkedList<>();
    private LinkedList<MagneticFieldData> mMagneticDataList = new LinkedList<>();
    private LinkedList<RotationVectorData> mRotationVectorDataList = new LinkedList<>();
    private Thread consumerThread;


    public AccelerationLiveData(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        listener = new SimpleSensorListener();
        this.mContext = context;
        orientationSensor = new Orientation(mContext, this);
        locationTracker = new LocationTracker(context);
        processingThread();
        initFilters();
        activityStartTime= System.currentTimeMillis();
        initializeFSensorFusions();
        executor = Executors.newFixedThreadPool(5);//creating a pool of 5 threads
        sensorExecutor = Executors.newFixedThreadPool(5);//creating a pool of 5 threads

        if(!mSingleLogTime){
            mSingleLogTime = true;
            logTime = System.currentTimeMillis();
        }
    }

    @Override
    protected void onActive() {
        registerSensors();
    }

    @Override
    protected void onInactive(){
        unregisterSensors();
        stopOrientation();
        if(consumerThread != null){
            consumerThread.interrupt();
        }
    }

    private void initFilters(){

        lowPassFilter = new LPFAndroidDeveloper();
        lowPassFilter.setAlphaStatic(staticAlpha);
        lowPassFilter.setAlpha(lpfStaticAlpha);

        meanFilterMagnitude = new MeanFilterByValue();
        meanFilterMagnitude.setWindowSize(10);

        // Mean filter for the acceleration...
        meanFilterAcceleration = new MeanFilterByArray();
        meanFilterAcceleration.setWindowSize(10);
    }

    private void initializeFSensorFusions() {

        lpfAccelerationSmoothing = new com.kircherelectronics.fsensor.filter.averaging.LowPassFilter();
        orientationFusionComplimentary = new OrientationComplimentaryFusion();
        linearAccelerationFilterComplimentary = new LinearAccelerationFusion(orientationFusionComplimentary);
    }

    private void unregisterSensors() {
        sensorManager.unregisterListener(listener);
    }

    private void registerSensors() {

        linearAccelerationFilterComplimentary.reset();
        orientationFusionComplimentary.reset();
        lpfAccelerationSmoothing.reset();

        sensorManager.registerListener(listener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(listener, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(listener, sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(listener, sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(listener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_FASTEST);
        startOrientation();
    }

    private class SimpleSensorListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent event) {

            if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                mRotationVectorDataList.add(new RotationVectorData(event.values[0],event.values[1],event.values[2], event.timestamp));
                //rotationVectorCalculation(event);
            }

            else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
                mGyroscopeDataList.add(new GyroscopeData(event.values[0],event.values[1],event.values[2], event.timestamp));
                //gyroscopeCalculation(event);
            }

            else if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mAccelerometerDataList.add(new AccelerometerData(event.values[0],event.values[1],event.values[2],
                        event.timestamp, System.currentTimeMillis()));
                //accelerationDataCalculation(event);
            }

            else if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){
                mLinearAccelerationDataList.add(new LinearAccelerationData(event.values[0],event.values[1],event.values[2], event.timestamp));
                //linearAccelerationDataCalculation(event);
            }

            else if(event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
                mMagneticDataList.add(new MagneticFieldData(event.values[0],event.values[1],event.values[2], event.timestamp));
                //MagnetometerValue = event.values;
            }


           /* linearAndMagnetoCalculation();
            if(locationTracker.getOverSpeeding()){
                mEventStatus = Constants.OVER_SPEEDING;
                locationTracker.setSpeedThreshold();
            }

            if(mIsAccDataPresent && mIsGyroDataPresent) {

                mIsGyroDataPresent = mIsAccDataPresent = false;

                processAcceleration(acceleration, gyroXAxis, gyroYAxis, gyroZAxis, gyroscopeRotationVelocity,
                        deltaQuaternionXAxis,deltaQuaternionYAxis,deltaQuaternionZAxis, magnitude,
                        totalDis, deltaTime, mGpsSpeed, mEventStatus,linearAcceleroValue,
                        mGyroFilterData,CurrentSpeed,currentAcceleration,CurrentDistanceTravelled,currentDirection,
                        locationTracker.getLatitude(),locationTracker.getLongitude(),locationTracker.getSpeed(),
                        locationTracker.getBearing(),locationTracker.getBearingAccuracy(),locationTracker.getAltitude(),
                        locationTracker.getSpeedAccuracy());
            }*/
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {}
    }


    private void processingThread(){

        /*consumerThread = new Thread(new Runnable(){
            @Override
            public void run(){
                processSensorCalculation();
            }
        });
        consumerThread.start();*/
        SensorThread sensorThread = new SensorThread();
        consumerThread = new Thread(sensorThread);
        consumerThread.start();
    }

    private void processSensorCalculation(){

        while (true){

            if (mRotationVectorDataList.size()> 0 && mGyroscopeDataList.size() >0 &&
                    mAccelerometerDataList.size()>0 && mLinearAccelerationDataList.size() >0 &&
                    mMagneticDataList.size() >0) {

                long startTime = System.currentTimeMillis();
                float[] rotationVectorValues = new float[3];
                float[] gyroscopeValues = new float[3];
                float[] accelerationValues = new float[3];
                float[] linearAccelerationValues = new float[3];
                float[] magneticFieldValues = new float[3];
                long rotationVectorTimeStamp;
                long gyroscopeTimeStamp;
                long accelerationTimeStamp;
                long linearAccelerationTimeStamp;
                long magneticFieldStamp;

                rotationVectorValues[0] = mRotationVectorDataList.get(0).getX();
                rotationVectorValues[1] = mRotationVectorDataList.get(0).getY();
                rotationVectorValues[2] = mRotationVectorDataList.get(0).getZ();
                rotationVectorTimeStamp = mRotationVectorDataList.get(0).getTimestamp();
                mRotationVectorDataList.removeFirst();

                gyroscopeValues[0] = mGyroscopeDataList.get(0).getX();
                gyroscopeValues[1] = mGyroscopeDataList.get(0).getY();
                gyroscopeValues[2] = mGyroscopeDataList.get(0).getZ();
                gyroscopeTimeStamp = mGyroscopeDataList.get(0).getTimestamp();
                mGyroscopeDataList.removeFirst();

                accelerationValues[0] = mAccelerometerDataList.get(0).getX();
                accelerationValues[1] = mAccelerometerDataList.get(0).getY();
                accelerationValues[2] = mAccelerometerDataList.get(0).getZ();
                accelerationTimeStamp = mAccelerometerDataList.get(0).getTimestamp();
                mAccelerometerDataList.removeFirst();

                linearAccelerationValues[0] = mLinearAccelerationDataList.get(0).getX();
                linearAccelerationValues[1] = mLinearAccelerationDataList.get(0).getY();
                linearAccelerationValues[2] = mLinearAccelerationDataList.get(0).getZ();
                linearAccelerationTimeStamp = mLinearAccelerationDataList.get(0).getTimestamp();
                mLinearAccelerationDataList.removeFirst();

                magneticFieldValues[0] = mMagneticDataList.get(0).getX();
                magneticFieldValues[1] = mMagneticDataList.get(0).getY();
                magneticFieldValues[2] = mMagneticDataList.get(0).getZ();
                magneticFieldStamp = mMagneticDataList.get(0).getTimestamp();
                mMagneticDataList.removeFirst();

                rotationVectorCalculation(rotationVectorValues,rotationVectorTimeStamp);
                gyroscopeCalculation(gyroscopeValues,gyroscopeTimeStamp);
                accelerationDataCalculation(accelerationValues,accelerationTimeStamp);
                linearAccelerationDataCalculation(linearAccelerationValues,linearAccelerationTimeStamp);
                MagnetometerValue = magneticFieldValues;
                linearAndMagnetoCalculation();
                processAcceleration(acceleration, gyroXAxis, gyroYAxis, gyroZAxis, gyroscopeRotationVelocity,
                        deltaQuaternionXAxis,deltaQuaternionYAxis,deltaQuaternionZAxis, magnitude,
                        totalDis, deltaTime, mGpsSpeed, mEventStatus,linearAcceleroValue,
                        mGyroFilterData,CurrentSpeed,currentAcceleration,CurrentDistanceTravelled,currentDirection,
                        locationTracker.getLatitude(),locationTracker.getLongitude(),locationTracker.getSpeed(),
                        locationTracker.getBearing(),locationTracker.getBearingAccuracy(),locationTracker.getAltitude(),
                        locationTracker.getSpeedAccuracy());

                long endTime = System.currentTimeMillis();
                Log.e(TAG,"************** total processing time in millis:"+(endTime-startTime));
            }
        }
    }

    private void rotationVectorCalculation(float[] values, long eventTimeStamp){

        SensorManager.getQuaternionFromVector(temporaryQuaternion, values);
        quaternionRotationVector.setXYZW(temporaryQuaternion[1], temporaryQuaternion[2], temporaryQuaternion[3], -temporaryQuaternion[0]);
        if (!positionInitialised) {
            quaternionGyroscope.set(quaternionRotationVector);
            positionInitialised = true;
        }
    }

    private void gyroscopeCalculation(float[] values, long eventTimeStamp){

        if (timestamp != 0) {

            mIsGyroDataPresent = true;
            mGyroFilterData = lpfAccelerationSmoothing.filter(values);

            final float dT = (eventTimeStamp - timestamp) * NS2S;
            float axisX = gyroXAxis = values[0];
            float axisY = gyroYAxis = values[1];
            float axisZ = gyroZAxis = values[2];

            gyroscopeRotationVelocity = Math.sqrt(axisX * axisX + axisY * axisY + axisZ * axisZ);

            if (gyroscopeRotationVelocity > EPSILON) {
                axisX /= gyroscopeRotationVelocity;
                axisY /= gyroscopeRotationVelocity;
                axisZ /= gyroscopeRotationVelocity;
            }
            double thetaOverTwo = gyroscopeRotationVelocity * dT / 2.0f;
            double sinThetaOverTwo = Math.sin(thetaOverTwo);
            double cosThetaOverTwo = Math.cos(thetaOverTwo);
            deltaQuaternion.setX((float) (sinThetaOverTwo * axisX));
            deltaQuaternion.setY((float) (sinThetaOverTwo * axisY));
            deltaQuaternion.setZ((float) (sinThetaOverTwo * axisZ));
            deltaQuaternion.setW(-(float) cosThetaOverTwo);

            deltaQuaternionXAxis = (float) (sinThetaOverTwo * axisX);
            deltaQuaternionYAxis = (float) (sinThetaOverTwo * axisY);
            deltaQuaternionZAxis = (float) (sinThetaOverTwo * axisZ);

            deltaQuaternion.multiplyByQuat(quaternionGyroscope, quaternionGyroscope);
            float dotProd = quaternionGyroscope.dotProduct(quaternionRotationVector);

            if (Math.abs(dotProd) < OUTLIER_THRESHOLD) {
                if (Math.abs(dotProd) < OUTLIER_PANIC_THRESHOLD) {
                    panicCounter++;
                }
                setOrientationQuaternionAndMatrix(quaternionGyroscope);

            } else {
                quaternionGyroscope.slerp(quaternionRotationVector, interpolatedQuaternion, DIRECT_INTERPOLATION_WEIGHT);
                setOrientationQuaternionAndMatrix(interpolatedQuaternion);
                quaternionGyroscope.copyVec4(interpolatedQuaternion);
                panicCounter = 0;
            }

            if (panicCounter > PANIC_THRESHOLD) {

                if (gyroscopeRotationVelocity < 3) {
                    setOrientationQuaternionAndMatrix(quaternionRotationVector);
                    quaternionGyroscope.copyVec4(quaternionRotationVector);
                    panicCounter = 0;
                }
            }
        }
        timestamp = eventTimeStamp;
    }


    private void accelerationDataCalculation(float[] values, long eventTimeStamp){

        mIsAccDataPresent = true;
        mGpsSpeed = locationTracker.getSpeedInKm();
        AccelerometerValue = values;
        System.arraycopy(values, 0, acceleration, 0, values.length);
        //orientationFusionComplimentary.setAcceleration(acceleration);
        acceleration[0] = acceleration[0] / SensorManager.GRAVITY_EARTH;
        acceleration[1] = acceleration[1] / SensorManager.GRAVITY_EARTH;
        acceleration[2] = acceleration[2] / SensorManager.GRAVITY_EARTH;

        acceleration = meanFilterAcceleration.filterFloat(acceleration);
        if (linearAccelerationActive) {

            lpfAcceleration = lowPassFilter.addSamples(acceleration);
            magnitude = meanFilterMagnitude.filterFloat((float) Math
                    .sqrt(Math.pow(lpfAcceleration[0], 2)
                            + Math.pow(lpfAcceleration[1], 2)
                            + Math.pow(lpfAcceleration[2], 2)));
        } else {
            magnitude = acceleration[2];
        }

        if (!filterReady){
            filterCount++;
        }
        if (filterCount > 50){
            filterReady = true;
        }

        if (magnitude > thresholdMax && filterReady){

            thresholdCountMax++;
            if (thresholdCountMax > thresholdCountMaxLimit){
                accelerationEventActive = true;
                thresholdCountMin = 0;
            }

        }else if (magnitude < thresholdMin && accelerationEventActive){

            thresholdCountMin++;
            if (thresholdCountMin > thresholdCountMinLimit){
                accelerationEventActive = false;
                thresholdCountMin = 0;
                thresholdCountMax = 0;

                if(mGpsSpeed > 10) {
                    mEventStatus = Constants.HARSH_BRAKE;
                    mHarshBrakeCounter++;
                    SharedPreferenceManager.write(SharedPrefConstants.HARSH_BRAKE, mHarshBrakeCounter);
                    playSound(HARSH_BREAK);
                }
            }
        }
    }

    private void linearAccelerationDataCalculation(float[] values, long eventTimeStamp){

        linearAcceleroValue = values;
        if(First){

            prevValues = values;
            prevTime = eventTimeStamp / 1000000000;
            First = false;
            currentVelocity[0] = currentVelocity[1] = currentVelocity[2] = 0;
            distanceX = distanceY= distanceZ = 0;

        }else{

            currentTime = eventTimeStamp / 1000000000.0f;
            changeTime = currentTime - prevTime;
            prevTime = currentTime;
            calculateDistance(values, changeTime);
            deltaTime = changeTime;
            currentAcceleration =  (float) Math.sqrt(values[0] * values[0] + values[1] * values[1] + values[2] * values[2]);
            CurrentSpeed = (float) Math.sqrt(speed[0] * speed[0] + speed[1] * speed[1] + speed[2] * speed[2]);
            CurrentDistanceTravelled = (float) Math.sqrt(distanceX *  distanceX + distanceY * distanceY +  distanceZ * distanceZ);
            CurrentDistanceTravelled = CurrentDistanceTravelled / 1000;

            if(FirstSensor){
                prevAcceleration = currentAcceleration;
                prevDistance = CurrentDistanceTravelled;
                prevSpeed = CurrentSpeed;
                FirstSensor = false;
            }
            prevValues = values;
        }
    }

    private void linearAndMagnetoCalculation(){

        if(currentAcceleration != prevAcceleration || CurrentSpeed != prevSpeed || prevDistance != CurrentDistanceTravelled){

            if(!FirstSensor)
                totalDistance = totalDistance + CurrentDistanceTravelled * 1000;
            totalDis = totalDistance;

            if (AccelerometerValue != null && MagnetometerValue != null && currentAcceleration != null) {

                float RT[] = new float[9];
                float I[] = new float[9];
                boolean success = SensorManager.getRotationMatrix(RT, I, AccelerometerValue, MagnetometerValue);

                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(RT, orientation);
                    float azimuth = (float) Math.round(Math.toDegrees(orientation[0]));
                    currentDirection =(azimuth+ 360) % 360;
                }
                prevAcceleration = currentAcceleration;
                prevSpeed = CurrentSpeed;
                prevDistance = CurrentDistanceTravelled;
            }
        }
    }

    private void processAcceleration(float[] acceleration, float gyroXAxis, float gyroYAxis, float gyroZAxis,
                                     double gyroscopeRotationVelocity, float deltaQuaternionXAxis,
                                     float deltaQuaternionYAxis, float deltaQuaternionZAxis, float magnitude,
                                     double totalDistance, double deltaTime, double gpsSpeed,double eventStatus,
                                     float[] linearAccelerationValue, float[] gyroFilteredValue, float currentSpeed,
                                     float currentAcceleration,float currentDistanceTravelled,float currentDirection,
                                     double latitude, double longitude, double speed, float bearing, float bearingAccuracy,
                                     double altitude, float speedAccuracy) {

        if(azimuth != 0) {

            azimuth = azimuth + 0.0001;
            yaw = yaw + 0.0001;
            yawInRadian = Math.toRadians(yaw);
            deviation = getDeviation(mOldAzimuthValue, azimuth);
            yawDeviation = (yawOldInRadian - yawInRadian);
            double yawDeviationAngle = (yawOldInAngle - yaw);

            if(mOldAzimuthValue == 0.0){
                deviation = 0;
                yawDeviation = 0;
                yawDeviationAngle = 0;
            }

            this.accelerationListValues[0] = System.currentTimeMillis();
            this.accelerationListValues[1] = acceleration[0];
            this.accelerationListValues[2] = acceleration[1];
            this.accelerationListValues[3] = acceleration[2];
            this.accelerationListValues[4] = gyroXAxis;
            this.accelerationListValues[5] = gyroYAxis;
            this.accelerationListValues[6] = gyroZAxis;
            this.accelerationListValues[7] = Float.valueOf(String.valueOf(gyroscopeRotationVelocity));
            this.accelerationListValues[8] = deltaQuaternionXAxis;
            this.accelerationListValues[9] = deltaQuaternionYAxis;
            this.accelerationListValues[10] = deltaQuaternionZAxis;
            this.accelerationListValues[11] = magnitude;
            this.accelerationListValues[12] = (float) azimuth;
            this.accelerationListValues[13] = (float) roll;
            this.accelerationListValues[14] = (float) pitch;
            this.accelerationListValues[15] = (float) deviation;
            this.accelerationListValues[16] = (float) latitude;
            this.accelerationListValues[17] = (float) longitude;
            this.accelerationListValues[18] = (float) speed;
            this.accelerationListValues[19] = currentSpeed;
            this.accelerationListValues[20] = currentAcceleration;
            this.accelerationListValues[21] = currentDistanceTravelled;
            this.accelerationListValues[22] = currentDirection;
            this.accelerationListValues[23] = linearAccelerationValue[0];
            this.accelerationListValues[24] = linearAccelerationValue[1];
            this.accelerationListValues[25] = linearAccelerationValue[2];
            this.accelerationListValues[26] = (float) totalDistance;
            this.accelerationListValues[27] = (float) deltaTime;
            this.accelerationListValues[28] = (float) yawInRadian;
            this.accelerationListValues[29] = (float) yawDeviation;
            this.accelerationListValues[30] =  gyroFilteredValue[0];
            this.accelerationListValues[31] =  gyroFilteredValue[1];
            this.accelerationListValues[32] =  gyroFilteredValue[2];
            this.accelerationListValues[33] =  (float) eventStatus;
            this.accelerationListValues[34] =  bearing;
            this.accelerationListValues[35] =  bearingAccuracy;
            this.accelerationListValues[36] =  (float) altitude;
            this.accelerationListValues[37] =  speedAccuracy;
            this.accelerationListValues[38] =  (float) yaw;
            this.accelerationListValues[39] =  (float) yawDeviationAngle;

            mOldAzimuthValue = this.accelerationListValues[12];
            yawOldInRadian = this.accelerationListValues[28];
            yawOldInAngle = this.accelerationListValues[38];

            mEventStatus = 0.0;

            if(!mIsLogTimeCalled){
                mIsLogTimeCalled = true;
                mLogTime = System.currentTimeMillis();
            }
            float timestamp = (System.currentTimeMillis() - mLogTime) / 1000.0f;

           /* mDeviationList.add(new EnergyDataModel(this.accelerationListValues[15], timestamp,
                    this.accelerationListValues[1],this.accelerationListValues[2],this.accelerationListValues[3],
                    this.accelerationListValues[4],this.accelerationListValues[5],this.accelerationListValues[6],
                    this.accelerationListValues[12],this.accelerationListValues[13],
                    this.accelerationListValues[14],(float)gpsSpeed));*/

            ArrayList<EnergyDataModel> localData = new ArrayList<>();
            if(mDeviationList.size()>=360) {
                mDeviationTempList.clear();
                mDeviationTempList.addAll(mDeviationList);
                for(int i=309; i<mDeviationList.size(); i++){
                    EnergyDataModel energyDataModel = mDeviationList.get(i);
                    localData.add(energyDataModel);
                }
                startThreadPool(mDeviationTempList, mDeviationTempList.size());
                mDeviationList.clear();
                mDeviationList.addAll(localData);
            }

            if(true/*MainActivity.IS_BUTTON_CLICKED*/){

                mButtonDataList.add(new ButtonDataModel(timestamp,accelerationListValues[1],accelerationListValues[2],
                        accelerationListValues[3],accelerationListValues[4],accelerationListValues[5],
                        accelerationListValues[6],accelerationListValues[7],accelerationListValues[8],
                        accelerationListValues[9],accelerationListValues[10],accelerationListValues[11],
                        accelerationListValues[12],accelerationListValues[13],accelerationListValues[14],
                        accelerationListValues[15],accelerationListValues[16],accelerationListValues[17],
                        accelerationListValues[18],accelerationListValues[19],accelerationListValues[20],
                        accelerationListValues[21],accelerationListValues[22],accelerationListValues[23],
                        accelerationListValues[24],accelerationListValues[25],accelerationListValues[26],
                        accelerationListValues[27],accelerationListValues[28],accelerationListValues[29],
                        accelerationListValues[30],accelerationListValues[31],accelerationListValues[32]));
            }else{

                if(mButtonDataList.size() > 0){
                    mButtonDataTempList.addAll(mButtonDataList);
                    mButtonDataList.clear();
                    mFileWriteTimeStamp = System.currentTimeMillis();
                    mIsOneButtonData = false;
                    /*new ButtonDataAysnc().execute(MainActivity.BUTTON_CLICKED_NAME);*/
                }
            }
            //setValue(accelerationListValues);
        }
    }


    private class SensorThread implements Runnable{

        @Override
        public void run() {
            processSensorCalculation();
        }
    }



    private void startSensorCalThreadPool(int length){

        Runnable worker = new SensorWorkerThread(length);
        sensorExecutor.execute(worker);
    }

    class SensorWorkerThread implements Runnable {

        private int mLength;

        public SensorWorkerThread(int length){
            this.mLength = length;
        }

        public void run() {

            Log.e(TAG,Thread.currentThread().getName()+"(Start)");
            Log.e(TAG,Thread.currentThread().getName()+"(ended)");
        }
    }



    /*private void calculateSpeedDeviation(){

        float d1 = speedList.get(0).getSpeed() - speedList.get(1).getSpeed();
        float d2 = speedList.get(1).getSpeed() - speedList.get(2).getSpeed();
        float d3 = speedList.get(2).getSpeed() - speedList.get(3).getSpeed();
        float d4 = speedList.get(3).getSpeed() - speedList.get(4).getSpeed();

        long d1Diff = speedList.get(0).getTimeStamp() - speedList.get(1).getTimeStamp();
        long d2Diff = speedList.get(1).getTimeStamp() - speedList.get(2).getTimeStamp();
        long d3Diff = speedList.get(2).getTimeStamp() - speedList.get(3).getTimeStamp();
        long d4Diff = speedList.get(3).getTimeStamp() - speedList.get(4).getTimeStamp();

        float dev1 = d1 - d2;
        float dev2 = d2 - d3;
        float dev3 = d3 - d4;

        if(dev1 >10 && d1Diff <= 1000){
            // TODO confirm deviation check n proceed
        }

    }*/

    private void setOrientationQuaternionAndMatrix(Quaternion quaternion) {
        correctedQuaternion.set(quaternion);
        correctedQuaternion.w(-correctedQuaternion.w());

        synchronized (synchronizationToken) {
            currentOrientationQuaternion.copyVec4(quaternion);
            SensorManager.getRotationMatrixFromVector(currentOrientationRotationMatrix.matrix, correctedQuaternion.array());
        }
    }

    private void startOrientation(){

        orientationSensor.init(1.0, 1.0, 1.0);
        orientationSensor.on(0);
        orientationSensor.isSupport();
    }

    public void stopOrientation(){
        orientationSensor.off();
    }

    @Override
    public void orientation(Double AZIMUTH, Double PITCH, Double ROLL, Double YAW) {

        double pitchDeviation  =  pitch - PITCH;
        double rollDeviation  =   roll - ROLL;

        if((pitchDeviation < -5  ||  pitchDeviation > 5) && (rollDeviation < -5  ||  rollDeviation > 5)) {
            mIsPitchMovementInRange = true;
        }else{
            mIsPitchMovementInRange = false;
        }

        //deviation = getDeviation(azimuth, AZIMUTH);
        //azimuth = Math.toRadians(AZIMUTH);
        azimuth = AZIMUTH;
        roll = ROLL;
        pitch = PITCH;
        yaw = YAW;

        if(lastActivityLogTime == 0) {
            lastActivityLogTime = System.currentTimeMillis();
        }
        if(!mTimeSet){
            mTimeSet = true;
            mTimeStamp = System.currentTimeMillis();
        }
        storeSamples(AZIMUTH,PITCH,ROLL,(System.currentTimeMillis()- mTimeStamp));
    }

    @Override
    public void orientationYawData(String Yaw, String Roll, String Pitch) {

    }


    private double getDeviation(double a, double b){

        double d = Math.abs(a - b) % 360;
        double r = d > 180 ? 360 - d : d;
        int sign = (a - b >= 0 && a - b <= 180) || (a - b <=-180 && a- b>= -360) ? 1 : -1;
        r *= sign;
        return r;
    }


    private void storeSamples(Double AZIMUTH, Double PITCH, Double ROLL, long timeStamp){

        if(System.currentTimeMillis() - lastActivityLogTime < 10000){

            AngularAxis axis = new AngularAxis(AZIMUTH,PITCH,ROLL,timeStamp);
            rotationSamples.add(axis);

        }else{
            tmpSampleList.clear();
            tmpSampleList.addAll(rotationSamples);
            rotationSamples.clear();
            AngularAxis axis = new AngularAxis(AZIMUTH,PITCH,ROLL,timeStamp);
            rotationSamples.add(axis);
            lastActivityLogTime= System.currentTimeMillis();
            processStoredDataSamples();
        }
    }

    private void processStoredDataSamples(){

        Iterator<AngularAxis> iter = tmpSampleList.iterator();
        List<AngularAxis> deviatedAxis = new ArrayList<>();
        boolean deviationStarted = false;
        AngularAxis lastAxis = null;
        Double deviation = null;
        while (iter.hasNext()){
            AngularAxis axis = iter.next();
            if(lastAxis!=null ){
                deviation= calculateDeviation(lastAxis.getAngle(),axis.getAngle());

                deviationStarted = (deviation <= -1.3 || deviation >= 1.3) && !deviationStarted;

                if(deviationStarted){
                    deviatedAxis.add(axis);
                }
            }
            lastAxis = axis;
        }
        processDeviatedPoints(deviatedAxis);
    }


    private void processDeviatedPoints(List<AngularAxis> deviatedAxis){

        for(int i=0; i<deviatedAxis.size(); i++) {

            String angle = String.valueOf(deviatedAxis.get(i).getAngle());
            String roll = String.valueOf(deviatedAxis.get(i).getRoll());
            String pitch = String.valueOf(deviatedAxis.get(i).getPitch());
            float time = (deviatedAxis.get(i).getTimestamp() / 1000.0f);

            saveData(angle, roll, pitch, String.valueOf(time));
        }
    }

    private Double calculateDeviation(Double a, Double b){
        Double d = Math.abs(a - b) % 360.0;
        Double r = d > 180 ? 360 - d : d;
        int sign = (a - b >= 0 && a - b <= 180) || (a - b <= -180 && a - b >= -360) ? 1 : -1;
        r *= sign;
        return  r;
    }

    private void saveData(String azimuth, String roll, String pitch, String time){

        String header = null;
        String entry;
        if(!mIsOneTime){
            mIsOneTime = true;
            ts = new SimpleDateFormat("yyyy-MM-dd-HH:mm", Locale.ENGLISH).format(new Date());
            header = "TimeStamp,Azimuth,Roll,pitch\n";
        }
        if(header != null) {
            entry = header+ time + "," + azimuth + "," + roll + "," + pitch + "\n";
        }else{
            entry = time +"," + azimuth + "," + roll + "," + pitch + "\n";
        }
        try {
            File dir = new File(Util.createAndGetFolderPath());
            if(!dir.exists()) {
                dir.mkdir();
            }
            File file = new File(dir, "deviationData.csv");
            FileOutputStream f = new FileOutputStream(file, true);
            try {
                f.write(entry.getBytes());
                f.flush();
                f.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    private void startThreadPool(ArrayList<EnergyDataModel> data, int length){

        Runnable worker = new WorkerThread(data, length);
        executor.execute(worker);
    }

    class WorkerThread implements Runnable {

        private ArrayList<EnergyDataModel> mData;
        private int mLength;

        public WorkerThread(ArrayList<EnergyDataModel> data, int length){

            this.mData = data;
            this.mLength = length;
        }

        public void run() {

            Log.e(TAG,Thread.currentThread().getName()+"(Start)");
            calculateEnergy(mData,mLength);
            Log.e(TAG,Thread.currentThread().getName()+"(ended)");
        }
    }

    private void calculateEnergy(ArrayList<EnergyDataModel> s, int N){

        double sum = 0, a1 = 0;
        String status = "";
        float SPEED_LIMIT = 0;

        for (int n = 0; n < N-1; n++) {

            float speed = s.get(n).getGpsSpeed();
            if(speed > 5){
                SPEED_LIMIT = speed;
            }

            for (int k = 0; k < 50; k++) {

                if (n - k >= 0) {
                    a1 = s.get(n - k).getDeviation();
                    sum = sum + (a1 * a1);
                } else {
                    sum = sum + 0;
                }
            }

            if(mLastManevurStarted && n<51){
                continue;
            }

            if(sum > MIN_THRESHOLD && !mManeuverStarted){
                mManeuverStarted = true;
                mMaxValue = sum;
                mMinValue = sum;
                mManeuverStartTime = System.currentTimeMillis();
            }

            if(mManeuverStarted && sum>=mMaxValue ){
                mMaxValue = sum;
                if(mMaxValue > MAX_THRESHOLD && !mIsThresholdAchieved){
                    mIsThresholdAchieved = true;
                }
            }

            if(mIsThresholdAchieved){

                if(sum<mMaxValue){

                    /*if((mMaxValue - sum) >= 30){
                        mWentDown = true;
                    }

                    if(mWentDown && (sum- mMinValue) >10){
                        mCounter++;
                    }

                    if(mCounter > 2 && (System.currentTimeMillis()- mManeuverStartTime) <= 3000){
                        Log.e(TAG,"POSSIBILITY FREQUENT LANE CHANGE DETECTED!!!!!!!!!!");
                        status = "FREQUENT_LANE_CHANGE";
                        playSound(FREQUENT_LANE_CHANGE);
                        resetFlags();
                        //continue;
                    }*/

                    if(sum>0 && (sum-MIN_THRESHOLD)<=15){

                        mManeuverEndTime = System.currentTimeMillis();
                        long eventDuration = mManeuverEndTime - mManeuverStartTime;
                        if(eventDuration>3000 && eventDuration<5000 && mMaxValue > MAX_THRESHOLD &&  mMaxValue <400 && SPEED_LIMIT >5){
                            Log.e(TAG,"POSSIBILITY NORMAL LANE CHANGE DETECTED!!!!!!!!!!");
                            status = "LANE_CHANGE";
                            mLaneChangeCounter++;
                            SharedPreferenceManager.write(SharedPrefConstants.NORMAL_LANE_CHANGE,mLaneChangeCounter);
                            playSound(LANE_CHANGE);
                            resetFlags();
                        }else if(eventDuration<3000 && mMaxValue > MAX_THRESHOLD && SPEED_LIMIT >5){
                            Log.e(TAG,"POSSIBILITY HARSH LANE CHANGE DETECTED!!!!!!!!!");
                            status = "HL_CHANGE";
                            mHarshLaneChangeCounter++;
                            SharedPreferenceManager.write(SharedPrefConstants.HARSH_LANE_CHANGE,mHarshLaneChangeCounter);
                            playSound(HARSH_LANE_CHANGE);
                            resetFlags();
                        }else if(eventDuration>4000 && eventDuration<8000 && mMaxValue>350 && SPEED_LIMIT >5){
                            Log.e(TAG,"POSSIBILITY OF LEFT OR RIGHT TURN DETECTED!!!!!!!");
                            status = "LR_TURN";
                            mLRTurnCounter++;
                            SharedPreferenceManager.write(SharedPrefConstants.LR_TURN_CHANGE,mLRTurnCounter);
                            playSound(LR_TURN);
                            resetFlags();
                        }else if(eventDuration>7000 && mMaxValue>1000 && SPEED_LIMIT >5){
                            Log.e(TAG,"POSSIBILITY OF U-TURN DETECTED!!!!!!");
                            status = "U_TURN";
                            mUTurnCounter++;
                            SharedPreferenceManager.write(SharedPrefConstants.U_TURN_CHANGE,mUTurnCounter);
                            playSound(U_TURN);
                            resetFlags();
                        }
                    }else if(sum>mMinValue && mMaxValue-mMinValue>=5 ){
                        // mMinValue = sum;
                        Log.e(TAG,"POSSIBILITY OF MANEUVER CONTINUATION!!!!!!");
                    }else{
                        mMinValue = sum;
                    }
                }else{
                    mMaxValue = sum;
                    Log.e(TAG,"Maneuver is ");
                }
            }else{
                if(mManeuverStarted && sum < MIN_THRESHOLD){
                    resetFlags();
                }
            }

            saveEnergyData(s.get(n).getTimestamp(),s.get(n).getAccelerometerXAxis(),s.get(n).getAccelerometerYAxis(),
                    s.get(n).getAccelerometerZAxis(),s.get(n).getGyroXAxis(),s.get(n).getGyroYAxis(),
                    s.get(n).getGyroZAxis(),s.get(n).getAzimuth(),s.get(n).getRoll(),s.get(n).getPitch(),
                    sum,status);
            sum = 0;
            status = "";

            if(n == N-2 && (mManeuverStarted || mIsThresholdAchieved)){
                mLastManevurStarted = true;
            }else {
                mLastManevurStarted = false;
            }
            SPEED_LIMIT = 0;
        }
    }

    private void resetFlags(){

        mManeuverStarted = false;
        mIsThresholdAchieved = false;
        mMaxValue = 0;
        mMinValue = 0;
        mManeuverStartTime = 0;
        mManeuverEndTime = 0;
        mCounter = 0;
        mWentDown = false;
    }

    private void saveEnergyData(float time, float accXValue, float accYValue, float accZValue,
                                float gyroXValue, float gyroYValue, float gyroZValue,
                                float azimuth, float roll, float pitch, double energy, String status){

        String header = null;
        String entry;
        if(!mIsOneTimeEnergyData){
            mIsOneTimeEnergyData = true;
            header = "TimeStamp,AccXValue,AccYValue,AccZVale,GyroXValue,GyroYValue,GyroZValue," +
                    "Azimuth,Roll,pitch,energy,status\n";
        }
        if(header != null) {
            entry = header +
                    time + "," +
                    accXValue + "," +
                    accYValue + "," +
                    accZValue + "," +
                    gyroXValue + "," +
                    gyroYValue + "," +
                    gyroZValue + "," +
                    azimuth + "," +
                    roll + "," +
                    pitch + "," +
                    energy + "," +
                    status + "\n";
        }else{
            entry = time + "," +
                    accXValue + "," +
                    accYValue + "," +
                    accZValue + "," +
                    gyroXValue + "," +
                    gyroYValue + "," +
                    gyroZValue + "," +
                    azimuth + "," +
                    roll + "," +
                    pitch + "," +
                    energy + "," +
                    status + "\n";
        }
        try {
            File dir = new File(Util.createAndGetFolderPath());
            if(!dir.exists()) {
                dir.mkdir();
            }
            File file = new File(dir, "energy.csv");
            FileOutputStream f = new FileOutputStream(file, true);
            try {
                f.write(entry.getBytes());
                f.flush();
                f.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    private void playSound(int soundType){
        MediaPlayer mp;
        switch (soundType){

            case LANE_CHANGE:
                mp = MediaPlayer.create(mContext, R.raw.lane);
                mp.start();
                break;
            case HARSH_LANE_CHANGE:
                mp = MediaPlayer.create(mContext, R.raw.harsh_lane);
                mp.start();
                break;
            case LR_TURN:
                mp = MediaPlayer.create(mContext, R.raw.lr_turn);
                mp.start();
                break;
            case U_TURN:
                mp = MediaPlayer.create(mContext, R.raw.u_turn);
                mp.start();
                break;
            case HARSH_BREAK:
                mp = MediaPlayer.create(mContext, R.raw.harsh_break);
                mp.start();
                break;
            case FREQUENT_LANE_CHANGE:
                mp = MediaPlayer.create(mContext, R.raw.freq_lane_change);
                mp.start();
                break;
        }
    }

    private void calculateDistance (float[] acceleration, float deltaTime) {

        float[] distance = new float[acceleration.length];

        for (int i = 0; i < acceleration.length; i++) {
            speed[i] = acceleration[i] * deltaTime;
            distance[i] = speed[i] * deltaTime + acceleration[i] * deltaTime * deltaTime / 2;
        }
        distanceX = distance[0];
        distanceY = distance[1];
        distanceZ = distance[2];
    }

    private void saveButtonClickData(float timeStamp, float AccXValue,float AccYValue,float AccZValue,float GyroXValue,
                                     float GyroYValue,float GyroZValue,float gyroRotationVelocity,
                                     float deltaQuaternionXAxis, float deltaQuaternionYAxis,
                                     float deltaQuaternionZAxis, float magnitude,float azimuth,float roll,
                                     float pitch,float deviation, float latitude,float longitude,float speed,
                                     float currentSpeed,float currentAcceleration,float CurrentDistanceTravelled,
                                     float currentDirection,float linearAccXValue,float linearAccYValue,
                                     float linearAccZValue,float totalDistance, float deltaTime,
                                     float yaw, float yawDeviation, float gyroFilteredXValue,
                                     float gyroFilteredYValue, float gyroFilteredZValue,String fileName){

        String header = null;
        String entry;

        if(!mIsOneButtonData){
            mIsOneButtonData = true;

            header = "TimeStamp,AccXValue,AccYValue,AccZValue,GyroXValue,GyroYValue,GyroZValue," +
                    "gyroRotationVelocity,deltaQuaternionXAxis,deltaQuaternionYAxis,deltaQuaternionZAxis," +
                    "magnitude,azimuth,roll,pitch,deviation,latitude,longitude,speed,currentSpeed,currentAcceleration," +
                    "CurrentDistanceTravelled,currentDirection,linearAccXValue,linearAccYValue,linearAccZValue," +
                    "totalDistance,deltaTime,yaw,yawDeviation,gyroFilteredXValue,gyroFilteredYValue," +
                    "gyroFilteredZValue\n";
        }

        if(header != null) {

            entry = header +
                    timeStamp + "," +
                    AccXValue + "," +
                    AccYValue + "," +
                    AccZValue + "," +
                    GyroXValue + "," +
                    GyroYValue + "," +
                    GyroZValue + "," +
                    gyroRotationVelocity + "," +
                    deltaQuaternionXAxis + "," +
                    deltaQuaternionYAxis + "," +
                    deltaQuaternionZAxis + "," +
                    magnitude + "," +
                    azimuth + "," +
                    roll + "," +
                    pitch + "," +
                    deviation + "," +
                    latitude + "," +
                    longitude + "," +
                    speed + "," +
                    currentSpeed + "," +
                    currentAcceleration + "," +
                    CurrentDistanceTravelled + "," +
                    currentDirection + "," +
                    linearAccXValue + "," +
                    linearAccYValue + "," +
                    linearAccZValue + "," +
                    totalDistance + "," +
                    deltaTime + "," +
                    yaw + "," +
                    yawDeviation + "," +
                    gyroFilteredXValue + "," +
                    gyroFilteredYValue + "," +
                    gyroFilteredZValue + "\n";

        }else{

            entry = timeStamp + "," +
                    AccXValue + "," +
                    AccYValue + "," +
                    AccZValue + "," +
                    GyroXValue + "," +
                    GyroYValue + "," +
                    GyroZValue + "," +
                    gyroRotationVelocity + "," +
                    deltaQuaternionXAxis + "," +
                    deltaQuaternionYAxis + "," +
                    deltaQuaternionZAxis + "," +
                    magnitude + "," +
                    azimuth + "," +
                    roll + "," +
                    pitch + "," +
                    deviation + "," +
                    latitude + "," +
                    longitude + "," +
                    speed + "," +
                    currentSpeed + "," +
                    currentAcceleration + "," +
                    CurrentDistanceTravelled + "," +
                    currentDirection + "," +
                    linearAccXValue + "," +
                    linearAccYValue + "," +
                    linearAccZValue + "," +
                    totalDistance + "," +
                    deltaTime + "," +
                    yaw + "," +
                    yawDeviation + "," +
                    gyroFilteredXValue + "," +
                    gyroFilteredYValue + "," +
                    gyroFilteredZValue + "\n";
        }
        try {
            File dir = new File(Util.createAndGetFolderPath());
            if(!dir.exists()) {
                dir.mkdir();
            }
            File file = new File(dir, fileName+"_"+mFileWriteTimeStamp+".csv");
            FileOutputStream f = new FileOutputStream(file, true);
            try {
                f.write(entry.getBytes());
                f.flush();
                f.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }

    private class ButtonDataAysnc extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... strings) {

            ArrayList<ButtonDataModel> list = new ArrayList<>();
            list.addAll(mButtonDataTempList);
            mButtonDataTempList.clear();

            for (int i=0; i<list.size(); i++){

                float timeStamp = list.get(i).getTimeStamp();
                float AccXValue = list.get(i).getAccXValue();
                float AccYValue = list.get(i).getAccYValue();
                float AccZValue = list.get(i).getAccZValue();
                float GyroXValue = list.get(i).getGyroXValue();
                float GyroYValue = list.get(i).getGyroYValue();
                float GyroZValue = list.get(i).getGyroZValue();
                float gyroRotationVelocity = list.get(i).getGyroRotationVelocity();
                float deltaQuaternionXAxis = list.get(i).getDeltaQuaternionXAxis();
                float deltaQuaternionYAxis = list.get(i).getDeltaQuaternionYAxis();
                float deltaQuaternionZAxis = list.get(i).getDeltaQuaternionZAxis();
                float magnitude = list.get(i).getMagnitude();
                float azimuth = list.get(i).getAzimuth();
                float roll = list.get(i).getRoll();
                float pitch = list.get(i).getPitch();
                float deviation = list.get(i).getDeviation();
                float latitude = list.get(i).getLatitude();
                float longitude = list.get(i).getLongitude();
                float speed = list.get(i).getSpeed();
                float currentSpeed = list.get(i).getCurrentSpeed();
                float currentAcceleration = list.get(i).getCurrentAcceleration();
                float CurrentDistanceTravelled = list.get(i).getCurrentDistanceTravelled();
                float currentDirection = list.get(i).getCurrentDirection();
                float linearAccXValue = list.get(i).getLinearAccXValue();
                float linearAccYValue = list.get(i).getLinearAccYValue();
                float linearAccZValue = list.get(i).getLinearAccZValue();
                float totalDistance = list.get(i).getTotalDistance();
                float deltaTime = list.get(i).getDeltaTime();
                float yaw = list.get(i).getYawRadians();
                float yawDeviation = list.get(i).getYawDeviation();
                float gyroFilteredXValue = list.get(i).getGyroFilteredXValue();
                float gyroFilteredYValue = list.get(i).getGyroFilteredYValue();
                float gyroFilteredZValue = list.get(i).getGyroFilteredZValue();

                saveButtonClickData(timeStamp,AccXValue,AccYValue,AccZValue,GyroXValue,GyroYValue,GyroZValue,
                        gyroRotationVelocity,deltaQuaternionXAxis,deltaQuaternionYAxis,deltaQuaternionZAxis,
                        magnitude,azimuth,roll,pitch,deviation,latitude,longitude,speed,currentSpeed,
                        currentAcceleration,CurrentDistanceTravelled,currentDirection,linearAccXValue,
                        linearAccYValue,linearAccZValue,totalDistance,deltaTime,yaw,yawDeviation,
                        gyroFilteredXValue,gyroFilteredYValue,gyroFilteredZValue,strings[0]);
            }

            return null;
        }
    }
}
