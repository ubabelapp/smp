package com.accelerationraw.android.eventProcess;

import com.accelerationraw.android.model.EventData;
import com.accelerationraw.android.processFactory.ProcessFactory;

/**
 * @author Vikash Sharma
 *
 */
public interface EventValidationProcess {
	
	public ProcessFactory getProcessType(ProcessFactory processFactory);
	public EventStats processData(EventData eventData);
	
}
