package com.accelerationraw.android.processFactory;

/**
 * @author rainbow
 *
 */
public enum ProcessFactory {

	SIMPLE_ACCELERATION_PROCESS(1), GPS_DATA_PROCESS(2), V_SENSE_PROCESS(3), FAST_DTW_PROCESS(4),NO_PROCESS(5),PROCESS_FINISHED(6);
	private int value;

	ProcessFactory(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
};
