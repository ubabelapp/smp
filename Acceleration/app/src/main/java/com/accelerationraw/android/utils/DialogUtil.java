package com.accelerationraw.android.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 *
 * Created by Sourabh on 21-03-2018.
 */

public class DialogUtil {

    public static ProgressDialog showLoadingDialog(Context context, String msg) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }
}
