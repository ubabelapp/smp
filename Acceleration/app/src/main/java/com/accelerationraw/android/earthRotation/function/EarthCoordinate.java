package com.accelerationraw.android.earthRotation.function;


public interface EarthCoordinate {
    boolean getRotationMatrix(float[] R, float[] I, float[] gravity, float[] geomagnetic);
}
