package com.accelerationraw.android.constant;

/*
 * Created by Sourabh on 02-02-2018.
 */

public class SharedPrefConstants {

    public static final String SPEED_THRESHOLD = "speedThreshold";
    public static final String HARSH_BRAKE = "harshBrake";
    public static final String HARSH_LANE_CHANGE = "harshLaneChange";
    public static final String NORMAL_LANE_CHANGE = "normalLaneChange";
    public static final String LR_TURN_CHANGE = "lrTurnChange";
    public static final String U_TURN_CHANGE = "uTurnChange";
    public static final String OVER_SPEEDING = "overSpeeding";
    public static final String STATIONARY_TIME_START = "stationaryTimeStart";
    public static final String VEHICLE_STATE = "vehicleState";
    public static final String STATIONARY_LATITUDE="stationaryLatitude";
    public static final String STATIONARY_LONGITUDE="stationaryLongitude";
    public static final String FOLDER_NAME = "folderName";
}
