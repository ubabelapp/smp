package com.accelerationraw.android.vSense;

import com.accelerationraw.android.constant.Constants;
import com.accelerationraw.android.model.AccelerometerData;
import com.accelerationraw.android.model.EventData;
import com.accelerationraw.android.model.GPSData;
import com.accelerationraw.android.model.GyroscopeData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * Created by vikash on 15-03-2018.
 */

public class VSenseDisplacement {

    public static double calculateDisplacement(ArrayList<EventData> eventDataList, int samplingFrequency){

        double mSamplingTimeStamp = (double)samplingFrequency / 1000.0;
        double totalDisplacement = 0;

        List<GyroscopeData> gyroList = new ArrayList<>();
        List<AccelerometerData> accList = new ArrayList<>();
        //List<GPSData> gpsDataList = new ArrayList<>();

        for (EventData eventData : eventDataList) {
            gyroList.add(eventData.getGyroscopeData());
            accList.add(eventData.getAccelerometerData());
            //gpsDataList.add(eventData.getGpsData());
        }

        double startVelocity = 0.0;
        double velocityDeviator = 0.0;
        int N = eventDataList.size();   //total number of seconds during the event
        int eventToFindDeviation = 0;

        for (int n=0; n<N; n++){

            if(n == eventToFindDeviation && n+samplingFrequency<eventDataList.size()){
                eventToFindDeviation = n+samplingFrequency;
                velocityDeviator = -1.0 *((eventDataList.get(n).getGyroscopeData().getSpeed() -
                        eventDataList.get(eventToFindDeviation-1).getGyroscopeData().getSpeed())/samplingFrequency);
            }
            double velocity = gyroList.get(n).getSpeed();
            double yawHeading = 0.0;
            for(int k =0; k<=n; k++){
                yawHeading = yawHeading + gyroList.get(k).getZ()* mSamplingTimeStamp;
            }
            totalDisplacement += (velocity + velocityDeviator)* mSamplingTimeStamp * Math.sin(yawHeading);
        }
        return totalDisplacement;
    }


    public static boolean isDisplacedForLaneChange(double displacement){
        return displacement >= Constants.LAN_CHANGE_DISPLACEMENT_THRESHOLD;
    }

}
