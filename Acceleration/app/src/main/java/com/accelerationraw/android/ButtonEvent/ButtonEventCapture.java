package com.accelerationraw.android.ButtonEvent;

import android.content.Context;

import com.accelerationraw.android.constant.Constants;
import com.accelerationraw.android.model.EnergyDataModel;

import java.util.ArrayList;

/**
 *
 * Created by Sourabh on 29-03-2018.
 */

public class ButtonEventCapture {

    private Context mContext;
    private Thread dataProcessingThread;

    private boolean potHoleEnabled, UTurnEnabled, LTurnEnabled, RTurnEnabled, zigZagEnabled,
            roundAboutEnabled,LRoadCurveEnabled,RRoadCurveEnabled,rashDrivingEnabled,harshBrakeEnabled,
            LLaneChangeEnabled,RLaneChangeEnabled,roadHumpsEnabled,unevenRoadEnabled, contiuonusCurveEnabled,
            trafficLightEnabled,upwardSlopeEnabled,downSlopeEnabled,HLaneChangeEnabled,underPassEnabled,
            flyOverEnabled = false;

    private  ArrayList<EnergyDataModel> potHoleList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> UturnList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> LturnList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> RturnList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> zigZagList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> roundAboutList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> LRoadCurveList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> RRoadCurveList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> rashDrivingList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> harshBrakeList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> LLaneChangeList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> RLaneChangeList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> roadHumpList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> unevenRoadList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> contiunousCurveList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> trafficLightList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> upwardSlopeList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> downSlopeList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> HLaneChangeList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> underPassList = new ArrayList<>();
    private  ArrayList<EnergyDataModel> flyOverList = new ArrayList<>();

    private PotHoleDataLogger mPotHoleDataLogger;
    private UTurnDataLogger mUTurnDataLogger;
    private LTurnDataLogger mLTurnDataLogger;
    private RTurnDatalogger mRTurnDatalogger;
    private ZigZagDataLogger mZigZagDataLogger;
    private RoundAboutDataLogger mRoundAboutDataLogger;
    private LRoadCurveDataLogger mLRoadCurveDataLogger;
    private RRoadCurveDataLogger mRRoadCurveDataLogger;
    private RashDrivingDataLogger mRashDrivingDataLogger;
    private HarshBrakingDataLogger mHarshBrakingDataLogger;
    private LLaneChangeDataLogger mLLaneChangeDataLogger;
    private RLaneChangeDataLogger mRLaneChangeDataLogger;
    private RoadHumpsDataLogger mRoadHumpsDataLogger;
    private UnEvenRoadDataLogger mUnEvenRoadDataLogger;
    private ContinousCurveDatalogger mContinousCurveDatalogger;
    private TrafficLightDataLogger mTrafficLightDataLogger;
    private UpwardSlopeDataLogger mUpwardSlopeDataLogger;
    private DownSlopeDataLogger mDownSlopeDataLogger;
    private HLaneChangeDataLogger mhHLaneChangeDataLogger;
    private UnderPassDataLogger mUnderPassDataLogger;
    private FlyOverDataLogger mFlyOverDataLogger;
    private boolean mIsThreadStopped = false;


    public ButtonEventCapture(Context context){
        this.mContext = context;
    }


    public void addDataToList(int type, float deviation, float timestamp, float[] accelerationValues,
                              float[] gyroscopeValues, float azimuth, float mRoll, float mPitch,
                              float speed, double latitude, double longitude, float bearing){

        switch (type){

            case Constants.POT_HOLE:
                potHoleEnabled = true;
                break;
            case Constants.U_TURN:
                UTurnEnabled = true;
                break;
            case Constants.L_TURN:
                LTurnEnabled = true;
                break;
            case Constants.R_TURN:
                RTurnEnabled = true;
                break;
            case Constants.ZIG_ZAG:
                zigZagEnabled = true;
                break;
            case Constants.ROUND_ABOUT:
                roundAboutEnabled = true;
                break;
            case Constants.L_ROAD_CURVE:
                LRoadCurveEnabled = true;
                break;
            case Constants.R_ROAD_CURVE:
                RRoadCurveEnabled = true;
                break;
            case Constants.RASH_DRIVING:
                rashDrivingEnabled = true;
                break;
            case Constants.HARSH_BRAKING:
                harshBrakeEnabled = true;
                break;
            case Constants.L_LANE_CHANGE:
                LLaneChangeEnabled = true;
                break;
            case Constants.R_LANE_CHANGE:
                RLaneChangeEnabled = true;
                break;
            case Constants.ROAD_HUMPS:
                roadHumpsEnabled = true;
                break;
            case Constants.UNEVEN_ROAD:
                unevenRoadEnabled = true;
                break;
            case Constants.CONTIUNOUS_CURVE:
                contiuonusCurveEnabled = true;
                break;
            case Constants.TRAFFIC_LIGHT:
                trafficLightEnabled = true;
                break;
            case Constants.UPWARD_SLOPE:
                upwardSlopeEnabled = true;
                break;
            case Constants.DOWN_SLOPE:
                downSlopeEnabled = true;
                break;
            case Constants.H_LANE_CHANGE:
                HLaneChangeEnabled = true;
                break;
            case Constants.UNDER_PASS:
                underPassEnabled = true;
                break;
            case Constants.FLY_OVER:
                flyOverEnabled = true;
                break;
            default:
                break;
        }

        if(potHoleEnabled){
            potHoleList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(UTurnEnabled){
            UturnList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(LTurnEnabled){
            LturnList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(RTurnEnabled){
            RturnList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(zigZagEnabled){
            zigZagList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(roundAboutEnabled){
            roundAboutList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(LRoadCurveEnabled){
            LRoadCurveList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(RRoadCurveEnabled){
            RRoadCurveList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(rashDrivingEnabled){
            rashDrivingList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(harshBrakeEnabled){
            harshBrakeList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(LLaneChangeEnabled){
            LLaneChangeList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(RLaneChangeEnabled){
            RLaneChangeList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(roadHumpsEnabled){
            roadHumpList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(unevenRoadEnabled){
            unevenRoadList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(contiuonusCurveEnabled){
            contiunousCurveList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(trafficLightEnabled){
            trafficLightList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(upwardSlopeEnabled){
            upwardSlopeList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(downSlopeEnabled){
            downSlopeList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(HLaneChangeEnabled){
            HLaneChangeList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(underPassEnabled){
            underPassList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
        if(flyOverEnabled){
            flyOverList.add(new EnergyDataModel(deviation,timestamp,accelerationValues[0],
                    accelerationValues[1],accelerationValues[2],gyroscopeValues[0],gyroscopeValues[1],
                    gyroscopeValues[2],azimuth, mRoll, mPitch,
                    speed,latitude,longitude,bearing));
        }
    }


    public void writeToFile(String btnName, int type){

        switch (type){

            case Constants.POT_HOLE:
                potHoleEnabled = false;
                ArrayList<EnergyDataModel> potHole = new ArrayList<>();
                potHole.addAll(potHoleList);
                potHoleList.clear();
                mPotHoleDataLogger = new PotHoleDataLogger(mContext,btnName);
                mPotHoleDataLogger.setHeaders();
                calculateEnergy(potHole,type);
                break;
            case Constants.U_TURN:
                UTurnEnabled = false;
                ArrayList<EnergyDataModel> uTurn = new ArrayList<>();
                uTurn.addAll(UturnList);
                UturnList.clear();
                mUTurnDataLogger = new UTurnDataLogger(mContext,btnName);
                mUTurnDataLogger.setHeaders();
                calculateEnergy(uTurn,type);
                break;
            case Constants.L_TURN:
                LTurnEnabled = false;
                ArrayList<EnergyDataModel> lTurn = new ArrayList<>();
                lTurn.addAll(LturnList);
                LturnList.clear();
                mLTurnDataLogger = new LTurnDataLogger(mContext,btnName);
                mLTurnDataLogger.setHeaders();
                calculateEnergy(lTurn,type);
                break;
            case Constants.R_TURN:
                RTurnEnabled = false;
                ArrayList<EnergyDataModel> rTurn = new ArrayList<>();
                rTurn.addAll(RturnList);
                RturnList.clear();
                mRTurnDatalogger = new RTurnDatalogger(mContext,btnName);
                mRTurnDatalogger.setHeaders();
                calculateEnergy(rTurn,type);
                break;
            case Constants.ZIG_ZAG:
                zigZagEnabled = false;
                ArrayList<EnergyDataModel> zigZag = new ArrayList<>();
                zigZag.addAll(zigZagList);
                zigZagList.clear();
                mZigZagDataLogger = new ZigZagDataLogger(mContext,btnName);
                mZigZagDataLogger.setHeaders();
                calculateEnergy(zigZag,type);
                break;
            case Constants.ROUND_ABOUT:
                roundAboutEnabled = false;
                ArrayList<EnergyDataModel> roundAbout = new ArrayList<>();
                roundAbout.addAll(roundAboutList);
                roundAboutList.clear();
                mRoundAboutDataLogger = new RoundAboutDataLogger(mContext,btnName);
                mRoundAboutDataLogger.setHeaders();
                calculateEnergy(roundAbout,type);
                break;
            case Constants.L_ROAD_CURVE:
                LRoadCurveEnabled = false;
                ArrayList<EnergyDataModel> lRoadCurve = new ArrayList<>();
                lRoadCurve.addAll(LRoadCurveList);
                LRoadCurveList.clear();
                mLRoadCurveDataLogger = new LRoadCurveDataLogger(mContext,btnName);
                mLRoadCurveDataLogger.setHeaders();
                calculateEnergy(lRoadCurve,type);
                break;
            case Constants.R_ROAD_CURVE:
                RRoadCurveEnabled = false;
                ArrayList<EnergyDataModel> rRoadCurve = new ArrayList<>();
                rRoadCurve.addAll(RRoadCurveList);
                RRoadCurveList.clear();
                mRRoadCurveDataLogger = new RRoadCurveDataLogger(mContext,btnName);
                mRRoadCurveDataLogger.setHeaders();
                calculateEnergy(rRoadCurve,type);
                break;
            case Constants.RASH_DRIVING:
                rashDrivingEnabled = false;
                ArrayList<EnergyDataModel> rashDriving = new ArrayList<>();
                rashDriving.addAll(rashDrivingList);
                rashDrivingList.clear();
                mRashDrivingDataLogger = new RashDrivingDataLogger(mContext,btnName);
                mRashDrivingDataLogger.setHeaders();
                calculateEnergy(rashDriving,type);
                break;
            case Constants.HARSH_BRAKING:
                harshBrakeEnabled = false;
                ArrayList<EnergyDataModel> harshBrake = new ArrayList<>();
                harshBrake.addAll(harshBrakeList);
                harshBrakeList.clear();
                mHarshBrakingDataLogger = new HarshBrakingDataLogger(mContext,btnName);
                mHarshBrakingDataLogger.setHeaders();
                calculateEnergy(harshBrake,type);
                break;
            case Constants.L_LANE_CHANGE:
                LLaneChangeEnabled = false;
                ArrayList<EnergyDataModel> lLaneChange = new ArrayList<>();
                lLaneChange.addAll(LLaneChangeList);
                LLaneChangeList.clear();
                mLLaneChangeDataLogger = new LLaneChangeDataLogger(mContext,btnName);
                mLLaneChangeDataLogger.setHeaders();
                calculateEnergy(lLaneChange,type);
                break;
            case Constants.R_LANE_CHANGE:
                RLaneChangeEnabled = false;
                ArrayList<EnergyDataModel> rLaneChange = new ArrayList<>();
                rLaneChange.addAll(RLaneChangeList);
                RLaneChangeList.clear();
                mRLaneChangeDataLogger = new RLaneChangeDataLogger(mContext,btnName);
                mRLaneChangeDataLogger.setHeaders();
                calculateEnergy(rLaneChange,type);
                break;
            case Constants.ROAD_HUMPS:
                roadHumpsEnabled = false;
                ArrayList<EnergyDataModel> roadHumps = new ArrayList<>();
                roadHumps.addAll(roadHumpList);
                roadHumpList.clear();
                mRoadHumpsDataLogger = new RoadHumpsDataLogger(mContext,btnName);
                mRoadHumpsDataLogger.setHeaders();
                calculateEnergy(roadHumps,type);
                break;
            case Constants.UNEVEN_ROAD:
                unevenRoadEnabled = false;
                ArrayList<EnergyDataModel> unevenRoad = new ArrayList<>();
                unevenRoad.addAll(unevenRoadList);
                unevenRoadList.clear();
                mUnEvenRoadDataLogger = new UnEvenRoadDataLogger(mContext,btnName);
                mUnEvenRoadDataLogger.setHeaders();
                calculateEnergy(unevenRoad,type);
                break;
            case Constants.CONTIUNOUS_CURVE:
                contiuonusCurveEnabled = false;
                ArrayList<EnergyDataModel> contiCurve = new ArrayList<>();
                contiCurve.addAll(contiunousCurveList);
                contiunousCurveList.clear();
                mContinousCurveDatalogger = new ContinousCurveDatalogger(mContext,btnName);
                mContinousCurveDatalogger.setHeaders();
                calculateEnergy(contiCurve,type);
                break;
            case Constants.TRAFFIC_LIGHT:
                trafficLightEnabled = false;
                ArrayList<EnergyDataModel> trafficLight = new ArrayList<>();
                trafficLight.addAll(trafficLightList);
                trafficLightList.clear();
                mTrafficLightDataLogger = new TrafficLightDataLogger(mContext,btnName);
                mTrafficLightDataLogger.setHeaders();
                calculateEnergy(trafficLight,type);
                break;
            case Constants.UPWARD_SLOPE:
                upwardSlopeEnabled = false;
                ArrayList<EnergyDataModel> upwardSlope = new ArrayList<>();
                upwardSlope.addAll(upwardSlopeList);
                upwardSlopeList.clear();
                mUpwardSlopeDataLogger = new UpwardSlopeDataLogger(mContext,btnName);
                mUpwardSlopeDataLogger.setHeaders();
                calculateEnergy(upwardSlope,type);
                break;
            case Constants.DOWN_SLOPE:
                downSlopeEnabled = false;
                ArrayList<EnergyDataModel> downSlope = new ArrayList<>();
                downSlope.addAll(downSlopeList);
                downSlopeList.clear();
                mDownSlopeDataLogger = new DownSlopeDataLogger(mContext,btnName);
                mDownSlopeDataLogger.setHeaders();
                calculateEnergy(downSlope,type);
                break;
            case Constants.H_LANE_CHANGE:
                HLaneChangeEnabled = false;
                ArrayList<EnergyDataModel> hLaneChange = new ArrayList<>();
                hLaneChange.addAll(HLaneChangeList);
                HLaneChangeList.clear();
                mhHLaneChangeDataLogger = new HLaneChangeDataLogger(mContext,btnName);
                mhHLaneChangeDataLogger.setHeaders();
                calculateEnergy(hLaneChange,type);
                break;
            case Constants.UNDER_PASS:
                underPassEnabled = false;
                ArrayList<EnergyDataModel> underPass = new ArrayList<>();
                underPass.addAll(underPassList);
                underPassList.clear();
                mUnderPassDataLogger = new UnderPassDataLogger(mContext,btnName);
                mUnderPassDataLogger.setHeaders();
                calculateEnergy(underPass,type);
                break;
            case Constants.FLY_OVER:
                flyOverEnabled = false;
                ArrayList<EnergyDataModel> flyOver = new ArrayList<>();
                flyOver.addAll(flyOverList);
                flyOverList.clear();
                mFlyOverDataLogger = new FlyOverDataLogger(mContext,btnName);
                mFlyOverDataLogger.setHeaders();
                calculateEnergy(flyOver,type);
                break;
            default:
                break;
        }
    }

    /**
     * @param data - energy data list
     * @param type - sensor data energy or button event energy
     */
    private void calculateEnergy(final ArrayList<EnergyDataModel> data, final int type) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                double energy = 0;
                double deviation;

                for (int n = 0; n < data.size()-1; n++) {

                    for (int k = 0; k < 50; k++) {

                        if (n - k >= 0) {
                            deviation = data.get(n - k).getDeviation();
                            energy = energy + (deviation * deviation);
                        } else {
                            energy = energy + 0;
                        }
                    }

                    ArrayList<Float> energyData = new ArrayList<>();
                    energyData.add(data.get(n).getTimestamp());
                    energyData.add(data.get(n).getAccelerometerXAxis());
                    energyData.add(data.get(n).getAccelerometerYAxis());
                    energyData.add(data.get(n).getAccelerometerZAxis());
                    energyData.add(data.get(n).getGyroXAxis());
                    energyData.add(data.get(n).getGyroYAxis());
                    energyData.add(data.get(n).getGyroZAxis());
                    energyData.add(data.get(n).getAzimuth());
                    energyData.add(data.get(n).getRoll());
                    energyData.add(data.get(n).getPitch());
                    energyData.add(data.get(n).getGpsSpeed());
                    energyData.add((float)data.get(n).getLatitude());
                    energyData.add((float)data.get(n).getLongitude());
                    energyData.add((float)data.get(n).getBearing());
                    energyData.add(data.get(n).getDeviation());
                    energyData.add((float)energy);

                    switch (type){

                        case Constants.POT_HOLE:
                            if(mPotHoleDataLogger!=null)
                            mPotHoleDataLogger.addRow(energyData);
                            break;
                        case Constants.U_TURN:
                            if(mUTurnDataLogger!=null)
                            mUTurnDataLogger.addRow(energyData);
                            break;
                        case Constants.L_TURN:
                            if(mLTurnDataLogger!=null)
                            mLTurnDataLogger.addRow(energyData);
                            break;
                        case Constants.R_TURN:
                            if(mRTurnDatalogger!=null)
                            mRTurnDatalogger.addRow(energyData);
                            break;
                        case Constants.ZIG_ZAG:
                            if(mZigZagDataLogger!=null)
                            mZigZagDataLogger.addRow(energyData);
                            break;
                        case Constants.ROUND_ABOUT:
                            if(mRoundAboutDataLogger!=null)
                            mRoundAboutDataLogger.addRow(energyData);
                            break;
                        case Constants.L_ROAD_CURVE:
                            if(mLRoadCurveDataLogger!=null)
                            mLRoadCurveDataLogger.addRow(energyData);
                            break;
                        case Constants.R_ROAD_CURVE:
                            if(mRRoadCurveDataLogger!=null)
                            mRRoadCurveDataLogger.addRow(energyData);
                            break;
                        case Constants.RASH_DRIVING:
                            if(mRashDrivingDataLogger!=null)
                            mRashDrivingDataLogger.addRow(energyData);
                            break;
                        case Constants.HARSH_BRAKING:
                            if(mHarshBrakingDataLogger!=null)
                            mHarshBrakingDataLogger.addRow(energyData);
                            break;
                        case Constants.L_LANE_CHANGE:
                            if(mLLaneChangeDataLogger!=null)
                            mLLaneChangeDataLogger.addRow(energyData);
                            break;
                        case Constants.R_LANE_CHANGE:
                            if(mRLaneChangeDataLogger!=null)
                            mRLaneChangeDataLogger.addRow(energyData);
                            break;
                        case Constants.ROAD_HUMPS:
                            if(mRoadHumpsDataLogger!=null)
                            mRoadHumpsDataLogger.addRow(energyData);
                            break;
                        case Constants.UNEVEN_ROAD:
                            if(mUnEvenRoadDataLogger!=null)
                            mUnEvenRoadDataLogger.addRow(energyData);
                            break;
                        case Constants.CONTIUNOUS_CURVE:
                            if(mContinousCurveDatalogger!=null)
                            mContinousCurveDatalogger.addRow(energyData);
                            break;
                        case Constants.TRAFFIC_LIGHT:
                            if(mTrafficLightDataLogger!=null)
                            mTrafficLightDataLogger.addRow(energyData);
                            break;
                        case Constants.UPWARD_SLOPE:
                            if(mUpwardSlopeDataLogger!=null)
                            mUpwardSlopeDataLogger.addRow(energyData);
                            break;
                        case Constants.DOWN_SLOPE:
                            if(mDownSlopeDataLogger!=null)
                            mDownSlopeDataLogger.addRow(energyData);
                            break;
                        case Constants.H_LANE_CHANGE:
                            if(mhHLaneChangeDataLogger!=null)
                            mhHLaneChangeDataLogger.addRow(energyData);
                            break;
                        case Constants.UNDER_PASS:
                            if(mUnderPassDataLogger!=null)
                            mUnderPassDataLogger.addRow(energyData);
                            break;
                        case Constants.FLY_OVER:
                            if(mFlyOverDataLogger!=null)
                            mFlyOverDataLogger.addRow(energyData);
                            break;
                        default:
                            break;
                    }
                    energyData.clear();
                    energy = 0;
                }

            }
        });
        thread.start();
    }


    private void startProcessingThread(){

        dataProcessingThread = new Thread(new Runnable(){
            @Override
            public void run(){
                dataProcess();
            }
        });
        dataProcessingThread.start();
    }

    private void dataProcess() {

        while (!mIsThreadStopped) {

            if (potHoleList.size() > 0) {

                ArrayList<EnergyDataModel> tempList = new ArrayList<>();
                tempList.addAll(potHoleList);
                for (int i=0; i<tempList.size(); i++){
                    potHoleList.remove(i);
                }
            }
        }

        try {
            Thread.sleep(1);
        } catch (InterruptedException e){
            e.printStackTrace();
        }

    }

    public void stopButtonEventThread(){

    }

}
