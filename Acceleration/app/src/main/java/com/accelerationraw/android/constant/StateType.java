package com.accelerationraw.android.constant;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vikash Sharma on 23-02-2018.
 */

public enum StateType {

    STATIONARY(1,"STATIONARY"),

    EXPECTED_STATIONARY(2,"EXPECTED_STATIONARY"),

    MOVING(3,"MOVING"),

    EXPECTED_MOVEMENT(4,"EXPECTED_MOVEMENT") ;


    private static final Map<Integer, StateType> STATE_TYPES_BY_ID = new HashMap<>();
    static{
        for(final StateType st : values()){
            if(st.getStateId() == null) {
                continue;
            }
            STATE_TYPES_BY_ID.put(st.getStateId(), st);
        }
    }

    private Integer stateId;
    private String name;

    StateType(Integer androidId, String name) {
        this.stateId = androidId;
        this.name = name;
    }

    public Integer getStateId() {
        return stateId;
    }

    public String getName() {
        return name;
    }



    public static StateType getByStateID(int stateId){
        return STATE_TYPES_BY_ID.get(stateId);
    }



}
