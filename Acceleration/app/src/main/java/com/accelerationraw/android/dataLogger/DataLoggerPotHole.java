package com.accelerationraw.android.dataLogger;

import android.content.Context;

import com.accelerationraw.android.constant.Constants;
import com.accelerationraw.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * Created by Sourabh on 28-03-2018.
 */

public class DataLoggerPotHole {

    private static final String TAG = DataLoggerAudioEvent.class.getSimpleName();

    private ArrayList<String> csvHeaders;

    private DataLoggerInterface dataLogger;

    public DataLoggerPotHole(Context context) {

        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<Float> value){
        dataLogger.addRowData(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return Constants.POT_HOLE_DATA_FILE_NAME;
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("TimeStamp");
        headers.add("Z value");
        headers.add("Moving avg filter value");
        headers.add("BandPass Filter value");
        headers.add("Latitude");
        headers.add("Longitude");
        headers.add("Speed");
        headers.add("Bearing");
        return headers;
    }
}
