package com.accelerationraw.android.earthRotation;

/**
 *
 * Created by jair on 07/05/16.
 */
public interface ThreadPriority {
    void set(int priority);
}
