package com.accelerationraw.android.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.accelerationraw.android.constant.SharedPrefConstants;
import com.accelerationraw.android.R;
import com.accelerationraw.android.utils.SharedPreferenceManager;
import com.accelerationraw.android.utils.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/*
 * Created by Sourabh on 12-01-2018.
 */

public class LocationTracker implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLocation;

    private static final long UPDATE_INTERVAL = 1000;
    private static final long FASTEST_INTERVAL = 1000;

    private double mLatitude;
    private double mLongitude;
    private double mSpeed;
    private boolean mIsPermissionGranted = false;
    private boolean mIsLessThan40Notified = false;
    private boolean mIs4050Notified = false;
    private boolean mIs60Notified = false;
    private boolean mIs5060Notified = false;

    private static final int LESS_THAN_40 = 0;
    private static final int FOURTY_FIFTY = 1;
    private static final int FIFTY_SIXTY = 2;
    private static final int GREATER_THAN_60 = 3;
    private boolean mIsOverSpeeding = false;
    private int mOverSpeedingCounter = 0;
    private double mSpeedInKm = 0;
    private float mBearing;
    private double mAltitude;
    private float mBearingAccuracy;
    private float mSpeedAccuracy;

    public LocationTracker(Context context) {
        this.mContext = context;
        init();
    }

    private void init() {

        if (isPermissionGranted()) {

            mIsPermissionGranted = true;
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            startClient();
        } else {
            mIsPermissionGranted = false;
        }
    }

    private void startClient() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation != null) {
            mLatitude = mLocation.getLatitude();
            mLongitude = mLocation.getLongitude();
            mSpeed = mLocation.getSpeed();
            mBearing = mLocation.getBearing();
            mAltitude = mLocation.getAltitude();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mBearingAccuracy = mLocation.getBearingAccuracyDegrees();
                mSpeedAccuracy = mLocation.getSpeedAccuracyMetersPerSecond();
            }

        }
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {

        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
        mSpeed = location.getSpeed();
        mBearing = location.getBearing();
        mAltitude = location.getAltitude();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mBearingAccuracy = location.getBearingAccuracyDegrees();
            mSpeedAccuracy = location.getSpeedAccuracyMetersPerSecond();
        }


        double speed = getGetInKM(mSpeed);
        mSpeedInKm = speed;
        int speedThreshold = SharedPreferenceManager.read(SharedPrefConstants.SPEED_THRESHOLD);
        if(speedThreshold != 0 && speed > speedThreshold){
            mIsOverSpeeding = true;
            mOverSpeedingCounter++;
            SharedPreferenceManager.write(SharedPrefConstants.OVER_SPEEDING,mOverSpeedingCounter);
        }

         if(speed > 10 && speed < 40){
            if(!mIsLessThan40Notified){
                mIsLessThan40Notified = true;
                playSound(LESS_THAN_40);
            }

        }else if(speed > 40 && speed <=50){

            if(!mIs4050Notified){
                mIs4050Notified = true;
                playSound(FOURTY_FIFTY);
            }

        }else if( speed > 50 && speed <= 60){

            if(!mIs5060Notified){
                mIs5060Notified = true;
                playSound(FIFTY_SIXTY);
            }

        }else if(speed >60){
            if(!mIs60Notified) {
                mIs60Notified = true;
                playSound(GREATER_THAN_60);
            }
        }
    }

    public boolean getOverSpeeding(){
        return mIsOverSpeeding;
    }


    public void setSpeedThreshold(){
        mIsOverSpeeding = false;
    }

    public double getSpeedInKm(){
        return mSpeedInKm;
    }

    private void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    public void stopLocationUpdates()
    {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    public double getLatitude(){
        return mLatitude;
    }

    public double getLongitude(){
        return mLongitude;
    }

    public double getSpeed(){
        return mSpeed;
    }

    public float getBearing(){
        return mBearing;
    }
    public double getAltitude(){
        return mAltitude;
    }
    public float getBearingAccuracy(){
        return mBearingAccuracy;
    }
    public float getSpeedAccuracy(){
        return mSpeedAccuracy;
    }

    public boolean isPermissionAvailable(){
        return mIsPermissionGranted;
    }

    private boolean isPermissionGranted(){

        boolean isGranted = ContextCompat.checkSelfPermission(mContext, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        return ((Build.VERSION.SDK_INT >= 23 && isGranted) || Build.VERSION.SDK_INT < 23);
    }

    private double getGetInKM(double speed){

        double a = speed*18;
        return a/5;
    }

    private void playSound(int soundType){

        try {
            MediaPlayer mp;
            switch (soundType) {

                case LESS_THAN_40:
                    mp = MediaPlayer.create(mContext, R.raw.less_than_40);
                    mp.start();
                    break;
                case FOURTY_FIFTY:
                    mp = MediaPlayer.create(mContext, R.raw.more_than_40);
                    mp.start();
                    break;
                case FIFTY_SIXTY:
                    mp = MediaPlayer.create(mContext, R.raw.more_than_50);
                    mp.start();
                    break;
                case GREATER_THAN_60:
                    mp = MediaPlayer.create(mContext, R.raw.more_than_60);
                    mp.start();
                    break;
            }
        }catch (Exception e){
            Log.e("TAG", "excception :"+e.toString());
        }
    }

}

