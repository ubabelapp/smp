package com.accelerationraw.android.AudioEvent;

/**
 *
 * Created by Sourabh on 21-03-2018.
 */

public class PeakDataModel {

    private double startTime;
    private double endTime;
    private double rms;
    private double latitude;
    private double longitude;
    private double speed;
    private double bearing;
    private long timeInMills;

    public PeakDataModel(double startTime, double endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public PeakDataModel(double startTime, double endTime, double rms, double latitude, double longitude, double speed, double bearing, long timeInMills) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.rms = rms;
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
        this.bearing = bearing;
        this.timeInMills = timeInMills;
    }

    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public double getRms() {
        return rms;
    }

    public void setRms(double rms) {
        this.rms = rms;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getBearing() {
        return bearing;
    }

    public void setBearing(double bearing) {
        this.bearing = bearing;
    }

    public long getTimeInMills() {
        return timeInMills;
    }

    public void setTimeInMills(long timeInMills) {
        this.timeInMills = timeInMills;
    }
}
