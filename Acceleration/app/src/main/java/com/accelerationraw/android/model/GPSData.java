package com.accelerationraw.android.model;

/**
 * @author rainbow
 *
 */
public class GPSData {

	private double latitude;
	private double longitude;
	private double speed;
	private double heading;
	private double altitude;
	private double headingAccuracy;

	/**
	 * 
	 * @param latitude
	 * @param longitude
	 * @param speed
	 * @param heading
	 * @param altitude
	 * @param headingAccuracy
	 */
	public GPSData(double latitude, double longitude, double speed, double heading, double altitude,
			double headingAccuracy) {

		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
		this.speed = speed;
		this.heading = heading;
		this.altitude = altitude;
		this.headingAccuracy = headingAccuracy;

	}
	public GPSData() {
	}

	/**
	 * 
	 * @param latitude
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * setting the double longitude value
	 * 
	 * @param longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * getting double longitude value
	 * 
	 * @return
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * setting the latitude value of the gps
	 * 
	 * @param altitude
	 */
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	/**
	 * getting the altitude
	 * @return double speed
	 */
	public double getAltitude() {
		return altitude;
	}

	/**
	 * setting the speed received from the GPS data
	 * @param speed
	 */
	public void setSpeed(double speed) {
		this.speed = speed;
	}

	/**
	 * getting the double speed of the gps data
	 * @return double speed
	 */
	public double getSpeed() {
		return speed;
	}

	/**
	 * setting the heading value of the gps data
	 * @param heading
	 */
	public void setHeading(double heading) {
		this.heading = heading;
	}

	/**
	 * getting the heading value of the GPS data This is also known as the
	 * bearing of the angle value from 0 degree to 360 in decimal places.
	 * 
	 * @return
	 */
	public double getHeading() {
		return heading;
	}

	/**
	 * setting the heading accuracy to measure the heading correctly
	 * @param headingAccuracy
	 */
	public void setHeadingAccuracy(double headingAccuracy) {
		this.headingAccuracy = headingAccuracy;
	}

	/**
	 * Getting the heading accuracy of the vehicle
	 * @return
	 */
	public double getHeadingAccuracy() {
		return headingAccuracy;
	}

}
