/**
 * 
 */
package com.accelerationraw.android.eventProcess;

import com.accelerationraw.android.model.EventData;
import com.accelerationraw.android.processFactory.ProcessFactory;

/**
 * @author Vikash Sharma
 *
 */
public class EventValidationImpl implements EventValidationProcess {

	/**
	 * 
	 */
	public EventValidationImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vsense.distance.EventValidationProcess#getProcessType()
	 */
	@Override
	public ProcessFactory getProcessType(ProcessFactory currentProcess) {

		ProcessFactory currentFactory = currentProcess;
		switch (currentFactory) {
		case NO_PROCESS:
			return ProcessFactory.SIMPLE_ACCELERATION_PROCESS;
		case SIMPLE_ACCELERATION_PROCESS:
			return ProcessFactory.V_SENSE_PROCESS;
		case V_SENSE_PROCESS:
			return ProcessFactory.GPS_DATA_PROCESS;
		case GPS_DATA_PROCESS:
			return ProcessFactory.FAST_DTW_PROCESS;
		case FAST_DTW_PROCESS:
			return ProcessFactory.PROCESS_FINISHED;
		default:
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vsense.distance.EventValidationProcess#processData(com.vsense.
	 * distance.EventData)
	 */
	@Override
	public EventStats processData(EventData eventData) {
		return null;
	}

}
