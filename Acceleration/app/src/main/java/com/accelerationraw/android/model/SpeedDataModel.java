package com.accelerationraw.android.model;

/*
 * Created by Sourabh on 13-01-2018.
 */

public class SpeedDataModel {

    private float speed;
    private long timeStamp;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public SpeedDataModel(float speed, long timeStamp) {
        this.speed = speed;
        this.timeStamp = timeStamp;
    }
}
