package com.accelerationraw.android.constant;

import com.accelerationraw.android.filterVO.StationaryFilterVO;

/**
 * Created by sourabh on 30-12-2017.
 */

public class Constants {

    public static final String APP_FOLDER_NAME = "ABC_SENSOR_DATA_RAW";
    public static final String SENSOR_DATA_FILE_NAME = "RawSensorData.csv";
    public static final String V_SENSE_DATA_FILE_NAME = "VSense.csv";
    public static final String ENERGY_DATA_FILE_NAME = "EnergySensorData.csv";
    public static final String INDICATOR_EVENT_FILE_NAME = "IndicatorEventData.csv";
    public static final String POT_HOLE_DATA_FILE_NAME = "PotHoleEventData.csv";

    public static final double OVER_SPEEDING = 1.0;
    public static final double HARSH_BRAKE = 2.0;

    public static final String SENSOR_ACCELEROMETER = "accelerometer";
    public static final String SENSOR_GYROSCOPE = "gyroscope";

    public static final String STRING_OVER_SPEEDING = "Over_Speeding";
    public static final String STRING_HARSH_BRAKE = "Harsh_Brake";

    public static final int V_SENSE_PROCESS = 1;
    public static final int NORMAL_PROCESS = 2;

    public static final int SAMPLING_RATE_10Htz   = 10;
    public static final int SAMPLING_RATE_20Htz   = 20;
    public static final int SAMPLING_RATE_30Htz   = 30;
    public static final int SAMPLING_RATE_40Htz   = 40;
    public static final int SAMPLING_RATE_50Htz   = 50;
    public static final int SAMPLING_RATE_60Htz   = 60;
    public static final int SAMPLING_RATE_100Htz  = 100;


    public static long STATIONARY_FILTER_START_TIME = 0;
    public static long STATIONARY_FILTER_END_TIME = 0;
    public static final long STATIONARY_MIN_THRESHOLD = 120000L;
    public static final long STATIONARY_EXPCT_THRESHOLD = 70000L;

    public static StationaryFilterVO FIRST_STATIONARY_OBJECT = new StationaryFilterVO();
    public static final String VEHICLE_STATE_1 = "STATIONARY";
    public static final String VEHICLE_STATE_2 = "EXPECTED_STATIONARY";
    public static final String VEHICLE_STATE_3 = "MOVING";
    public static final String VEHICLE_STATE_4 = "EXPECTED_MOVEMENT";

    public static final int BUFFER_WINDOW_SIZE = 101;

    public static final int GYROSCOPE_SENSOR = 1;
    public static final int ACCELEROMETER_SENSOR = 2;
    public static final int MAGNETIC_FIELD_SENSOR = 3;

    public static final double LAN_CHANGE_DISPLACEMENT_THRESHOLD = 3.5;  // 3.5 has been taken as the width of the one lane in India
    // if vehicle changes the lane it should have crossed atleast this threshold


    public static final int POT_HOLE = 1;
    public static final int U_TURN = 2;
    public static final int L_TURN = 3;
    public static final int R_TURN = 4;
    public static final int ZIG_ZAG = 5;
    public static final int ROUND_ABOUT = 6;
    public static final int L_ROAD_CURVE = 7;
    public static final int R_ROAD_CURVE = 8;
    public static final int RASH_DRIVING = 9;
    public static final int HARSH_BRAKING = 10;
    public static final int L_LANE_CHANGE = 11;
    public static final int R_LANE_CHANGE = 12;
    public static final int ROAD_HUMPS = 13;
    public static final int UNEVEN_ROAD = 14;
    public static final int CONTIUNOUS_CURVE = 15;
    public static final int TRAFFIC_LIGHT = 16;
    public static final int UPWARD_SLOPE = 17;
    public static final int DOWN_SLOPE = 18;
    public static final int H_LANE_CHANGE = 19;
    public static final int UNDER_PASS = 20;
    public static final int FLY_OVER = 21;


}
