package com.accelerationraw.android.earthRotation;


import com.accelerationraw.android.async.ErrorContext;

public class TripErrorContext implements ErrorContext {
    private Trip trip;
    private DataCollectionEventConsumerErrorContext dataCollectionEventConsumerErrorContext;

    public TripErrorContext(Trip trip, DataCollectionEventConsumerErrorContext dataCollectionEventConsumerErrorContext) {
        this.trip = trip;
        this.dataCollectionEventConsumerErrorContext = dataCollectionEventConsumerErrorContext;
    }

    public Trip getTrip() {
        return trip;
    }

    @Override
    public Throwable getError() {
        return dataCollectionEventConsumerErrorContext.getError();
    }

    public DataCollectionEventConsumerErrorContext getDataCollectionEventConsumerErrorContext() {
        return dataCollectionEventConsumerErrorContext;
    }
}
