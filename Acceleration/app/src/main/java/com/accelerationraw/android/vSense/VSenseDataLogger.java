package com.accelerationraw.android.vSense;

import android.content.Context;

import com.accelerationraw.android.constant.Constants;
import com.accelerationraw.android.dataLogger.CsvDataLogger;
import com.accelerationraw.android.dataLogger.DataLogger;
import com.accelerationraw.android.dataLogger.DataLoggerInterface;
import com.accelerationraw.android.utils.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sourabh on 16-03-2018.
 */

public class VSenseDataLogger {

    private static final String TAG = DataLogger.class.getSimpleName();

    private ArrayList<String> csvHeaders;
    private DataLoggerInterface dataLogger;

    public VSenseDataLogger(Context context) {

        dataLogger = new CsvDataLogger(context, getFile(this.getFilePath(), this.getFileName()));
        csvHeaders = getCsvHeaders();
    }

    public void setHeaders(){
        dataLogger.setHeaders(csvHeaders);
    }

    public void addRow(ArrayList<String> value){
        dataLogger.addRow(value);
    }

    public void stopLogging(){
        dataLogger.writeToFile();
    }

    private File getFile(String filePath, String fileName) {
        File dir = new File(filePath);

        if (!dir.exists()) {
            dir.mkdirs();
        }
        return new File(dir, fileName);
    }

    private String getFilePath() {
        return Util.createAndGetFolderPath();
    }

    private String getFileName() {
        return Constants.V_SENSE_DATA_FILE_NAME;
    }

    private ArrayList<String> getCsvHeaders() {

        ArrayList<String> headers = new ArrayList<>();
        headers.add("Timestamp");
        headers.add("TimeStampMillis");
        headers.add("type");
        headers.add("Gyro-Z-value (Yaw)");
        headers.add("Speed");
        headers.add("Latitude");
        headers.add("Longitude");
        headers.add("Altitude");
        headers.add("Bearing");
        headers.add("Event Type");
        headers.add("Displacement value");
        return headers;
    }
}
