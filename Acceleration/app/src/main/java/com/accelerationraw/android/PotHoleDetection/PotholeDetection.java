package com.accelerationraw.android.PotHoleDetection;

import android.content.Context;

import com.accelerationraw.android.Fdtw.vsense.distance.MovingAverage;
import com.accelerationraw.android.constant.Constants;
import com.accelerationraw.android.dataLogger.DataLoggerPotHole;
import com.accelerationraw.android.model.AccelerometerData;
import com.accelerationraw.android.utils.ResamplingData;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by vikash on 27-03-2018.
 */

public class PotholeDetection {

    private DataLoggerPotHole mDataLoggerPotHole;

    public PotholeDetection(Context context){

        this.mDataLoggerPotHole = new DataLoggerPotHole(context);
        this.mDataLoggerPotHole.setHeaders();
    }

    public void process(List<AccelerometerData>data, float sensorFrequency, int desiredFrequency){

        List<AccelerometerData> reSampledData = ResamplingData.reSampleAccelerometerDataModel(data, sensorFrequency, desiredFrequency);

        MovingAverage movingAverage = new MovingAverage(Constants.BUFFER_WINDOW_SIZE);
        for (int i=0; i<reSampledData.size(); i++){

            movingAverage.pushValue(reSampledData.get(i).getZ());
            double value = movingAverage.getValue();
            double bandPassValue = applyBandPassFilter(value);

            ArrayList<Float> rowData = new ArrayList<>();
            rowData.add((float)reSampledData.get(i).getTimestamp());
            rowData.add(reSampledData.get(i).getZ());
            rowData.add((float) value);
            rowData.add((float) bandPassValue);
            rowData.add((float) reSampledData.get(i).getLatitude());
            rowData.add((float) reSampledData.get(i).getLongitude());
            rowData.add((float) reSampledData.get(i).getSpeed());
            rowData.add((float) reSampledData.get(i).getBearing());
            mDataLoggerPotHole.addRow(rowData);
            rowData.clear();
        }
    }


    private double applyBandPassFilter(double value){

        final double alpha = 0.8; // constant for our filter below
        double gravity =0;

        // Isolate the force of gravity with the low-pass filter.
        gravity = alpha * gravity + (1 - alpha) * value;

        // Remove the gravity contribution with the high-pass filter.
        return value - gravity;
    }


    public void stopLogging(){
        if(mDataLoggerPotHole != null){
            mDataLoggerPotHole.stopLogging();
        }
    }

}
