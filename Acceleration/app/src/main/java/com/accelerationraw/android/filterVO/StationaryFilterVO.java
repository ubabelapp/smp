package com.accelerationraw.android.filterVO;

/**
 * Created by Vikash Sharma on 23-02-2018.
 */

public class StationaryFilterVO implements Cloneable {

    private double speed;
    private double longitude;
    private double latitude;
    private double altitude;
    private long eventTime;

    public StationaryFilterVO(double speed, double longitude, double latitude, double altitude, long eventTime) {
        this.speed = speed;
        this.longitude = longitude;
        this.latitude = latitude;
        this.altitude = altitude;
        this.eventTime = eventTime;
    }


    public StationaryFilterVO(){

    }


    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEventTime(long eventTime) {
        this.eventTime = eventTime;
    }


    @Override
    public String toString() {
        return "StationaryFilterVO{" +
                "speed=" + speed +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", altitude=" + altitude +
                ", eventTime=" + eventTime +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StationaryFilterVO)) return false;

        StationaryFilterVO that = (StationaryFilterVO) o;

        if (Double.compare(that.getSpeed(), getSpeed()) != 0) return false;
        if (Double.compare(that.getLongitude(), getLongitude()) != 0) return false;
        if (Double.compare(that.getLatitude(), getLatitude()) != 0) return false;
        return Double.compare(that.getAltitude(), getAltitude()) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getSpeed());
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLongitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getLatitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getAltitude());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


    @Override
    public Object clone() throws  CloneNotSupportedException{
        return super.clone();
    }

}
